
[![Modeling environment with Papyrus](images/carousel/carousel-papyrus.png)][epapyrus]

Our application is a component of [Papyrus][epapyrus], an integrated and user-consumable environment for editing any kind of EMF model and particularly supporting UML2 and related modeling languages .

[epapyrus]: https://www.eclipse.org/papyrus/



[![My Title](images/carousel/carousel-MARTE.png)]

Use XXXXXXX.

[ecomponent]: https://www.eclipse.org/papyrus/components/MARTE


---




This site is mainly dedicated for developers, you can find information relative to the MARTE's developments, such as [Git][egit], Dependencies, [Javadoc][ejavadoc], procedures, tips etc...You can download a specific [RCP][ercp] with or you can copy/paste the [update site url][esite] into your own favorite eclipse.
If you are looking to informations linked to Papyrus, please have a look to the official [Papyrus main site][papyrus].



Have a look at the online developer documentation of [developer][devhome] or download it as pdf [developer-guide.pdf][developer-guide.pdf].

Have a look at the online user documentation of [user][userhome] or download it as pdf [user-guide.pdf][user-guide.pdf].

[devhome]: ./developer/index.html
[developer-guide.pdf]: ./pdf/developer-guide.pdf
[ercp]: http://download.eclipse.org/modeling/mdt/papyrus/components/MARTE/downloads/
[esite]: http://download.eclipse.org/modeling/mdt/papyrus/components/MARTE
[userhome]: ./user/index.html
[user-guide.pdf]: ./pdf/user-guide.pdf
[ejavadoc]: ./xref/
[egit]: ./source-repository.html


---



MARTE is a component of the Eclipse [Papyrus][papyrus]'s galaxy.

[papyrus]: https://eclipse.org/papyrus/

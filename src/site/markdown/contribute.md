Contributing to Papyrus MARTE
--------------------

Thanks for your interest in this project.

Project description:
--------------------

MyprojectDescription

Create a new [bug][ebug]:
-----------------

Be sure to search for existing bugs before you create another one. Remember that contributions are always welcome!

[ebug]: https://bugs.eclipse.org/bugs/enter_bug.cgi?

Search for [bugs][ebug]:
----------------

This project uses Bugzilla to track ongoing development and issues.

[ebug]: https://bugs.eclipse.org/bugs/enter_bug.cgi

Contributor License Agreement:
------------------------------

Before your contribution can be accepted by the project, you need to create and electronically 
sign the Eclipse Foundation Contributor License Agreement [(CLA)](http://www.eclipse.org/legal/CLA.php).

Contact:
--------

Contact the project developers and users via the project's ["user" list](https://dev.eclipse.org/mailman/listinfo/papyrus-MARTE-users/).

Developer resources:
--------------------

 * Check the README.md file for developing information
 * Continuous integration with [Hudson](https://hudson.eclipse.org/papyrus/)
 * Contribution review with [Gerrit](https://git.eclipse.org/)

The MARTE profile defines a set of data types (notably in the context of VSL) that need to be mapped
to strings. As far as we know, the UML2 plugin does not support this mapping via configuration
attributes. This directory contains a patch of the UML2 plugin which needs to be applied to the
installed UML2 plugin prior to a re-generation of the static profile.

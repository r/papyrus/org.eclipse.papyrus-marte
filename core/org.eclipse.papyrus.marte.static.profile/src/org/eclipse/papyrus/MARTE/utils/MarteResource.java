/*******************************************************************************
 * Copyright (c) 2006, 2016 CEA LIST and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *     
 *     
 *******************************************************************************/
package org.eclipse.papyrus.MARTE.utils;

public class MarteResource {

	public static String FLOW_PORT_ID = "MARTE::MARTE_DesignModel::GCM::FlowPort";

	public static String CLIENT_SERVER_PORT_ID = "MARTE::MARTE_DesignModel::GCM::ClientServerPort";


}

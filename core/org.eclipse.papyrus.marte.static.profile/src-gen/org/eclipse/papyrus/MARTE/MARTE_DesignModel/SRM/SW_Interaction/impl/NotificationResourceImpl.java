/**
 * Copyright (c) 2010, 2016 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Chokri MRAIDHA (CEA LIST) chokri.mraidha@cea.fr - Initial API and implementation
 *
 */
package org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.NotificationKind;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.NotificationResource;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.NotificationResourceKind;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.SW_InteractionPackage;

import org.eclipse.uml2.uml.BehavioralFeature;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.TypedElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Notification Resource</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.impl.NotificationResourceImpl#getOccurence <em>Occurence</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.impl.NotificationResourceImpl#getMechanism <em>Mechanism</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.impl.NotificationResourceImpl#getOccurenceCountElements <em>Occurence Count Elements</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.impl.NotificationResourceImpl#getMaskElements <em>Mask Elements</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.impl.NotificationResourceImpl#getFlushServices <em>Flush Services</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.impl.NotificationResourceImpl#getSignalServices <em>Signal Services</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.impl.NotificationResourceImpl#getWaitServices <em>Wait Services</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.impl.NotificationResourceImpl#getClearServices <em>Clear Services</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NotificationResourceImpl extends SwSynchronizationResourceImpl implements NotificationResource {
	/**
	 * The default value of the '{@link #getOccurence() <em>Occurence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getOccurence()
	 * @generated
	 * @ordered
	 */
	protected static final NotificationKind OCCURENCE_EDEFAULT = NotificationKind.MEMORIZED;

	/**
	 * The cached value of the '{@link #getOccurence() <em>Occurence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getOccurence()
	 * @generated
	 * @ordered
	 */
	protected NotificationKind occurence = OCCURENCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMechanism() <em>Mechanism</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getMechanism()
	 * @generated
	 * @ordered
	 */
	protected static final NotificationResourceKind MECHANISM_EDEFAULT = NotificationResourceKind.EVENT;

	/**
	 * The cached value of the '{@link #getMechanism() <em>Mechanism</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getMechanism()
	 * @generated
	 * @ordered
	 */
	protected NotificationResourceKind mechanism = MECHANISM_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOccurenceCountElements() <em>Occurence Count Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getOccurenceCountElements()
	 * @generated
	 * @ordered
	 */
	protected EList<TypedElement> occurenceCountElements;

	/**
	 * The cached value of the '{@link #getMaskElements() <em>Mask Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getMaskElements()
	 * @generated
	 * @ordered
	 */
	protected EList<TypedElement> maskElements;

	/**
	 * The cached value of the '{@link #getFlushServices() <em>Flush Services</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getFlushServices()
	 * @generated
	 * @ordered
	 */
	protected EList<BehavioralFeature> flushServices;

	/**
	 * The cached value of the '{@link #getSignalServices() <em>Signal Services</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getSignalServices()
	 * @generated
	 * @ordered
	 */
	protected EList<BehavioralFeature> signalServices;

	/**
	 * The cached value of the '{@link #getWaitServices() <em>Wait Services</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getWaitServices()
	 * @generated
	 * @ordered
	 */
	protected EList<BehavioralFeature> waitServices;

	/**
	 * The cached value of the '{@link #getClearServices() <em>Clear Services</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getClearServices()
	 * @generated
	 * @ordered
	 */
	protected EList<BehavioralFeature> clearServices;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	protected NotificationResourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SW_InteractionPackage.Literals.NOTIFICATION_RESOURCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public NotificationKind getOccurence() {
		return occurence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public void setOccurence(NotificationKind newOccurence) {
		NotificationKind oldOccurence = occurence;
		occurence = newOccurence == null ? OCCURENCE_EDEFAULT : newOccurence;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, SW_InteractionPackage.NOTIFICATION_RESOURCE__OCCURENCE, oldOccurence, occurence));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public NotificationResourceKind getMechanism() {
		return mechanism;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public void setMechanism(NotificationResourceKind newMechanism) {
		NotificationResourceKind oldMechanism = mechanism;
		mechanism = newMechanism == null ? MECHANISM_EDEFAULT : newMechanism;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, SW_InteractionPackage.NOTIFICATION_RESOURCE__MECHANISM, oldMechanism, mechanism));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<TypedElement> getOccurenceCountElements() {
		if (occurenceCountElements == null) {
			occurenceCountElements = new EObjectResolvingEList<TypedElement>(TypedElement.class, this, SW_InteractionPackage.NOTIFICATION_RESOURCE__OCCURENCE_COUNT_ELEMENTS);
		}
		return occurenceCountElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public TypedElement getOccurenceCountElements(String name, Type type) {
		return getOccurenceCountElements(name, type, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public TypedElement getOccurenceCountElements(String name, Type type, boolean ignoreCase, EClass eClass) {
		occurenceCountElementsLoop: for (TypedElement occurenceCountElements : getOccurenceCountElements()) {
			if ((eClass != null && !eClass.isInstance(occurenceCountElements)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(occurenceCountElements.getName()) : name.equals(occurenceCountElements.getName())))) {
				continue occurenceCountElementsLoop;
			}
			if (type != null && !type.equals(occurenceCountElements.getType())) {
				continue occurenceCountElementsLoop;
			}
			return occurenceCountElements;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<TypedElement> getMaskElements() {
		if (maskElements == null) {
			maskElements = new EObjectResolvingEList<TypedElement>(TypedElement.class, this, SW_InteractionPackage.NOTIFICATION_RESOURCE__MASK_ELEMENTS);
		}
		return maskElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public TypedElement getMaskElements(String name, Type type) {
		return getMaskElements(name, type, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public TypedElement getMaskElements(String name, Type type, boolean ignoreCase, EClass eClass) {
		maskElementsLoop: for (TypedElement maskElements : getMaskElements()) {
			if ((eClass != null && !eClass.isInstance(maskElements)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(maskElements.getName()) : name.equals(maskElements.getName())))) {
				continue maskElementsLoop;
			}
			if (type != null && !type.equals(maskElements.getType())) {
				continue maskElementsLoop;
			}
			return maskElements;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<BehavioralFeature> getFlushServices() {
		if (flushServices == null) {
			flushServices = new EObjectResolvingEList<BehavioralFeature>(BehavioralFeature.class, this, SW_InteractionPackage.NOTIFICATION_RESOURCE__FLUSH_SERVICES);
		}
		return flushServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getFlushServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes) {
		return getFlushServices(name, ownedParameterNames, ownedParameterTypes, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getFlushServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass) {
		flushServicesLoop: for (BehavioralFeature flushServices : getFlushServices()) {
			if ((eClass != null && !eClass.isInstance(flushServices)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(flushServices.getName()) : name.equals(flushServices.getName())))) {
				continue flushServicesLoop;
			}
			EList<Parameter> ownedParameterList = flushServices.getOwnedParameters();
			int ownedParameterListSize = ownedParameterList.size();
			if (ownedParameterNames != null && ownedParameterNames.size() != ownedParameterListSize || (ownedParameterTypes != null && ownedParameterTypes.size() != ownedParameterListSize)) {
				continue flushServicesLoop;
			}
			for (int j = 0; j < ownedParameterListSize; j++) {
				Parameter ownedParameter = ownedParameterList.get(j);
				if (ownedParameterNames != null && !(ignoreCase ? (ownedParameterNames.get(j)).equalsIgnoreCase(ownedParameter.getName()) : ownedParameterNames.get(j).equals(ownedParameter.getName()))) {
					continue flushServicesLoop;
				}
				if (ownedParameterTypes != null && !ownedParameterTypes.get(j).equals(ownedParameter.getType())) {
					continue flushServicesLoop;
				}
			}
			return flushServices;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<BehavioralFeature> getSignalServices() {
		if (signalServices == null) {
			signalServices = new EObjectResolvingEList<BehavioralFeature>(BehavioralFeature.class, this, SW_InteractionPackage.NOTIFICATION_RESOURCE__SIGNAL_SERVICES);
		}
		return signalServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getSignalServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes) {
		return getSignalServices(name, ownedParameterNames, ownedParameterTypes, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getSignalServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass) {
		signalServicesLoop: for (BehavioralFeature signalServices : getSignalServices()) {
			if ((eClass != null && !eClass.isInstance(signalServices)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(signalServices.getName()) : name.equals(signalServices.getName())))) {
				continue signalServicesLoop;
			}
			EList<Parameter> ownedParameterList = signalServices.getOwnedParameters();
			int ownedParameterListSize = ownedParameterList.size();
			if (ownedParameterNames != null && ownedParameterNames.size() != ownedParameterListSize || (ownedParameterTypes != null && ownedParameterTypes.size() != ownedParameterListSize)) {
				continue signalServicesLoop;
			}
			for (int j = 0; j < ownedParameterListSize; j++) {
				Parameter ownedParameter = ownedParameterList.get(j);
				if (ownedParameterNames != null && !(ignoreCase ? (ownedParameterNames.get(j)).equalsIgnoreCase(ownedParameter.getName()) : ownedParameterNames.get(j).equals(ownedParameter.getName()))) {
					continue signalServicesLoop;
				}
				if (ownedParameterTypes != null && !ownedParameterTypes.get(j).equals(ownedParameter.getType())) {
					continue signalServicesLoop;
				}
			}
			return signalServices;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<BehavioralFeature> getWaitServices() {
		if (waitServices == null) {
			waitServices = new EObjectResolvingEList<BehavioralFeature>(BehavioralFeature.class, this, SW_InteractionPackage.NOTIFICATION_RESOURCE__WAIT_SERVICES);
		}
		return waitServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getWaitServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes) {
		return getWaitServices(name, ownedParameterNames, ownedParameterTypes, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getWaitServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass) {
		waitServicesLoop: for (BehavioralFeature waitServices : getWaitServices()) {
			if ((eClass != null && !eClass.isInstance(waitServices)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(waitServices.getName()) : name.equals(waitServices.getName())))) {
				continue waitServicesLoop;
			}
			EList<Parameter> ownedParameterList = waitServices.getOwnedParameters();
			int ownedParameterListSize = ownedParameterList.size();
			if (ownedParameterNames != null && ownedParameterNames.size() != ownedParameterListSize || (ownedParameterTypes != null && ownedParameterTypes.size() != ownedParameterListSize)) {
				continue waitServicesLoop;
			}
			for (int j = 0; j < ownedParameterListSize; j++) {
				Parameter ownedParameter = ownedParameterList.get(j);
				if (ownedParameterNames != null && !(ignoreCase ? (ownedParameterNames.get(j)).equalsIgnoreCase(ownedParameter.getName()) : ownedParameterNames.get(j).equals(ownedParameter.getName()))) {
					continue waitServicesLoop;
				}
				if (ownedParameterTypes != null && !ownedParameterTypes.get(j).equals(ownedParameter.getType())) {
					continue waitServicesLoop;
				}
			}
			return waitServices;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<BehavioralFeature> getClearServices() {
		if (clearServices == null) {
			clearServices = new EObjectResolvingEList<BehavioralFeature>(BehavioralFeature.class, this, SW_InteractionPackage.NOTIFICATION_RESOURCE__CLEAR_SERVICES);
		}
		return clearServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getClearServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes) {
		return getClearServices(name, ownedParameterNames, ownedParameterTypes, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getClearServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass) {
		clearServicesLoop: for (BehavioralFeature clearServices : getClearServices()) {
			if ((eClass != null && !eClass.isInstance(clearServices)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(clearServices.getName()) : name.equals(clearServices.getName())))) {
				continue clearServicesLoop;
			}
			EList<Parameter> ownedParameterList = clearServices.getOwnedParameters();
			int ownedParameterListSize = ownedParameterList.size();
			if (ownedParameterNames != null && ownedParameterNames.size() != ownedParameterListSize || (ownedParameterTypes != null && ownedParameterTypes.size() != ownedParameterListSize)) {
				continue clearServicesLoop;
			}
			for (int j = 0; j < ownedParameterListSize; j++) {
				Parameter ownedParameter = ownedParameterList.get(j);
				if (ownedParameterNames != null && !(ignoreCase ? (ownedParameterNames.get(j)).equalsIgnoreCase(ownedParameter.getName()) : ownedParameterNames.get(j).equals(ownedParameter.getName()))) {
					continue clearServicesLoop;
				}
				if (ownedParameterTypes != null && !ownedParameterTypes.get(j).equals(ownedParameter.getType())) {
					continue clearServicesLoop;
				}
			}
			return clearServices;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__OCCURENCE:
			return getOccurence();
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__MECHANISM:
			return getMechanism();
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__OCCURENCE_COUNT_ELEMENTS:
			return getOccurenceCountElements();
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__MASK_ELEMENTS:
			return getMaskElements();
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__FLUSH_SERVICES:
			return getFlushServices();
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__SIGNAL_SERVICES:
			return getSignalServices();
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__WAIT_SERVICES:
			return getWaitServices();
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__CLEAR_SERVICES:
			return getClearServices();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__OCCURENCE:
			setOccurence((NotificationKind) newValue);
			return;
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__MECHANISM:
			setMechanism((NotificationResourceKind) newValue);
			return;
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__OCCURENCE_COUNT_ELEMENTS:
			getOccurenceCountElements().clear();
			getOccurenceCountElements().addAll((Collection<? extends TypedElement>) newValue);
			return;
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__MASK_ELEMENTS:
			getMaskElements().clear();
			getMaskElements().addAll((Collection<? extends TypedElement>) newValue);
			return;
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__FLUSH_SERVICES:
			getFlushServices().clear();
			getFlushServices().addAll((Collection<? extends BehavioralFeature>) newValue);
			return;
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__SIGNAL_SERVICES:
			getSignalServices().clear();
			getSignalServices().addAll((Collection<? extends BehavioralFeature>) newValue);
			return;
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__WAIT_SERVICES:
			getWaitServices().clear();
			getWaitServices().addAll((Collection<? extends BehavioralFeature>) newValue);
			return;
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__CLEAR_SERVICES:
			getClearServices().clear();
			getClearServices().addAll((Collection<? extends BehavioralFeature>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__OCCURENCE:
			setOccurence(OCCURENCE_EDEFAULT);
			return;
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__MECHANISM:
			setMechanism(MECHANISM_EDEFAULT);
			return;
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__OCCURENCE_COUNT_ELEMENTS:
			getOccurenceCountElements().clear();
			return;
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__MASK_ELEMENTS:
			getMaskElements().clear();
			return;
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__FLUSH_SERVICES:
			getFlushServices().clear();
			return;
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__SIGNAL_SERVICES:
			getSignalServices().clear();
			return;
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__WAIT_SERVICES:
			getWaitServices().clear();
			return;
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__CLEAR_SERVICES:
			getClearServices().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__OCCURENCE:
			return occurence != OCCURENCE_EDEFAULT;
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__MECHANISM:
			return mechanism != MECHANISM_EDEFAULT;
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__OCCURENCE_COUNT_ELEMENTS:
			return occurenceCountElements != null && !occurenceCountElements.isEmpty();
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__MASK_ELEMENTS:
			return maskElements != null && !maskElements.isEmpty();
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__FLUSH_SERVICES:
			return flushServices != null && !flushServices.isEmpty();
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__SIGNAL_SERVICES:
			return signalServices != null && !signalServices.isEmpty();
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__WAIT_SERVICES:
			return waitServices != null && !waitServices.isEmpty();
		case SW_InteractionPackage.NOTIFICATION_RESOURCE__CLEAR_SERVICES:
			return clearServices != null && !clearServices.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) {
			return super.toString();
		}

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (occurence: "); //$NON-NLS-1$
		result.append(occurence);
		result.append(", mechanism: "); //$NON-NLS-1$
		result.append(mechanism);
		result.append(')');
		return result.toString();
	}

} // NotificationResourceImpl

/**
 * Copyright (c) 2010, 2016 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Chokri MRAIDHA (CEA LIST) chokri.mraidha@cea.fr - Initial API and implementation
 *
 */
package org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.AccessPolicyKind;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.MemoryBroker;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.SW_BrokeringPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.impl.SwResourceImpl;

import org.eclipse.uml2.uml.BehavioralFeature;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.TypedElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Memory Broker</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.impl.MemoryBrokerImpl#getAccessPolicy <em>Access Policy</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.impl.MemoryBrokerImpl#getMemories <em>Memories</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.impl.MemoryBrokerImpl#getMemoryBlockAdressElements <em>Memory Block Adress Elements</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.impl.MemoryBrokerImpl#getMemoryBlockSizeElements <em>Memory Block Size Elements</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.impl.MemoryBrokerImpl#getLockServices <em>Lock Services</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.impl.MemoryBrokerImpl#getUnlockServices <em>Unlock Services</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.impl.MemoryBrokerImpl#getMapServices <em>Map Services</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.impl.MemoryBrokerImpl#getUnMapServices <em>Un Map Services</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MemoryBrokerImpl extends SwResourceImpl implements MemoryBroker {
	/**
	 * The default value of the '{@link #getAccessPolicy() <em>Access Policy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getAccessPolicy()
	 * @generated
	 * @ordered
	 */
	protected static final AccessPolicyKind ACCESS_POLICY_EDEFAULT = AccessPolicyKind.READ;

	/**
	 * The cached value of the '{@link #getAccessPolicy() <em>Access Policy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getAccessPolicy()
	 * @generated
	 * @ordered
	 */
	protected AccessPolicyKind accessPolicy = ACCESS_POLICY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMemories() <em>Memories</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getMemories()
	 * @generated
	 * @ordered
	 */
	protected EList<TypedElement> memories;

	/**
	 * The cached value of the '{@link #getMemoryBlockAdressElements() <em>Memory Block Adress Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getMemoryBlockAdressElements()
	 * @generated
	 * @ordered
	 */
	protected EList<TypedElement> memoryBlockAdressElements;

	/**
	 * The cached value of the '{@link #getMemoryBlockSizeElements() <em>Memory Block Size Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getMemoryBlockSizeElements()
	 * @generated
	 * @ordered
	 */
	protected EList<TypedElement> memoryBlockSizeElements;

	/**
	 * The cached value of the '{@link #getLockServices() <em>Lock Services</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getLockServices()
	 * @generated
	 * @ordered
	 */
	protected EList<BehavioralFeature> lockServices;

	/**
	 * The cached value of the '{@link #getUnlockServices() <em>Unlock Services</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getUnlockServices()
	 * @generated
	 * @ordered
	 */
	protected EList<BehavioralFeature> unlockServices;

	/**
	 * The cached value of the '{@link #getMapServices() <em>Map Services</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getMapServices()
	 * @generated
	 * @ordered
	 */
	protected EList<BehavioralFeature> mapServices;

	/**
	 * The cached value of the '{@link #getUnMapServices() <em>Un Map Services</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getUnMapServices()
	 * @generated
	 * @ordered
	 */
	protected EList<BehavioralFeature> unMapServices;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	protected MemoryBrokerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SW_BrokeringPackage.Literals.MEMORY_BROKER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public AccessPolicyKind getAccessPolicy() {
		return accessPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public void setAccessPolicy(AccessPolicyKind newAccessPolicy) {
		AccessPolicyKind oldAccessPolicy = accessPolicy;
		accessPolicy = newAccessPolicy == null ? ACCESS_POLICY_EDEFAULT : newAccessPolicy;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, SW_BrokeringPackage.MEMORY_BROKER__ACCESS_POLICY, oldAccessPolicy, accessPolicy));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<TypedElement> getMemories() {
		if (memories == null) {
			memories = new EObjectResolvingEList<TypedElement>(TypedElement.class, this, SW_BrokeringPackage.MEMORY_BROKER__MEMORIES);
		}
		return memories;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public TypedElement getMemories(String name, Type type) {
		return getMemories(name, type, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public TypedElement getMemories(String name, Type type, boolean ignoreCase, EClass eClass) {
		memoriesLoop: for (TypedElement memories : getMemories()) {
			if ((eClass != null && !eClass.isInstance(memories)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(memories.getName()) : name.equals(memories.getName())))) {
				continue memoriesLoop;
			}
			if (type != null && !type.equals(memories.getType())) {
				continue memoriesLoop;
			}
			return memories;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<TypedElement> getMemoryBlockAdressElements() {
		if (memoryBlockAdressElements == null) {
			memoryBlockAdressElements = new EObjectResolvingEList<TypedElement>(TypedElement.class, this, SW_BrokeringPackage.MEMORY_BROKER__MEMORY_BLOCK_ADRESS_ELEMENTS);
		}
		return memoryBlockAdressElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public TypedElement getMemoryBlockAdressElements(String name, Type type) {
		return getMemoryBlockAdressElements(name, type, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public TypedElement getMemoryBlockAdressElements(String name, Type type, boolean ignoreCase, EClass eClass) {
		memoryBlockAdressElementsLoop: for (TypedElement memoryBlockAdressElements : getMemoryBlockAdressElements()) {
			if ((eClass != null && !eClass.isInstance(memoryBlockAdressElements)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(memoryBlockAdressElements.getName()) : name.equals(memoryBlockAdressElements.getName())))) {
				continue memoryBlockAdressElementsLoop;
			}
			if (type != null && !type.equals(memoryBlockAdressElements.getType())) {
				continue memoryBlockAdressElementsLoop;
			}
			return memoryBlockAdressElements;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<TypedElement> getMemoryBlockSizeElements() {
		if (memoryBlockSizeElements == null) {
			memoryBlockSizeElements = new EObjectResolvingEList<TypedElement>(TypedElement.class, this, SW_BrokeringPackage.MEMORY_BROKER__MEMORY_BLOCK_SIZE_ELEMENTS);
		}
		return memoryBlockSizeElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public TypedElement getMemoryBlockSizeElements(String name, Type type) {
		return getMemoryBlockSizeElements(name, type, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public TypedElement getMemoryBlockSizeElements(String name, Type type, boolean ignoreCase, EClass eClass) {
		memoryBlockSizeElementsLoop: for (TypedElement memoryBlockSizeElements : getMemoryBlockSizeElements()) {
			if ((eClass != null && !eClass.isInstance(memoryBlockSizeElements)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(memoryBlockSizeElements.getName()) : name.equals(memoryBlockSizeElements.getName())))) {
				continue memoryBlockSizeElementsLoop;
			}
			if (type != null && !type.equals(memoryBlockSizeElements.getType())) {
				continue memoryBlockSizeElementsLoop;
			}
			return memoryBlockSizeElements;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<BehavioralFeature> getLockServices() {
		if (lockServices == null) {
			lockServices = new EObjectResolvingEList<BehavioralFeature>(BehavioralFeature.class, this, SW_BrokeringPackage.MEMORY_BROKER__LOCK_SERVICES);
		}
		return lockServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getLockServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes) {
		return getLockServices(name, ownedParameterNames, ownedParameterTypes, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getLockServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass) {
		lockServicesLoop: for (BehavioralFeature lockServices : getLockServices()) {
			if ((eClass != null && !eClass.isInstance(lockServices)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(lockServices.getName()) : name.equals(lockServices.getName())))) {
				continue lockServicesLoop;
			}
			EList<Parameter> ownedParameterList = lockServices.getOwnedParameters();
			int ownedParameterListSize = ownedParameterList.size();
			if (ownedParameterNames != null && ownedParameterNames.size() != ownedParameterListSize || (ownedParameterTypes != null && ownedParameterTypes.size() != ownedParameterListSize)) {
				continue lockServicesLoop;
			}
			for (int j = 0; j < ownedParameterListSize; j++) {
				Parameter ownedParameter = ownedParameterList.get(j);
				if (ownedParameterNames != null && !(ignoreCase ? (ownedParameterNames.get(j)).equalsIgnoreCase(ownedParameter.getName()) : ownedParameterNames.get(j).equals(ownedParameter.getName()))) {
					continue lockServicesLoop;
				}
				if (ownedParameterTypes != null && !ownedParameterTypes.get(j).equals(ownedParameter.getType())) {
					continue lockServicesLoop;
				}
			}
			return lockServices;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<BehavioralFeature> getUnlockServices() {
		if (unlockServices == null) {
			unlockServices = new EObjectResolvingEList<BehavioralFeature>(BehavioralFeature.class, this, SW_BrokeringPackage.MEMORY_BROKER__UNLOCK_SERVICES);
		}
		return unlockServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getUnlockServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes) {
		return getUnlockServices(name, ownedParameterNames, ownedParameterTypes, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getUnlockServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass) {
		unlockServicesLoop: for (BehavioralFeature unlockServices : getUnlockServices()) {
			if ((eClass != null && !eClass.isInstance(unlockServices)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(unlockServices.getName()) : name.equals(unlockServices.getName())))) {
				continue unlockServicesLoop;
			}
			EList<Parameter> ownedParameterList = unlockServices.getOwnedParameters();
			int ownedParameterListSize = ownedParameterList.size();
			if (ownedParameterNames != null && ownedParameterNames.size() != ownedParameterListSize || (ownedParameterTypes != null && ownedParameterTypes.size() != ownedParameterListSize)) {
				continue unlockServicesLoop;
			}
			for (int j = 0; j < ownedParameterListSize; j++) {
				Parameter ownedParameter = ownedParameterList.get(j);
				if (ownedParameterNames != null && !(ignoreCase ? (ownedParameterNames.get(j)).equalsIgnoreCase(ownedParameter.getName()) : ownedParameterNames.get(j).equals(ownedParameter.getName()))) {
					continue unlockServicesLoop;
				}
				if (ownedParameterTypes != null && !ownedParameterTypes.get(j).equals(ownedParameter.getType())) {
					continue unlockServicesLoop;
				}
			}
			return unlockServices;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<BehavioralFeature> getMapServices() {
		if (mapServices == null) {
			mapServices = new EObjectResolvingEList<BehavioralFeature>(BehavioralFeature.class, this, SW_BrokeringPackage.MEMORY_BROKER__MAP_SERVICES);
		}
		return mapServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getMapServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes) {
		return getMapServices(name, ownedParameterNames, ownedParameterTypes, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getMapServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass) {
		mapServicesLoop: for (BehavioralFeature mapServices : getMapServices()) {
			if ((eClass != null && !eClass.isInstance(mapServices)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(mapServices.getName()) : name.equals(mapServices.getName())))) {
				continue mapServicesLoop;
			}
			EList<Parameter> ownedParameterList = mapServices.getOwnedParameters();
			int ownedParameterListSize = ownedParameterList.size();
			if (ownedParameterNames != null && ownedParameterNames.size() != ownedParameterListSize || (ownedParameterTypes != null && ownedParameterTypes.size() != ownedParameterListSize)) {
				continue mapServicesLoop;
			}
			for (int j = 0; j < ownedParameterListSize; j++) {
				Parameter ownedParameter = ownedParameterList.get(j);
				if (ownedParameterNames != null && !(ignoreCase ? (ownedParameterNames.get(j)).equalsIgnoreCase(ownedParameter.getName()) : ownedParameterNames.get(j).equals(ownedParameter.getName()))) {
					continue mapServicesLoop;
				}
				if (ownedParameterTypes != null && !ownedParameterTypes.get(j).equals(ownedParameter.getType())) {
					continue mapServicesLoop;
				}
			}
			return mapServices;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<BehavioralFeature> getUnMapServices() {
		if (unMapServices == null) {
			unMapServices = new EObjectResolvingEList<BehavioralFeature>(BehavioralFeature.class, this, SW_BrokeringPackage.MEMORY_BROKER__UN_MAP_SERVICES);
		}
		return unMapServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getUnMapServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes) {
		return getUnMapServices(name, ownedParameterNames, ownedParameterTypes, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getUnMapServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass) {
		unMapServicesLoop: for (BehavioralFeature unMapServices : getUnMapServices()) {
			if ((eClass != null && !eClass.isInstance(unMapServices)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(unMapServices.getName()) : name.equals(unMapServices.getName())))) {
				continue unMapServicesLoop;
			}
			EList<Parameter> ownedParameterList = unMapServices.getOwnedParameters();
			int ownedParameterListSize = ownedParameterList.size();
			if (ownedParameterNames != null && ownedParameterNames.size() != ownedParameterListSize || (ownedParameterTypes != null && ownedParameterTypes.size() != ownedParameterListSize)) {
				continue unMapServicesLoop;
			}
			for (int j = 0; j < ownedParameterListSize; j++) {
				Parameter ownedParameter = ownedParameterList.get(j);
				if (ownedParameterNames != null && !(ignoreCase ? (ownedParameterNames.get(j)).equalsIgnoreCase(ownedParameter.getName()) : ownedParameterNames.get(j).equals(ownedParameter.getName()))) {
					continue unMapServicesLoop;
				}
				if (ownedParameterTypes != null && !ownedParameterTypes.get(j).equals(ownedParameter.getType())) {
					continue unMapServicesLoop;
				}
			}
			return unMapServices;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SW_BrokeringPackage.MEMORY_BROKER__ACCESS_POLICY:
			return getAccessPolicy();
		case SW_BrokeringPackage.MEMORY_BROKER__MEMORIES:
			return getMemories();
		case SW_BrokeringPackage.MEMORY_BROKER__MEMORY_BLOCK_ADRESS_ELEMENTS:
			return getMemoryBlockAdressElements();
		case SW_BrokeringPackage.MEMORY_BROKER__MEMORY_BLOCK_SIZE_ELEMENTS:
			return getMemoryBlockSizeElements();
		case SW_BrokeringPackage.MEMORY_BROKER__LOCK_SERVICES:
			return getLockServices();
		case SW_BrokeringPackage.MEMORY_BROKER__UNLOCK_SERVICES:
			return getUnlockServices();
		case SW_BrokeringPackage.MEMORY_BROKER__MAP_SERVICES:
			return getMapServices();
		case SW_BrokeringPackage.MEMORY_BROKER__UN_MAP_SERVICES:
			return getUnMapServices();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SW_BrokeringPackage.MEMORY_BROKER__ACCESS_POLICY:
			setAccessPolicy((AccessPolicyKind) newValue);
			return;
		case SW_BrokeringPackage.MEMORY_BROKER__MEMORIES:
			getMemories().clear();
			getMemories().addAll((Collection<? extends TypedElement>) newValue);
			return;
		case SW_BrokeringPackage.MEMORY_BROKER__MEMORY_BLOCK_ADRESS_ELEMENTS:
			getMemoryBlockAdressElements().clear();
			getMemoryBlockAdressElements().addAll((Collection<? extends TypedElement>) newValue);
			return;
		case SW_BrokeringPackage.MEMORY_BROKER__MEMORY_BLOCK_SIZE_ELEMENTS:
			getMemoryBlockSizeElements().clear();
			getMemoryBlockSizeElements().addAll((Collection<? extends TypedElement>) newValue);
			return;
		case SW_BrokeringPackage.MEMORY_BROKER__LOCK_SERVICES:
			getLockServices().clear();
			getLockServices().addAll((Collection<? extends BehavioralFeature>) newValue);
			return;
		case SW_BrokeringPackage.MEMORY_BROKER__UNLOCK_SERVICES:
			getUnlockServices().clear();
			getUnlockServices().addAll((Collection<? extends BehavioralFeature>) newValue);
			return;
		case SW_BrokeringPackage.MEMORY_BROKER__MAP_SERVICES:
			getMapServices().clear();
			getMapServices().addAll((Collection<? extends BehavioralFeature>) newValue);
			return;
		case SW_BrokeringPackage.MEMORY_BROKER__UN_MAP_SERVICES:
			getUnMapServices().clear();
			getUnMapServices().addAll((Collection<? extends BehavioralFeature>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SW_BrokeringPackage.MEMORY_BROKER__ACCESS_POLICY:
			setAccessPolicy(ACCESS_POLICY_EDEFAULT);
			return;
		case SW_BrokeringPackage.MEMORY_BROKER__MEMORIES:
			getMemories().clear();
			return;
		case SW_BrokeringPackage.MEMORY_BROKER__MEMORY_BLOCK_ADRESS_ELEMENTS:
			getMemoryBlockAdressElements().clear();
			return;
		case SW_BrokeringPackage.MEMORY_BROKER__MEMORY_BLOCK_SIZE_ELEMENTS:
			getMemoryBlockSizeElements().clear();
			return;
		case SW_BrokeringPackage.MEMORY_BROKER__LOCK_SERVICES:
			getLockServices().clear();
			return;
		case SW_BrokeringPackage.MEMORY_BROKER__UNLOCK_SERVICES:
			getUnlockServices().clear();
			return;
		case SW_BrokeringPackage.MEMORY_BROKER__MAP_SERVICES:
			getMapServices().clear();
			return;
		case SW_BrokeringPackage.MEMORY_BROKER__UN_MAP_SERVICES:
			getUnMapServices().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SW_BrokeringPackage.MEMORY_BROKER__ACCESS_POLICY:
			return accessPolicy != ACCESS_POLICY_EDEFAULT;
		case SW_BrokeringPackage.MEMORY_BROKER__MEMORIES:
			return memories != null && !memories.isEmpty();
		case SW_BrokeringPackage.MEMORY_BROKER__MEMORY_BLOCK_ADRESS_ELEMENTS:
			return memoryBlockAdressElements != null && !memoryBlockAdressElements.isEmpty();
		case SW_BrokeringPackage.MEMORY_BROKER__MEMORY_BLOCK_SIZE_ELEMENTS:
			return memoryBlockSizeElements != null && !memoryBlockSizeElements.isEmpty();
		case SW_BrokeringPackage.MEMORY_BROKER__LOCK_SERVICES:
			return lockServices != null && !lockServices.isEmpty();
		case SW_BrokeringPackage.MEMORY_BROKER__UNLOCK_SERVICES:
			return unlockServices != null && !unlockServices.isEmpty();
		case SW_BrokeringPackage.MEMORY_BROKER__MAP_SERVICES:
			return mapServices != null && !mapServices.isEmpty();
		case SW_BrokeringPackage.MEMORY_BROKER__UN_MAP_SERVICES:
			return unMapServices != null && !unMapServices.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) {
			return super.toString();
		}

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (accessPolicy: "); //$NON-NLS-1$
		result.append(accessPolicy);
		result.append(')');
		return result.toString();
	}

} // MemoryBrokerImpl

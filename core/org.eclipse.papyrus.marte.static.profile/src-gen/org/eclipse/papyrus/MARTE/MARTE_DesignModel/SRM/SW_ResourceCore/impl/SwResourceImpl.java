/**
 * Copyright (c) 2010, 2016 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Chokri MRAIDHA (CEA LIST) chokri.mraidha@cea.fr - Initial API and implementation
 *
 */
package org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SW_ResourceCorePackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SwResource;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.impl.ResourceImpl;

import org.eclipse.uml2.uml.BehavioralFeature;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.TypedElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sw Resource</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.impl.SwResourceImpl#getIdentifierElements <em>Identifier Elements</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.impl.SwResourceImpl#getStateElements <em>State Elements</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.impl.SwResourceImpl#getMemorySizeFootprint <em>Memory Size Footprint</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.impl.SwResourceImpl#getCreateServices <em>Create Services</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.impl.SwResourceImpl#getDeleteServices <em>Delete Services</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.impl.SwResourceImpl#getInitializeServices <em>Initialize Services</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class SwResourceImpl extends ResourceImpl implements SwResource {
	/**
	 * The cached value of the '{@link #getIdentifierElements() <em>Identifier Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getIdentifierElements()
	 * @generated
	 * @ordered
	 */
	protected EList<TypedElement> identifierElements;

	/**
	 * The cached value of the '{@link #getStateElements() <em>State Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getStateElements()
	 * @generated
	 * @ordered
	 */
	protected EList<TypedElement> stateElements;

	/**
	 * The cached value of the '{@link #getMemorySizeFootprint() <em>Memory Size Footprint</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getMemorySizeFootprint()
	 * @generated
	 * @ordered
	 */
	protected TypedElement memorySizeFootprint;

	/**
	 * The cached value of the '{@link #getCreateServices() <em>Create Services</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getCreateServices()
	 * @generated
	 * @ordered
	 */
	protected EList<BehavioralFeature> createServices;

	/**
	 * The cached value of the '{@link #getDeleteServices() <em>Delete Services</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getDeleteServices()
	 * @generated
	 * @ordered
	 */
	protected EList<BehavioralFeature> deleteServices;

	/**
	 * The cached value of the '{@link #getInitializeServices() <em>Initialize Services</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #getInitializeServices()
	 * @generated
	 * @ordered
	 */
	protected EList<BehavioralFeature> initializeServices;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	protected SwResourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SW_ResourceCorePackage.Literals.SW_RESOURCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<TypedElement> getIdentifierElements() {
		if (identifierElements == null) {
			identifierElements = new EObjectResolvingEList<TypedElement>(TypedElement.class, this, SW_ResourceCorePackage.SW_RESOURCE__IDENTIFIER_ELEMENTS);
		}
		return identifierElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public TypedElement getIdentifierElements(String name, Type type) {
		return getIdentifierElements(name, type, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public TypedElement getIdentifierElements(String name, Type type, boolean ignoreCase, EClass eClass) {
		identifierElementsLoop: for (TypedElement identifierElements : getIdentifierElements()) {
			if ((eClass != null && !eClass.isInstance(identifierElements)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(identifierElements.getName()) : name.equals(identifierElements.getName())))) {
				continue identifierElementsLoop;
			}
			if (type != null && !type.equals(identifierElements.getType())) {
				continue identifierElementsLoop;
			}
			return identifierElements;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<TypedElement> getStateElements() {
		if (stateElements == null) {
			stateElements = new EObjectResolvingEList<TypedElement>(TypedElement.class, this, SW_ResourceCorePackage.SW_RESOURCE__STATE_ELEMENTS);
		}
		return stateElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public TypedElement getStateElements(String name, Type type) {
		return getStateElements(name, type, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public TypedElement getStateElements(String name, Type type, boolean ignoreCase, EClass eClass) {
		stateElementsLoop: for (TypedElement stateElements : getStateElements()) {
			if ((eClass != null && !eClass.isInstance(stateElements)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(stateElements.getName()) : name.equals(stateElements.getName())))) {
				continue stateElementsLoop;
			}
			if (type != null && !type.equals(stateElements.getType())) {
				continue stateElementsLoop;
			}
			return stateElements;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public TypedElement getMemorySizeFootprint() {
		if (memorySizeFootprint != null && memorySizeFootprint.eIsProxy()) {
			InternalEObject oldMemorySizeFootprint = (InternalEObject) memorySizeFootprint;
			memorySizeFootprint = (TypedElement) eResolveProxy(oldMemorySizeFootprint);
			if (memorySizeFootprint != oldMemorySizeFootprint) {
				if (eNotificationRequired()) {
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SW_ResourceCorePackage.SW_RESOURCE__MEMORY_SIZE_FOOTPRINT, oldMemorySizeFootprint, memorySizeFootprint));
				}
			}
		}
		return memorySizeFootprint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	public TypedElement basicGetMemorySizeFootprint() {
		return memorySizeFootprint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public void setMemorySizeFootprint(TypedElement newMemorySizeFootprint) {
		TypedElement oldMemorySizeFootprint = memorySizeFootprint;
		memorySizeFootprint = newMemorySizeFootprint;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, SW_ResourceCorePackage.SW_RESOURCE__MEMORY_SIZE_FOOTPRINT, oldMemorySizeFootprint, memorySizeFootprint));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<BehavioralFeature> getCreateServices() {
		if (createServices == null) {
			createServices = new EObjectResolvingEList<BehavioralFeature>(BehavioralFeature.class, this, SW_ResourceCorePackage.SW_RESOURCE__CREATE_SERVICES);
		}
		return createServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getCreateServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes) {
		return getCreateServices(name, ownedParameterNames, ownedParameterTypes, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getCreateServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass) {
		createServicesLoop: for (BehavioralFeature createServices : getCreateServices()) {
			if ((eClass != null && !eClass.isInstance(createServices)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(createServices.getName()) : name.equals(createServices.getName())))) {
				continue createServicesLoop;
			}
			EList<Parameter> ownedParameterList = createServices.getOwnedParameters();
			int ownedParameterListSize = ownedParameterList.size();
			if (ownedParameterNames != null && ownedParameterNames.size() != ownedParameterListSize || (ownedParameterTypes != null && ownedParameterTypes.size() != ownedParameterListSize)) {
				continue createServicesLoop;
			}
			for (int j = 0; j < ownedParameterListSize; j++) {
				Parameter ownedParameter = ownedParameterList.get(j);
				if (ownedParameterNames != null && !(ignoreCase ? (ownedParameterNames.get(j)).equalsIgnoreCase(ownedParameter.getName()) : ownedParameterNames.get(j).equals(ownedParameter.getName()))) {
					continue createServicesLoop;
				}
				if (ownedParameterTypes != null && !ownedParameterTypes.get(j).equals(ownedParameter.getType())) {
					continue createServicesLoop;
				}
			}
			return createServices;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<BehavioralFeature> getDeleteServices() {
		if (deleteServices == null) {
			deleteServices = new EObjectResolvingEList<BehavioralFeature>(BehavioralFeature.class, this, SW_ResourceCorePackage.SW_RESOURCE__DELETE_SERVICES);
		}
		return deleteServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getDeleteServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes) {
		return getDeleteServices(name, ownedParameterNames, ownedParameterTypes, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getDeleteServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass) {
		deleteServicesLoop: for (BehavioralFeature deleteServices : getDeleteServices()) {
			if ((eClass != null && !eClass.isInstance(deleteServices)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(deleteServices.getName()) : name.equals(deleteServices.getName())))) {
				continue deleteServicesLoop;
			}
			EList<Parameter> ownedParameterList = deleteServices.getOwnedParameters();
			int ownedParameterListSize = ownedParameterList.size();
			if (ownedParameterNames != null && ownedParameterNames.size() != ownedParameterListSize || (ownedParameterTypes != null && ownedParameterTypes.size() != ownedParameterListSize)) {
				continue deleteServicesLoop;
			}
			for (int j = 0; j < ownedParameterListSize; j++) {
				Parameter ownedParameter = ownedParameterList.get(j);
				if (ownedParameterNames != null && !(ignoreCase ? (ownedParameterNames.get(j)).equalsIgnoreCase(ownedParameter.getName()) : ownedParameterNames.get(j).equals(ownedParameter.getName()))) {
					continue deleteServicesLoop;
				}
				if (ownedParameterTypes != null && !ownedParameterTypes.get(j).equals(ownedParameter.getType())) {
					continue deleteServicesLoop;
				}
			}
			return deleteServices;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EList<BehavioralFeature> getInitializeServices() {
		if (initializeServices == null) {
			initializeServices = new EObjectResolvingEList<BehavioralFeature>(BehavioralFeature.class, this, SW_ResourceCorePackage.SW_RESOURCE__INITIALIZE_SERVICES);
		}
		return initializeServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getInitializeServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes) {
		return getInitializeServices(name, ownedParameterNames, ownedParameterTypes, false, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BehavioralFeature getInitializeServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass) {
		initializeServicesLoop: for (BehavioralFeature initializeServices : getInitializeServices()) {
			if ((eClass != null && !eClass.isInstance(initializeServices)) || (name != null && !(ignoreCase ? name.equalsIgnoreCase(initializeServices.getName()) : name.equals(initializeServices.getName())))) {
				continue initializeServicesLoop;
			}
			EList<Parameter> ownedParameterList = initializeServices.getOwnedParameters();
			int ownedParameterListSize = ownedParameterList.size();
			if (ownedParameterNames != null && ownedParameterNames.size() != ownedParameterListSize || (ownedParameterTypes != null && ownedParameterTypes.size() != ownedParameterListSize)) {
				continue initializeServicesLoop;
			}
			for (int j = 0; j < ownedParameterListSize; j++) {
				Parameter ownedParameter = ownedParameterList.get(j);
				if (ownedParameterNames != null && !(ignoreCase ? (ownedParameterNames.get(j)).equalsIgnoreCase(ownedParameter.getName()) : ownedParameterNames.get(j).equals(ownedParameter.getName()))) {
					continue initializeServicesLoop;
				}
				if (ownedParameterTypes != null && !ownedParameterTypes.get(j).equals(ownedParameter.getType())) {
					continue initializeServicesLoop;
				}
			}
			return initializeServices;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SW_ResourceCorePackage.SW_RESOURCE__IDENTIFIER_ELEMENTS:
			return getIdentifierElements();
		case SW_ResourceCorePackage.SW_RESOURCE__STATE_ELEMENTS:
			return getStateElements();
		case SW_ResourceCorePackage.SW_RESOURCE__MEMORY_SIZE_FOOTPRINT:
			if (resolve) {
				return getMemorySizeFootprint();
			}
			return basicGetMemorySizeFootprint();
		case SW_ResourceCorePackage.SW_RESOURCE__CREATE_SERVICES:
			return getCreateServices();
		case SW_ResourceCorePackage.SW_RESOURCE__DELETE_SERVICES:
			return getDeleteServices();
		case SW_ResourceCorePackage.SW_RESOURCE__INITIALIZE_SERVICES:
			return getInitializeServices();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SW_ResourceCorePackage.SW_RESOURCE__IDENTIFIER_ELEMENTS:
			getIdentifierElements().clear();
			getIdentifierElements().addAll((Collection<? extends TypedElement>) newValue);
			return;
		case SW_ResourceCorePackage.SW_RESOURCE__STATE_ELEMENTS:
			getStateElements().clear();
			getStateElements().addAll((Collection<? extends TypedElement>) newValue);
			return;
		case SW_ResourceCorePackage.SW_RESOURCE__MEMORY_SIZE_FOOTPRINT:
			setMemorySizeFootprint((TypedElement) newValue);
			return;
		case SW_ResourceCorePackage.SW_RESOURCE__CREATE_SERVICES:
			getCreateServices().clear();
			getCreateServices().addAll((Collection<? extends BehavioralFeature>) newValue);
			return;
		case SW_ResourceCorePackage.SW_RESOURCE__DELETE_SERVICES:
			getDeleteServices().clear();
			getDeleteServices().addAll((Collection<? extends BehavioralFeature>) newValue);
			return;
		case SW_ResourceCorePackage.SW_RESOURCE__INITIALIZE_SERVICES:
			getInitializeServices().clear();
			getInitializeServices().addAll((Collection<? extends BehavioralFeature>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SW_ResourceCorePackage.SW_RESOURCE__IDENTIFIER_ELEMENTS:
			getIdentifierElements().clear();
			return;
		case SW_ResourceCorePackage.SW_RESOURCE__STATE_ELEMENTS:
			getStateElements().clear();
			return;
		case SW_ResourceCorePackage.SW_RESOURCE__MEMORY_SIZE_FOOTPRINT:
			setMemorySizeFootprint((TypedElement) null);
			return;
		case SW_ResourceCorePackage.SW_RESOURCE__CREATE_SERVICES:
			getCreateServices().clear();
			return;
		case SW_ResourceCorePackage.SW_RESOURCE__DELETE_SERVICES:
			getDeleteServices().clear();
			return;
		case SW_ResourceCorePackage.SW_RESOURCE__INITIALIZE_SERVICES:
			getInitializeServices().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SW_ResourceCorePackage.SW_RESOURCE__IDENTIFIER_ELEMENTS:
			return identifierElements != null && !identifierElements.isEmpty();
		case SW_ResourceCorePackage.SW_RESOURCE__STATE_ELEMENTS:
			return stateElements != null && !stateElements.isEmpty();
		case SW_ResourceCorePackage.SW_RESOURCE__MEMORY_SIZE_FOOTPRINT:
			return memorySizeFootprint != null;
		case SW_ResourceCorePackage.SW_RESOURCE__CREATE_SERVICES:
			return createServices != null && !createServices.isEmpty();
		case SW_ResourceCorePackage.SW_RESOURCE__DELETE_SERVICES:
			return deleteServices != null && !deleteServices.isEmpty();
		case SW_ResourceCorePackage.SW_RESOURCE__INITIALIZE_SERVICES:
			return initializeServices != null && !initializeServices.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // SwResourceImpl

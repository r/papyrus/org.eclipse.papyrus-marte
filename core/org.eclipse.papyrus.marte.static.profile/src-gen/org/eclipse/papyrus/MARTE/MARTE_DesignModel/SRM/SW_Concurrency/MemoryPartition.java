/**
 * Copyright (c) 2010, 2016 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Chokri MRAIDHA (CEA LIST) chokri.mraidha@cea.fr - Initial API and implementation
 *
 */
package org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SwResource;

import org.eclipse.uml2.uml.BehavioralFeature;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.TypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Memory Partition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.MemoryPartition#getConcurrentResources <em>Concurrent Resources</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.MemoryPartition#getMemorySpaces <em>Memory Spaces</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.MemoryPartition#getFork <em>Fork</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.MemoryPartition#getExit <em>Exit</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.MemoryPartition#getBase_Namespace <em>Base Namespace</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SW_ConcurrencyPackage#getMemoryPartition()
 * @model
 * @generated
 */
public interface MemoryPartition extends SwResource {
	/**
	 * Returns the value of the '<em><b>Concurrent Resources</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.TypedElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Concurrent Resources</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Concurrent Resources</em>' reference list.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SW_ConcurrencyPackage#getMemoryPartition_ConcurrentResources()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TypedElement> getConcurrentResources();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>' from the '<em><b>Concurrent Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                 The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param type
	 *                 The '<em><b>Type</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>', or <code>null</code>.
	 * @see #getConcurrentResources()
	 * @generated
	 */
	TypedElement getConcurrentResources(String name, Type type);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>' from the '<em><b>Concurrent Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                       The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param type
	 *                       The '<em><b>Type</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param ignoreCase
	 *                       Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass
	 *                       The Ecore class of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>', or <code>null</code>.
	 * @see #getConcurrentResources()
	 * @generated
	 */
	TypedElement getConcurrentResources(String name, Type type, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Memory Spaces</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.TypedElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memory Spaces</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Memory Spaces</em>' reference list.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SW_ConcurrencyPackage#getMemoryPartition_MemorySpaces()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TypedElement> getMemorySpaces();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>' from the '<em><b>Memory Spaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                 The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param type
	 *                 The '<em><b>Type</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>', or <code>null</code>.
	 * @see #getMemorySpaces()
	 * @generated
	 */
	TypedElement getMemorySpaces(String name, Type type);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>' from the '<em><b>Memory Spaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                       The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param type
	 *                       The '<em><b>Type</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param ignoreCase
	 *                       Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass
	 *                       The Ecore class of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>', or <code>null</code>.
	 * @see #getMemorySpaces()
	 * @generated
	 */
	TypedElement getMemorySpaces(String name, Type type, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Fork</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.BehavioralFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fork</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Fork</em>' reference list.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SW_ConcurrencyPackage#getMemoryPartition_Fork()
	 * @model ordered="false"
	 * @generated
	 */
	EList<BehavioralFeature> getFork();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Fork</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getFork()
	 * @generated
	 */
	BehavioralFeature getFork(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Fork</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ignoreCase
	 *                                Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass
	 *                                The Ecore class of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getFork()
	 * @generated
	 */
	BehavioralFeature getFork(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Exit</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.BehavioralFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exit</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Exit</em>' reference list.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SW_ConcurrencyPackage#getMemoryPartition_Exit()
	 * @model ordered="false"
	 * @generated
	 */
	EList<BehavioralFeature> getExit();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Exit</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getExit()
	 * @generated
	 */
	BehavioralFeature getExit(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Exit</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ignoreCase
	 *                                Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass
	 *                                The Ecore class of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getExit()
	 * @generated
	 */
	BehavioralFeature getExit(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Base Namespace</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Namespace</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Base Namespace</em>' reference.
	 * @see #setBase_Namespace(Namespace)
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SW_ConcurrencyPackage#getMemoryPartition_Base_Namespace()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Namespace getBase_Namespace();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.MemoryPartition#getBase_Namespace <em>Base Namespace</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param value
	 *                  the new value of the '<em>Base Namespace</em>' reference.
	 * @see #getBase_Namespace()
	 * @generated
	 */
	void setBase_Namespace(Namespace value);

} // MemoryPartition

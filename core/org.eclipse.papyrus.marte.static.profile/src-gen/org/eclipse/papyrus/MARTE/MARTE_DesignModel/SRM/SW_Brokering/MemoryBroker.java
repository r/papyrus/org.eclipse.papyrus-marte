/**
 * Copyright (c) 2010, 2016 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Chokri MRAIDHA (CEA LIST) chokri.mraidha@cea.fr - Initial API and implementation
 *
 */
package org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SwResource;

import org.eclipse.uml2.uml.BehavioralFeature;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.TypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Memory Broker</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.MemoryBroker#getAccessPolicy <em>Access Policy</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.MemoryBroker#getMemories <em>Memories</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.MemoryBroker#getMemoryBlockAdressElements <em>Memory Block Adress Elements</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.MemoryBroker#getMemoryBlockSizeElements <em>Memory Block Size Elements</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.MemoryBroker#getLockServices <em>Lock Services</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.MemoryBroker#getUnlockServices <em>Unlock Services</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.MemoryBroker#getMapServices <em>Map Services</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.MemoryBroker#getUnMapServices <em>Un Map Services</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.SW_BrokeringPackage#getMemoryBroker()
 * @model
 * @generated
 */
public interface MemoryBroker extends SwResource {
	/**
	 * Returns the value of the '<em><b>Access Policy</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.AccessPolicyKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access Policy</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Access Policy</em>' attribute.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.AccessPolicyKind
	 * @see #setAccessPolicy(AccessPolicyKind)
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.SW_BrokeringPackage#getMemoryBroker_AccessPolicy()
	 * @model unique="false" ordered="false"
	 * @generated
	 */
	AccessPolicyKind getAccessPolicy();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.MemoryBroker#getAccessPolicy <em>Access Policy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param value
	 *                  the new value of the '<em>Access Policy</em>' attribute.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.AccessPolicyKind
	 * @see #getAccessPolicy()
	 * @generated
	 */
	void setAccessPolicy(AccessPolicyKind value);

	/**
	 * Returns the value of the '<em><b>Memories</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.TypedElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memories</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Memories</em>' reference list.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.SW_BrokeringPackage#getMemoryBroker_Memories()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TypedElement> getMemories();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>' from the '<em><b>Memories</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                 The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param type
	 *                 The '<em><b>Type</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>', or <code>null</code>.
	 * @see #getMemories()
	 * @generated
	 */
	TypedElement getMemories(String name, Type type);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>' from the '<em><b>Memories</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                       The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param type
	 *                       The '<em><b>Type</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param ignoreCase
	 *                       Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass
	 *                       The Ecore class of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>', or <code>null</code>.
	 * @see #getMemories()
	 * @generated
	 */
	TypedElement getMemories(String name, Type type, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Memory Block Adress Elements</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.TypedElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memory Block Adress Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Memory Block Adress Elements</em>' reference list.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.SW_BrokeringPackage#getMemoryBroker_MemoryBlockAdressElements()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TypedElement> getMemoryBlockAdressElements();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>' from the '<em><b>Memory Block Adress Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                 The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param type
	 *                 The '<em><b>Type</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>', or <code>null</code>.
	 * @see #getMemoryBlockAdressElements()
	 * @generated
	 */
	TypedElement getMemoryBlockAdressElements(String name, Type type);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>' from the '<em><b>Memory Block Adress Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                       The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param type
	 *                       The '<em><b>Type</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param ignoreCase
	 *                       Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass
	 *                       The Ecore class of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>', or <code>null</code>.
	 * @see #getMemoryBlockAdressElements()
	 * @generated
	 */
	TypedElement getMemoryBlockAdressElements(String name, Type type, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Memory Block Size Elements</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.TypedElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memory Block Size Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Memory Block Size Elements</em>' reference list.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.SW_BrokeringPackage#getMemoryBroker_MemoryBlockSizeElements()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TypedElement> getMemoryBlockSizeElements();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>' from the '<em><b>Memory Block Size Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                 The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param type
	 *                 The '<em><b>Type</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>', or <code>null</code>.
	 * @see #getMemoryBlockSizeElements()
	 * @generated
	 */
	TypedElement getMemoryBlockSizeElements(String name, Type type);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>' from the '<em><b>Memory Block Size Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                       The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param type
	 *                       The '<em><b>Type</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param ignoreCase
	 *                       Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass
	 *                       The Ecore class of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>', or <code>null</code>.
	 * @see #getMemoryBlockSizeElements()
	 * @generated
	 */
	TypedElement getMemoryBlockSizeElements(String name, Type type, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Lock Services</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.BehavioralFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lock Services</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Lock Services</em>' reference list.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.SW_BrokeringPackage#getMemoryBroker_LockServices()
	 * @model ordered="false"
	 * @generated
	 */
	EList<BehavioralFeature> getLockServices();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Lock Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getLockServices()
	 * @generated
	 */
	BehavioralFeature getLockServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Lock Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ignoreCase
	 *                                Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass
	 *                                The Ecore class of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getLockServices()
	 * @generated
	 */
	BehavioralFeature getLockServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Unlock Services</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.BehavioralFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unlock Services</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Unlock Services</em>' reference list.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.SW_BrokeringPackage#getMemoryBroker_UnlockServices()
	 * @model ordered="false"
	 * @generated
	 */
	EList<BehavioralFeature> getUnlockServices();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Unlock Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getUnlockServices()
	 * @generated
	 */
	BehavioralFeature getUnlockServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Unlock Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ignoreCase
	 *                                Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass
	 *                                The Ecore class of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getUnlockServices()
	 * @generated
	 */
	BehavioralFeature getUnlockServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Map Services</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.BehavioralFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Map Services</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Map Services</em>' reference list.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.SW_BrokeringPackage#getMemoryBroker_MapServices()
	 * @model ordered="false"
	 * @generated
	 */
	EList<BehavioralFeature> getMapServices();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Map Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getMapServices()
	 * @generated
	 */
	BehavioralFeature getMapServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Map Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ignoreCase
	 *                                Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass
	 *                                The Ecore class of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getMapServices()
	 * @generated
	 */
	BehavioralFeature getMapServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Un Map Services</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.BehavioralFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Un Map Services</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Un Map Services</em>' reference list.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.SW_BrokeringPackage#getMemoryBroker_UnMapServices()
	 * @model ordered="false"
	 * @generated
	 */
	EList<BehavioralFeature> getUnMapServices();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Un Map Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getUnMapServices()
	 * @generated
	 */
	BehavioralFeature getUnMapServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Un Map Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ignoreCase
	 *                                Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass
	 *                                The Ecore class of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getUnMapServices()
	 * @generated
	 */
	BehavioralFeature getUnMapServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass);

} // MemoryBroker

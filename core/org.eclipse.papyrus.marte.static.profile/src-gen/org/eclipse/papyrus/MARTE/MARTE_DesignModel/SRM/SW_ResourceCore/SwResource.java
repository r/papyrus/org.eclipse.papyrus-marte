/**
 * Copyright (c) 2010, 2016 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Chokri MRAIDHA (CEA LIST) chokri.mraidha@cea.fr - Initial API and implementation
 *
 */
package org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.Resource;

import org.eclipse.uml2.uml.BehavioralFeature;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.TypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sw Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SwResource#getIdentifierElements <em>Identifier Elements</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SwResource#getStateElements <em>State Elements</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SwResource#getMemorySizeFootprint <em>Memory Size Footprint</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SwResource#getCreateServices <em>Create Services</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SwResource#getDeleteServices <em>Delete Services</em>}</li>
 * <li>{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SwResource#getInitializeServices <em>Initialize Services</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SW_ResourceCorePackage#getSwResource()
 * @model abstract="true"
 * @generated
 */
public interface SwResource extends Resource {
	/**
	 * Returns the value of the '<em><b>Identifier Elements</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.TypedElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identifier Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Identifier Elements</em>' reference list.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SW_ResourceCorePackage#getSwResource_IdentifierElements()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TypedElement> getIdentifierElements();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>' from the '<em><b>Identifier Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                 The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param type
	 *                 The '<em><b>Type</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>', or <code>null</code>.
	 * @see #getIdentifierElements()
	 * @generated
	 */
	TypedElement getIdentifierElements(String name, Type type);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>' from the '<em><b>Identifier Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                       The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param type
	 *                       The '<em><b>Type</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param ignoreCase
	 *                       Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass
	 *                       The Ecore class of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>', or <code>null</code>.
	 * @see #getIdentifierElements()
	 * @generated
	 */
	TypedElement getIdentifierElements(String name, Type type, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>State Elements</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.TypedElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>State Elements</em>' reference list.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SW_ResourceCorePackage#getSwResource_StateElements()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TypedElement> getStateElements();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>' from the '<em><b>State Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                 The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param type
	 *                 The '<em><b>Type</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>', or <code>null</code>.
	 * @see #getStateElements()
	 * @generated
	 */
	TypedElement getStateElements(String name, Type type);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>' from the '<em><b>State Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                       The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param type
	 *                       The '<em><b>Type</b></em>' of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @param ignoreCase
	 *                       Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass
	 *                       The Ecore class of the {@link org.eclipse.uml2.uml.TypedElement} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.TypedElement} with the specified '<em><b>Name</b></em>', and '<em><b>Type</b></em>', or <code>null</code>.
	 * @see #getStateElements()
	 * @generated
	 */
	TypedElement getStateElements(String name, Type type, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Memory Size Footprint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memory Size Footprint</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Memory Size Footprint</em>' reference.
	 * @see #setMemorySizeFootprint(TypedElement)
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SW_ResourceCorePackage#getSwResource_MemorySizeFootprint()
	 * @model ordered="false"
	 * @generated
	 */
	TypedElement getMemorySizeFootprint();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SwResource#getMemorySizeFootprint <em>Memory Size Footprint</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param value
	 *                  the new value of the '<em>Memory Size Footprint</em>' reference.
	 * @see #getMemorySizeFootprint()
	 * @generated
	 */
	void setMemorySizeFootprint(TypedElement value);

	/**
	 * Returns the value of the '<em><b>Create Services</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.BehavioralFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Create Services</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Create Services</em>' reference list.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SW_ResourceCorePackage#getSwResource_CreateServices()
	 * @model ordered="false"
	 * @generated
	 */
	EList<BehavioralFeature> getCreateServices();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Create Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getCreateServices()
	 * @generated
	 */
	BehavioralFeature getCreateServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Create Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ignoreCase
	 *                                Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass
	 *                                The Ecore class of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getCreateServices()
	 * @generated
	 */
	BehavioralFeature getCreateServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Delete Services</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.BehavioralFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delete Services</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Delete Services</em>' reference list.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SW_ResourceCorePackage#getSwResource_DeleteServices()
	 * @model ordered="false"
	 * @generated
	 */
	EList<BehavioralFeature> getDeleteServices();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Delete Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getDeleteServices()
	 * @generated
	 */
	BehavioralFeature getDeleteServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Delete Services</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ignoreCase
	 *                                Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass
	 *                                The Ecore class of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getDeleteServices()
	 * @generated
	 */
	BehavioralFeature getDeleteServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass);

	/**
	 * Returns the value of the '<em><b>Initialize Services</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.BehavioralFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initialize Services</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 *
	 * @return the value of the '<em>Initialize Services</em>' reference list.
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SW_ResourceCorePackage#getSwResource_InitializeServices()
	 * @model ordered="false"
	 * @generated
	 */
	EList<BehavioralFeature> getInitializeServices();

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Initialize Services</b></em>' reference
	 * list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getInitializeServices()
	 * @generated
	 */
	BehavioralFeature getInitializeServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes);

	/**
	 * Retrieves the first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>' from the '<em><b>Initialize Services</b></em>' reference
	 * list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @param name
	 *                                The '<em><b>Name</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterNames
	 *                                The '<em><b>Owned Parameter Names</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ownedParameterTypes
	 *                                The '<em><b>Owned Parameter Types</b></em>' of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @param ignoreCase
	 *                                Whether to ignore case in {@link java.lang.String} comparisons.
	 * @param eClass
	 *                                The Ecore class of the {@link org.eclipse.uml2.uml.BehavioralFeature} to retrieve, or <code>null</code>.
	 * @return The first {@link org.eclipse.uml2.uml.BehavioralFeature} with the specified '<em><b>Name</b></em>', '<em><b>Owned Parameter Names</b></em>', and '<em><b>Owned Parameter Types</b></em>', or <code>null</code>.
	 * @see #getInitializeServices()
	 * @generated
	 */
	BehavioralFeature getInitializeServices(String name, EList<String> ownedParameterNames, EList<Type> ownedParameterTypes, boolean ignoreCase, EClass eClass);

} // SwResource

/**
 * Copyright (c) 2010, 2016 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Chokri MRAIDHA (CEA LIST) chokri.mraidha@cea.fr - Initial API and implementation
 *
 */
package org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.MARTE.MARTEPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GQAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GQAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.MARTE_AnalysisModelPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.PAM.PAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.PAM.impl.PAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.SAM.SAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.SAM.impl.SAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.impl.MARTE_AnalysisModelPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.MARTE_AnnexesPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.RSM.RSMPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.RSM.impl.RSMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.DataTypes.DataTypesPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.DataTypes.impl.DataTypesPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Operators.OperatorsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Operators.impl.OperatorsPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.VSLPackage;
import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Variables.VariablesPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Variables.impl.VariablesPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.impl.VSLPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_Annexes.impl.MARTE_AnnexesPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.GCMPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.impl.GCMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HLAM.HLAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HLAM.impl.HLAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HRMPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwGeneral.HwGeneralPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwGeneral.impl.HwGeneralPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwCommunication.HwCommunicationPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwCommunication.impl.HwCommunicationPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwComputing.HwComputingPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwComputing.impl.HwComputingPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwDevice.HwDevicePackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwDevice.impl.HwDevicePackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwLogicalPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwMemory.HwMemoryPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwMemory.impl.HwMemoryPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwStorageManager.HwStorageManagerPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwStorageManager.impl.HwStorageManagerPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwStoragePackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.impl.HwStoragePackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwTiming.HwTimingPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwTiming.impl.HwTimingPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.impl.HwLogicalPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwLayout.HwLayoutPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwLayout.impl.HwLayoutPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwPhysicalPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwPower.HwPowerPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwPower.impl.HwPowerPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.impl.HwPhysicalPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.impl.HRMPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.MARTE_DesignModelPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SRMPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.SW_BrokeringPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.impl.SW_BrokeringPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.Alarm;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.EntryPoint;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.InterruptKind;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.InterruptResource;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.MemoryPartition;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SW_ConcurrencyFactory;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SW_ConcurrencyPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SwConcurrentResource;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SwSchedulableResource;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SwTimerResource;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.SW_InteractionPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.impl.SW_InteractionPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SW_ResourceCorePackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.impl.SW_ResourceCorePackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.impl.SRMPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.impl.MARTE_DesignModelPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.AllocPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.impl.AllocPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.CoreElements.CoreElementsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.CoreElements.impl.CoreElementsPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.GRMPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.impl.GRMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.MARTE_FoundationsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.NFPs.NFPsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.NFPs.impl.NFPsPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.TimePackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.impl.TimePackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.impl.MARTE_FoundationsPackageImpl;

import org.eclipse.papyrus.MARTE.impl.MARTEPackageImpl;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.GRM_BasicTypesPackage;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.impl.GRM_BasicTypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.impl.MARTE_DataTypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.impl.MARTE_PrimitivesTypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.impl.MeasurementUnitsPackageImpl;

import org.eclipse.papyrus.MARTE_Library.RS_Library.RS_LibraryPackage;

import org.eclipse.papyrus.MARTE_Library.RS_Library.impl.RS_LibraryPackageImpl;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.TimeLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.impl.TimeLibraryPackageImpl;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.TimeTypesLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.impl.TimeTypesLibraryPackageImpl;

import org.eclipse.uml2.types.TypesPackage;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SW_ConcurrencyPackageImpl extends EPackageImpl implements SW_ConcurrencyPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass entryPointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass swConcurrentResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass interruptResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass swSchedulableResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass swTimerResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass memoryPartitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass alarmEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EEnum interruptKindEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SW_ConcurrencyPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SW_ConcurrencyPackageImpl() {
		super(eNS_URI, SW_ConcurrencyFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>
	 * This method is used to initialize {@link SW_ConcurrencyPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SW_ConcurrencyPackage init() {
		if (isInited) {
			return (SW_ConcurrencyPackage) EPackage.Registry.INSTANCE.getEPackage(SW_ConcurrencyPackage.eNS_URI);
		}

		// Obtain or create and register package
		Object registeredSW_ConcurrencyPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		SW_ConcurrencyPackageImpl theSW_ConcurrencyPackage = registeredSW_ConcurrencyPackage instanceof SW_ConcurrencyPackageImpl ? (SW_ConcurrencyPackageImpl) registeredSW_ConcurrencyPackage : new SW_ConcurrencyPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTEPackage.eNS_URI);
		MARTEPackageImpl theMARTEPackage = (MARTEPackageImpl) (registeredPackage instanceof MARTEPackageImpl ? registeredPackage : MARTEPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_FoundationsPackage.eNS_URI);
		MARTE_FoundationsPackageImpl theMARTE_FoundationsPackage = (MARTE_FoundationsPackageImpl) (registeredPackage instanceof MARTE_FoundationsPackageImpl ? registeredPackage : MARTE_FoundationsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(NFPsPackage.eNS_URI);
		NFPsPackageImpl theNFPsPackage = (NFPsPackageImpl) (registeredPackage instanceof NFPsPackageImpl ? registeredPackage : NFPsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CoreElementsPackage.eNS_URI);
		CoreElementsPackageImpl theCoreElementsPackage = (CoreElementsPackageImpl) (registeredPackage instanceof CoreElementsPackageImpl ? registeredPackage : CoreElementsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(AllocPackage.eNS_URI);
		AllocPackageImpl theAllocPackage = (AllocPackageImpl) (registeredPackage instanceof AllocPackageImpl ? registeredPackage : AllocPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimePackage.eNS_URI);
		TimePackageImpl theTimePackage = (TimePackageImpl) (registeredPackage instanceof TimePackageImpl ? registeredPackage : TimePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GRMPackage.eNS_URI);
		GRMPackageImpl theGRMPackage = (GRMPackageImpl) (registeredPackage instanceof GRMPackageImpl ? registeredPackage : GRMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_AnnexesPackage.eNS_URI);
		MARTE_AnnexesPackageImpl theMARTE_AnnexesPackage = (MARTE_AnnexesPackageImpl) (registeredPackage instanceof MARTE_AnnexesPackageImpl ? registeredPackage : MARTE_AnnexesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(RSMPackage.eNS_URI);
		RSMPackageImpl theRSMPackage = (RSMPackageImpl) (registeredPackage instanceof RSMPackageImpl ? registeredPackage : RSMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(VSLPackage.eNS_URI);
		VSLPackageImpl theVSLPackage = (VSLPackageImpl) (registeredPackage instanceof VSLPackageImpl ? registeredPackage : VSLPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(VariablesPackage.eNS_URI);
		VariablesPackageImpl theVariablesPackage = (VariablesPackageImpl) (registeredPackage instanceof VariablesPackageImpl ? registeredPackage : VariablesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI);
		OperatorsPackageImpl theOperatorsPackage = (OperatorsPackageImpl) (registeredPackage instanceof OperatorsPackageImpl ? registeredPackage : OperatorsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(DataTypesPackage.eNS_URI);
		DataTypesPackageImpl theDataTypesPackage = (DataTypesPackageImpl) (registeredPackage instanceof DataTypesPackageImpl ? registeredPackage : DataTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_DesignModelPackage.eNS_URI);
		MARTE_DesignModelPackageImpl theMARTE_DesignModelPackage = (MARTE_DesignModelPackageImpl) (registeredPackage instanceof MARTE_DesignModelPackageImpl ? registeredPackage : MARTE_DesignModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HLAMPackage.eNS_URI);
		HLAMPackageImpl theHLAMPackage = (HLAMPackageImpl) (registeredPackage instanceof HLAMPackageImpl ? registeredPackage : HLAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HRMPackage.eNS_URI);
		HRMPackageImpl theHRMPackage = (HRMPackageImpl) (registeredPackage instanceof HRMPackageImpl ? registeredPackage : HRMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwLogicalPackage.eNS_URI);
		HwLogicalPackageImpl theHwLogicalPackage = (HwLogicalPackageImpl) (registeredPackage instanceof HwLogicalPackageImpl ? registeredPackage : HwLogicalPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwComputingPackage.eNS_URI);
		HwComputingPackageImpl theHwComputingPackage = (HwComputingPackageImpl) (registeredPackage instanceof HwComputingPackageImpl ? registeredPackage : HwComputingPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwCommunicationPackage.eNS_URI);
		HwCommunicationPackageImpl theHwCommunicationPackage = (HwCommunicationPackageImpl) (registeredPackage instanceof HwCommunicationPackageImpl ? registeredPackage : HwCommunicationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwStoragePackage.eNS_URI);
		HwStoragePackageImpl theHwStoragePackage = (HwStoragePackageImpl) (registeredPackage instanceof HwStoragePackageImpl ? registeredPackage : HwStoragePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwStorageManagerPackage.eNS_URI);
		HwStorageManagerPackageImpl theHwStorageManagerPackage = (HwStorageManagerPackageImpl) (registeredPackage instanceof HwStorageManagerPackageImpl ? registeredPackage : HwStorageManagerPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwMemoryPackage.eNS_URI);
		HwMemoryPackageImpl theHwMemoryPackage = (HwMemoryPackageImpl) (registeredPackage instanceof HwMemoryPackageImpl ? registeredPackage : HwMemoryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwTimingPackage.eNS_URI);
		HwTimingPackageImpl theHwTimingPackage = (HwTimingPackageImpl) (registeredPackage instanceof HwTimingPackageImpl ? registeredPackage : HwTimingPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwDevicePackage.eNS_URI);
		HwDevicePackageImpl theHwDevicePackage = (HwDevicePackageImpl) (registeredPackage instanceof HwDevicePackageImpl ? registeredPackage : HwDevicePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwGeneralPackage.eNS_URI);
		HwGeneralPackageImpl theHwGeneralPackage = (HwGeneralPackageImpl) (registeredPackage instanceof HwGeneralPackageImpl ? registeredPackage : HwGeneralPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwPhysicalPackage.eNS_URI);
		HwPhysicalPackageImpl theHwPhysicalPackage = (HwPhysicalPackageImpl) (registeredPackage instanceof HwPhysicalPackageImpl ? registeredPackage : HwPhysicalPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwLayoutPackage.eNS_URI);
		HwLayoutPackageImpl theHwLayoutPackage = (HwLayoutPackageImpl) (registeredPackage instanceof HwLayoutPackageImpl ? registeredPackage : HwLayoutPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwPowerPackage.eNS_URI);
		HwPowerPackageImpl theHwPowerPackage = (HwPowerPackageImpl) (registeredPackage instanceof HwPowerPackageImpl ? registeredPackage : HwPowerPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SRMPackage.eNS_URI);
		SRMPackageImpl theSRMPackage = (SRMPackageImpl) (registeredPackage instanceof SRMPackageImpl ? registeredPackage : SRMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_ResourceCorePackage.eNS_URI);
		SW_ResourceCorePackageImpl theSW_ResourceCorePackage = (SW_ResourceCorePackageImpl) (registeredPackage instanceof SW_ResourceCorePackageImpl ? registeredPackage : SW_ResourceCorePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_BrokeringPackage.eNS_URI);
		SW_BrokeringPackageImpl theSW_BrokeringPackage = (SW_BrokeringPackageImpl) (registeredPackage instanceof SW_BrokeringPackageImpl ? registeredPackage : SW_BrokeringPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_InteractionPackage.eNS_URI);
		SW_InteractionPackageImpl theSW_InteractionPackage = (SW_InteractionPackageImpl) (registeredPackage instanceof SW_InteractionPackageImpl ? registeredPackage : SW_InteractionPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GCMPackage.eNS_URI);
		GCMPackageImpl theGCMPackage = (GCMPackageImpl) (registeredPackage instanceof GCMPackageImpl ? registeredPackage : GCMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_AnalysisModelPackage.eNS_URI);
		MARTE_AnalysisModelPackageImpl theMARTE_AnalysisModelPackage = (MARTE_AnalysisModelPackageImpl) (registeredPackage instanceof MARTE_AnalysisModelPackageImpl ? registeredPackage : MARTE_AnalysisModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GQAMPackage.eNS_URI);
		GQAMPackageImpl theGQAMPackage = (GQAMPackageImpl) (registeredPackage instanceof GQAMPackageImpl ? registeredPackage : GQAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SAMPackage.eNS_URI);
		SAMPackageImpl theSAMPackage = (SAMPackageImpl) (registeredPackage instanceof SAMPackageImpl ? registeredPackage : SAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(PAMPackage.eNS_URI);
		PAMPackageImpl thePAMPackage = (PAMPackageImpl) (registeredPackage instanceof PAMPackageImpl ? registeredPackage : PAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MeasurementUnitsPackage.eNS_URI);
		MeasurementUnitsPackageImpl theMeasurementUnitsPackage = (MeasurementUnitsPackageImpl) (registeredPackage instanceof MeasurementUnitsPackageImpl ? registeredPackage : MeasurementUnitsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GRM_BasicTypesPackage.eNS_URI);
		GRM_BasicTypesPackageImpl theGRM_BasicTypesPackage = (GRM_BasicTypesPackageImpl) (registeredPackage instanceof GRM_BasicTypesPackageImpl ? registeredPackage : GRM_BasicTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_DataTypesPackage.eNS_URI);
		MARTE_DataTypesPackageImpl theMARTE_DataTypesPackage = (MARTE_DataTypesPackageImpl) (registeredPackage instanceof MARTE_DataTypesPackageImpl ? registeredPackage : MARTE_DataTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BasicNFP_TypesPackage.eNS_URI);
		BasicNFP_TypesPackageImpl theBasicNFP_TypesPackage = (BasicNFP_TypesPackageImpl) (registeredPackage instanceof BasicNFP_TypesPackageImpl ? registeredPackage : BasicNFP_TypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimeTypesLibraryPackage.eNS_URI);
		TimeTypesLibraryPackageImpl theTimeTypesLibraryPackage = (TimeTypesLibraryPackageImpl) (registeredPackage instanceof TimeTypesLibraryPackageImpl ? registeredPackage : TimeTypesLibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimeLibraryPackage.eNS_URI);
		TimeLibraryPackageImpl theTimeLibraryPackage = (TimeLibraryPackageImpl) (registeredPackage instanceof TimeLibraryPackageImpl ? registeredPackage : TimeLibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(RS_LibraryPackage.eNS_URI);
		RS_LibraryPackageImpl theRS_LibraryPackage = (RS_LibraryPackageImpl) (registeredPackage instanceof RS_LibraryPackageImpl ? registeredPackage : RS_LibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_PrimitivesTypesPackage.eNS_URI);
		MARTE_PrimitivesTypesPackageImpl theMARTE_PrimitivesTypesPackage = (MARTE_PrimitivesTypesPackageImpl) (registeredPackage instanceof MARTE_PrimitivesTypesPackageImpl ? registeredPackage : MARTE_PrimitivesTypesPackage.eINSTANCE);

		// Create package meta-data objects
		theSW_ConcurrencyPackage.createPackageContents();
		theMARTEPackage.createPackageContents();
		theMARTE_FoundationsPackage.createPackageContents();
		theNFPsPackage.createPackageContents();
		theCoreElementsPackage.createPackageContents();
		theAllocPackage.createPackageContents();
		theTimePackage.createPackageContents();
		theGRMPackage.createPackageContents();
		theMARTE_AnnexesPackage.createPackageContents();
		theRSMPackage.createPackageContents();
		theVSLPackage.createPackageContents();
		theVariablesPackage.createPackageContents();
		theOperatorsPackage.createPackageContents();
		theDataTypesPackage.createPackageContents();
		theMARTE_DesignModelPackage.createPackageContents();
		theHLAMPackage.createPackageContents();
		theHRMPackage.createPackageContents();
		theHwLogicalPackage.createPackageContents();
		theHwComputingPackage.createPackageContents();
		theHwCommunicationPackage.createPackageContents();
		theHwStoragePackage.createPackageContents();
		theHwStorageManagerPackage.createPackageContents();
		theHwMemoryPackage.createPackageContents();
		theHwTimingPackage.createPackageContents();
		theHwDevicePackage.createPackageContents();
		theHwGeneralPackage.createPackageContents();
		theHwPhysicalPackage.createPackageContents();
		theHwLayoutPackage.createPackageContents();
		theHwPowerPackage.createPackageContents();
		theSRMPackage.createPackageContents();
		theSW_ResourceCorePackage.createPackageContents();
		theSW_BrokeringPackage.createPackageContents();
		theSW_InteractionPackage.createPackageContents();
		theGCMPackage.createPackageContents();
		theMARTE_AnalysisModelPackage.createPackageContents();
		theGQAMPackage.createPackageContents();
		theSAMPackage.createPackageContents();
		thePAMPackage.createPackageContents();
		theMeasurementUnitsPackage.createPackageContents();
		theGRM_BasicTypesPackage.createPackageContents();
		theMARTE_DataTypesPackage.createPackageContents();
		theBasicNFP_TypesPackage.createPackageContents();
		theTimeTypesLibraryPackage.createPackageContents();
		theTimeLibraryPackage.createPackageContents();
		theRS_LibraryPackage.createPackageContents();
		theMARTE_PrimitivesTypesPackage.createPackageContents();

		// Initialize created meta-data
		theSW_ConcurrencyPackage.initializePackageContents();
		theMARTEPackage.initializePackageContents();
		theMARTE_FoundationsPackage.initializePackageContents();
		theNFPsPackage.initializePackageContents();
		theCoreElementsPackage.initializePackageContents();
		theAllocPackage.initializePackageContents();
		theTimePackage.initializePackageContents();
		theGRMPackage.initializePackageContents();
		theMARTE_AnnexesPackage.initializePackageContents();
		theRSMPackage.initializePackageContents();
		theVSLPackage.initializePackageContents();
		theVariablesPackage.initializePackageContents();
		theOperatorsPackage.initializePackageContents();
		theDataTypesPackage.initializePackageContents();
		theMARTE_DesignModelPackage.initializePackageContents();
		theHLAMPackage.initializePackageContents();
		theHRMPackage.initializePackageContents();
		theHwLogicalPackage.initializePackageContents();
		theHwComputingPackage.initializePackageContents();
		theHwCommunicationPackage.initializePackageContents();
		theHwStoragePackage.initializePackageContents();
		theHwStorageManagerPackage.initializePackageContents();
		theHwMemoryPackage.initializePackageContents();
		theHwTimingPackage.initializePackageContents();
		theHwDevicePackage.initializePackageContents();
		theHwGeneralPackage.initializePackageContents();
		theHwPhysicalPackage.initializePackageContents();
		theHwLayoutPackage.initializePackageContents();
		theHwPowerPackage.initializePackageContents();
		theSRMPackage.initializePackageContents();
		theSW_ResourceCorePackage.initializePackageContents();
		theSW_BrokeringPackage.initializePackageContents();
		theSW_InteractionPackage.initializePackageContents();
		theGCMPackage.initializePackageContents();
		theMARTE_AnalysisModelPackage.initializePackageContents();
		theGQAMPackage.initializePackageContents();
		theSAMPackage.initializePackageContents();
		thePAMPackage.initializePackageContents();
		theMeasurementUnitsPackage.initializePackageContents();
		theGRM_BasicTypesPackage.initializePackageContents();
		theMARTE_DataTypesPackage.initializePackageContents();
		theBasicNFP_TypesPackage.initializePackageContents();
		theTimeTypesLibraryPackage.initializePackageContents();
		theTimeLibraryPackage.initializePackageContents();
		theRS_LibraryPackage.initializePackageContents();
		theMARTE_PrimitivesTypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSW_ConcurrencyPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SW_ConcurrencyPackage.eNS_URI, theSW_ConcurrencyPackage);
		return theSW_ConcurrencyPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getEntryPoint() {
		return entryPointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getEntryPoint_IsReentrant() {
		return (EAttribute) entryPointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getEntryPoint_Routine() {
		return (EReference) entryPointEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getSwConcurrentResource() {
		return swConcurrentResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getSwConcurrentResource_Type() {
		return (EAttribute) swConcurrentResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getSwConcurrentResource_ActivationCapacity() {
		return (EAttribute) swConcurrentResourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwConcurrentResource_EntryPoints() {
		return (EReference) swConcurrentResourceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwConcurrentResource_AdressSpace() {
		return (EReference) swConcurrentResourceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwConcurrentResource_PeriodElements() {
		return (EReference) swConcurrentResourceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwConcurrentResource_PriorityElements() {
		return (EReference) swConcurrentResourceEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwConcurrentResource_StackSizeElements() {
		return (EReference) swConcurrentResourceEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwConcurrentResource_ActivateServices() {
		return (EReference) swConcurrentResourceEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwConcurrentResource_EnableConcurrencyServices() {
		return (EReference) swConcurrentResourceEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwConcurrentResource_ResumeServices() {
		return (EReference) swConcurrentResourceEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwConcurrentResource_SuspendServices() {
		return (EReference) swConcurrentResourceEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwConcurrentResource_TerminateServices() {
		return (EReference) swConcurrentResourceEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwConcurrentResource_DisableConcurrencyServices() {
		return (EReference) swConcurrentResourceEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwConcurrentResource_ShareDataResources() {
		return (EReference) swConcurrentResourceEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwConcurrentResource_MessageResources() {
		return (EReference) swConcurrentResourceEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwConcurrentResource_MutualExclusionResources() {
		return (EReference) swConcurrentResourceEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwConcurrentResource_NotificationResources() {
		return (EReference) swConcurrentResourceEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwConcurrentResource_HeapSizeElements() {
		return (EReference) swConcurrentResourceEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getInterruptResource() {
		return interruptResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getInterruptResource_Kind() {
		return (EAttribute) interruptResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getInterruptResource_IsMaskable() {
		return (EAttribute) interruptResourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getInterruptResource_VectorElements() {
		return (EReference) interruptResourceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getInterruptResource_MaskElements() {
		return (EReference) interruptResourceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getInterruptResource_RoutineConnectServices() {
		return (EReference) interruptResourceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getInterruptResource_RoutineDisconnectServices() {
		return (EReference) interruptResourceEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getSwSchedulableResource() {
		return swSchedulableResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getSwSchedulableResource_IsStaticSchedulingFeature() {
		return (EAttribute) swSchedulableResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getSwSchedulableResource_IsPreemptable() {
		return (EAttribute) swSchedulableResourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwSchedulableResource_Schedulers() {
		return (EReference) swSchedulableResourceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwSchedulableResource_DeadlineElements() {
		return (EReference) swSchedulableResourceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwSchedulableResource_DeadlineTypeElements() {
		return (EReference) swSchedulableResourceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwSchedulableResource_TimeSliceElements() {
		return (EReference) swSchedulableResourceEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwSchedulableResource_DelayServices() {
		return (EReference) swSchedulableResourceEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwSchedulableResource_JoinServices() {
		return (EReference) swSchedulableResourceEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwSchedulableResource_YieldServices() {
		return (EReference) swSchedulableResourceEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getSwTimerResource() {
		return swTimerResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSwTimerResource_DurationElements() {
		return (EReference) swTimerResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getMemoryPartition() {
		return memoryPartitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getMemoryPartition_ConcurrentResources() {
		return (EReference) memoryPartitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getMemoryPartition_MemorySpaces() {
		return (EReference) memoryPartitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getMemoryPartition_Fork() {
		return (EReference) memoryPartitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getMemoryPartition_Exit() {
		return (EReference) memoryPartitionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getMemoryPartition_Base_Namespace() {
		return (EReference) memoryPartitionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getAlarm() {
		return alarmEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getAlarm_IsWatchdog() {
		return (EAttribute) alarmEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getAlarm_Timers() {
		return (EReference) alarmEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EEnum getInterruptKind() {
		return interruptKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public SW_ConcurrencyFactory getSW_ConcurrencyFactory() {
		return (SW_ConcurrencyFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package. This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) {
			return;
		}
		isCreated = true;

		// Create classes and their features
		entryPointEClass = createEClass(ENTRY_POINT);
		createEAttribute(entryPointEClass, ENTRY_POINT__IS_REENTRANT);
		createEReference(entryPointEClass, ENTRY_POINT__ROUTINE);

		swConcurrentResourceEClass = createEClass(SW_CONCURRENT_RESOURCE);
		createEAttribute(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__TYPE);
		createEAttribute(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__ACTIVATION_CAPACITY);
		createEReference(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__ENTRY_POINTS);
		createEReference(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__ADRESS_SPACE);
		createEReference(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__PERIOD_ELEMENTS);
		createEReference(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__PRIORITY_ELEMENTS);
		createEReference(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__STACK_SIZE_ELEMENTS);
		createEReference(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__ACTIVATE_SERVICES);
		createEReference(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__ENABLE_CONCURRENCY_SERVICES);
		createEReference(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__RESUME_SERVICES);
		createEReference(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__SUSPEND_SERVICES);
		createEReference(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__TERMINATE_SERVICES);
		createEReference(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__DISABLE_CONCURRENCY_SERVICES);
		createEReference(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__SHARE_DATA_RESOURCES);
		createEReference(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__MESSAGE_RESOURCES);
		createEReference(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__MUTUAL_EXCLUSION_RESOURCES);
		createEReference(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__NOTIFICATION_RESOURCES);
		createEReference(swConcurrentResourceEClass, SW_CONCURRENT_RESOURCE__HEAP_SIZE_ELEMENTS);

		interruptResourceEClass = createEClass(INTERRUPT_RESOURCE);
		createEAttribute(interruptResourceEClass, INTERRUPT_RESOURCE__KIND);
		createEAttribute(interruptResourceEClass, INTERRUPT_RESOURCE__IS_MASKABLE);
		createEReference(interruptResourceEClass, INTERRUPT_RESOURCE__VECTOR_ELEMENTS);
		createEReference(interruptResourceEClass, INTERRUPT_RESOURCE__MASK_ELEMENTS);
		createEReference(interruptResourceEClass, INTERRUPT_RESOURCE__ROUTINE_CONNECT_SERVICES);
		createEReference(interruptResourceEClass, INTERRUPT_RESOURCE__ROUTINE_DISCONNECT_SERVICES);

		swSchedulableResourceEClass = createEClass(SW_SCHEDULABLE_RESOURCE);
		createEAttribute(swSchedulableResourceEClass, SW_SCHEDULABLE_RESOURCE__IS_STATIC_SCHEDULING_FEATURE);
		createEAttribute(swSchedulableResourceEClass, SW_SCHEDULABLE_RESOURCE__IS_PREEMPTABLE);
		createEReference(swSchedulableResourceEClass, SW_SCHEDULABLE_RESOURCE__SCHEDULERS);
		createEReference(swSchedulableResourceEClass, SW_SCHEDULABLE_RESOURCE__DEADLINE_ELEMENTS);
		createEReference(swSchedulableResourceEClass, SW_SCHEDULABLE_RESOURCE__DEADLINE_TYPE_ELEMENTS);
		createEReference(swSchedulableResourceEClass, SW_SCHEDULABLE_RESOURCE__TIME_SLICE_ELEMENTS);
		createEReference(swSchedulableResourceEClass, SW_SCHEDULABLE_RESOURCE__DELAY_SERVICES);
		createEReference(swSchedulableResourceEClass, SW_SCHEDULABLE_RESOURCE__JOIN_SERVICES);
		createEReference(swSchedulableResourceEClass, SW_SCHEDULABLE_RESOURCE__YIELD_SERVICES);

		swTimerResourceEClass = createEClass(SW_TIMER_RESOURCE);
		createEReference(swTimerResourceEClass, SW_TIMER_RESOURCE__DURATION_ELEMENTS);

		memoryPartitionEClass = createEClass(MEMORY_PARTITION);
		createEReference(memoryPartitionEClass, MEMORY_PARTITION__CONCURRENT_RESOURCES);
		createEReference(memoryPartitionEClass, MEMORY_PARTITION__MEMORY_SPACES);
		createEReference(memoryPartitionEClass, MEMORY_PARTITION__FORK);
		createEReference(memoryPartitionEClass, MEMORY_PARTITION__EXIT);
		createEReference(memoryPartitionEClass, MEMORY_PARTITION__BASE_NAMESPACE);

		alarmEClass = createEClass(ALARM);
		createEAttribute(alarmEClass, ALARM__IS_WATCHDOG);
		createEReference(alarmEClass, ALARM__TIMERS);

		// Create enums
		interruptKindEEnum = createEEnum(INTERRUPT_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) {
			return;
		}
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		AllocPackage theAllocPackage = (AllocPackage) EPackage.Registry.INSTANCE.getEPackage(AllocPackage.eNS_URI);
		MARTE_PrimitivesTypesPackage theMARTE_PrimitivesTypesPackage = (MARTE_PrimitivesTypesPackage) EPackage.Registry.INSTANCE.getEPackage(MARTE_PrimitivesTypesPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage) EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		SW_ResourceCorePackage theSW_ResourceCorePackage = (SW_ResourceCorePackage) EPackage.Registry.INSTANCE.getEPackage(SW_ResourceCorePackage.eNS_URI);
		BasicNFP_TypesPackage theBasicNFP_TypesPackage = (BasicNFP_TypesPackage) EPackage.Registry.INSTANCE.getEPackage(BasicNFP_TypesPackage.eNS_URI);
		GRMPackage theGRMPackage = (GRMPackage) EPackage.Registry.INSTANCE.getEPackage(GRMPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		entryPointEClass.getESuperTypes().add(theAllocPackage.getAllocate());
		swConcurrentResourceEClass.getESuperTypes().add(theSW_ResourceCorePackage.getSwResource());
		interruptResourceEClass.getESuperTypes().add(this.getSwConcurrentResource());
		swSchedulableResourceEClass.getESuperTypes().add(this.getSwConcurrentResource());
		swSchedulableResourceEClass.getESuperTypes().add(theGRMPackage.getSchedulableResource());
		swTimerResourceEClass.getESuperTypes().add(theGRMPackage.getTimerResource());
		memoryPartitionEClass.getESuperTypes().add(theSW_ResourceCorePackage.getSwResource());
		alarmEClass.getESuperTypes().add(this.getInterruptResource());

		// Initialize classes and features; add operations and parameters
		initEClass(entryPointEClass, EntryPoint.class, "EntryPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getEntryPoint_IsReentrant(), theMARTE_PrimitivesTypesPackage.getBoolean(), "isReentrant", null, 0, 1, EntryPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getEntryPoint_Routine(), theUMLPackage.getBehavioralFeature(), null, "routine", null, 1, 1, EntryPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);

		initEClass(swConcurrentResourceEClass, SwConcurrentResource.class, "SwConcurrentResource", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getSwConcurrentResource_Type(), theBasicNFP_TypesPackage.getArrivalPattern(), "type", null, 0, 1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getSwConcurrentResource_ActivationCapacity(), theMARTE_PrimitivesTypesPackage.getInteger(), "activationCapacity", null, 0, 1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getSwConcurrentResource_EntryPoints(), theUMLPackage.getElement(), null, "entryPoints", null, 0, -1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getSwConcurrentResource_AdressSpace(), theUMLPackage.getTypedElement(), null, "adressSpace", null, 0, -1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwConcurrentResource_PeriodElements(), theUMLPackage.getTypedElement(), null, "periodElements", null, 0, -1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwConcurrentResource_PriorityElements(), theUMLPackage.getTypedElement(), null, "priorityElements", null, 0, -1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwConcurrentResource_StackSizeElements(), theUMLPackage.getTypedElement(), null, "stackSizeElements", null, 0, -1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwConcurrentResource_ActivateServices(), theUMLPackage.getBehavioralFeature(), null, "activateServices", null, 0, -1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwConcurrentResource_EnableConcurrencyServices(), theUMLPackage.getBehavioralFeature(), null, "enableConcurrencyServices", null, 0, -1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwConcurrentResource_ResumeServices(), theUMLPackage.getBehavioralFeature(), null, "resumeServices", null, 0, -1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwConcurrentResource_SuspendServices(), theUMLPackage.getBehavioralFeature(), null, "suspendServices", null, 0, -1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwConcurrentResource_TerminateServices(), theUMLPackage.getBehavioralFeature(), null, "terminateServices", null, 0, -1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwConcurrentResource_DisableConcurrencyServices(), theUMLPackage.getBehavioralFeature(), null, "disableConcurrencyServices", null, 0, -1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwConcurrentResource_ShareDataResources(), theUMLPackage.getTypedElement(), null, "shareDataResources", null, 0, -1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwConcurrentResource_MessageResources(), theUMLPackage.getTypedElement(), null, "messageResources", null, 0, -1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwConcurrentResource_MutualExclusionResources(), theUMLPackage.getTypedElement(), null, "mutualExclusionResources", null, 0, -1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwConcurrentResource_NotificationResources(), theUMLPackage.getTypedElement(), null, "notificationResources", null, 0, -1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwConcurrentResource_HeapSizeElements(), theUMLPackage.getTypedElement(), null, "heapSizeElements", null, 0, -1, SwConcurrentResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(interruptResourceEClass, InterruptResource.class, "InterruptResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getInterruptResource_Kind(), this.getInterruptKind(), "kind", null, 0, 1, InterruptResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getInterruptResource_IsMaskable(), theMARTE_PrimitivesTypesPackage.getBoolean(), "isMaskable", null, 0, 1, InterruptResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getInterruptResource_VectorElements(), theUMLPackage.getTypedElement(), null, "vectorElements", null, 0, -1, InterruptResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getInterruptResource_MaskElements(), theUMLPackage.getTypedElement(), null, "maskElements", null, 0, -1, InterruptResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getInterruptResource_RoutineConnectServices(), theUMLPackage.getBehavioralFeature(), null, "routineConnectServices", null, 0, -1, InterruptResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getInterruptResource_RoutineDisconnectServices(), theUMLPackage.getBehavioralFeature(), null, "routineDisconnectServices", null, 0, -1, InterruptResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(swSchedulableResourceEClass, SwSchedulableResource.class, "SwSchedulableResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getSwSchedulableResource_IsStaticSchedulingFeature(), theMARTE_PrimitivesTypesPackage.getBoolean(), "isStaticSchedulingFeature", null, 0, 1, SwSchedulableResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, //$NON-NLS-1$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getSwSchedulableResource_IsPreemptable(), theMARTE_PrimitivesTypesPackage.getBoolean(), "isPreemptable", null, 0, 1, SwSchedulableResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getSwSchedulableResource_Schedulers(), theUMLPackage.getNamedElement(), null, "schedulers", null, 1, 1, SwSchedulableResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwSchedulableResource_DeadlineElements(), theUMLPackage.getTypedElement(), null, "deadlineElements", null, 0, -1, SwSchedulableResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwSchedulableResource_DeadlineTypeElements(), theUMLPackage.getTypedElement(), null, "deadlineTypeElements", null, 0, -1, SwSchedulableResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwSchedulableResource_TimeSliceElements(), theUMLPackage.getTypedElement(), null, "timeSliceElements", null, 0, -1, SwSchedulableResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwSchedulableResource_DelayServices(), theUMLPackage.getBehavioralFeature(), null, "delayServices", null, 0, -1, SwSchedulableResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwSchedulableResource_JoinServices(), theUMLPackage.getBehavioralFeature(), null, "joinServices", null, 0, -1, SwSchedulableResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSwSchedulableResource_YieldServices(), theUMLPackage.getBehavioralFeature(), null, "yieldServices", null, 0, -1, SwSchedulableResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(swTimerResourceEClass, SwTimerResource.class, "SwTimerResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSwTimerResource_DurationElements(), theUMLPackage.getTypedElement(), null, "durationElements", null, 0, 1, SwTimerResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(memoryPartitionEClass, MemoryPartition.class, "MemoryPartition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getMemoryPartition_ConcurrentResources(), theUMLPackage.getTypedElement(), null, "concurrentResources", null, 0, -1, MemoryPartition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getMemoryPartition_MemorySpaces(), theUMLPackage.getTypedElement(), null, "memorySpaces", null, 0, -1, MemoryPartition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getMemoryPartition_Fork(), theUMLPackage.getBehavioralFeature(), null, "fork", null, 0, -1, MemoryPartition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getMemoryPartition_Exit(), theUMLPackage.getBehavioralFeature(), null, "exit", null, 0, -1, MemoryPartition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getMemoryPartition_Base_Namespace(), theUMLPackage.getNamespace(), null, "base_Namespace", null, 1, 1, MemoryPartition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);

		initEClass(alarmEClass, Alarm.class, "Alarm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getAlarm_IsWatchdog(), theMARTE_PrimitivesTypesPackage.getBoolean(), "isWatchdog", null, 0, 1, Alarm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getAlarm_Timers(), theUMLPackage.getTypedElement(), null, "timers", null, 0, -1, Alarm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		// Initialize enums and add enum literals
		initEEnum(interruptKindEEnum, InterruptKind.class, "InterruptKind"); //$NON-NLS-1$
		addEEnumLiteral(interruptKindEEnum, InterruptKind.HARDWARE_INTERRUPTION);
		addEEnumLiteral(interruptKindEEnum, InterruptKind.PROCESSOR_DETECTED_EXCEPTION);
		addEEnumLiteral(interruptKindEEnum, InterruptKind.PROGRAMMED_EXCEPTION);
		addEEnumLiteral(interruptKindEEnum, InterruptKind.UNDEF);
		addEEnumLiteral(interruptKindEEnum, InterruptKind.OTHER);
	}

} // SW_ConcurrencyPackageImpl

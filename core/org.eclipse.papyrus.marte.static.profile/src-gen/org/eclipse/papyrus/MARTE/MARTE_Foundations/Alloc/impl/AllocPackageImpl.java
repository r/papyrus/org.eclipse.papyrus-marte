/**
 * Copyright (c) 2010, 2016 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Chokri MRAIDHA (CEA LIST) chokri.mraidha@cea.fr - Initial API and implementation
 *
 */
package org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.MARTE.MARTEPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GQAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GQAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.MARTE_AnalysisModelPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.PAM.PAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.PAM.impl.PAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.SAM.SAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.SAM.impl.SAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.impl.MARTE_AnalysisModelPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.MARTE_AnnexesPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.RSM.RSMPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.RSM.impl.RSMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.DataTypes.DataTypesPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.DataTypes.impl.DataTypesPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Operators.OperatorsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Operators.impl.OperatorsPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.VSLPackage;
import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Variables.VariablesPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Variables.impl.VariablesPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.impl.VSLPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_Annexes.impl.MARTE_AnnexesPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.GCMPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.impl.GCMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HLAM.HLAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HLAM.impl.HLAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HRMPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwGeneral.HwGeneralPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwGeneral.impl.HwGeneralPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwCommunication.HwCommunicationPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwCommunication.impl.HwCommunicationPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwComputing.HwComputingPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwComputing.impl.HwComputingPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwDevice.HwDevicePackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwDevice.impl.HwDevicePackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwLogicalPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwMemory.HwMemoryPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwMemory.impl.HwMemoryPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwStorageManager.HwStorageManagerPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwStorageManager.impl.HwStorageManagerPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwStoragePackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.impl.HwStoragePackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwTiming.HwTimingPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwTiming.impl.HwTimingPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.impl.HwLogicalPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwLayout.HwLayoutPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwLayout.impl.HwLayoutPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwPhysicalPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwPower.HwPowerPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwPower.impl.HwPowerPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.impl.HwPhysicalPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.impl.HRMPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.MARTE_DesignModelPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SRMPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.SW_BrokeringPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.impl.SW_BrokeringPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SW_ConcurrencyPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.impl.SW_ConcurrencyPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.SW_InteractionPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.impl.SW_InteractionPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SW_ResourceCorePackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.impl.SW_ResourceCorePackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.impl.SRMPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.impl.MARTE_DesignModelPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.AllocFactory;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.AllocPackage;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.Allocate;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.AllocateActivityGroup;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.Allocated;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.AllocationEndKind;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.AllocationKind;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.AllocationNature;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.Assign;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.AssignmentKind;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.AssignmentNature;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.NfpRefine;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.CoreElements.CoreElementsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.CoreElements.impl.CoreElementsPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.GRMPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.impl.GRMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.MARTE_FoundationsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.NFPs.NFPsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.NFPs.impl.NFPsPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.TimePackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.impl.TimePackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.impl.MARTE_FoundationsPackageImpl;

import org.eclipse.papyrus.MARTE.impl.MARTEPackageImpl;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.GRM_BasicTypesPackage;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.impl.GRM_BasicTypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.impl.MARTE_DataTypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.impl.MARTE_PrimitivesTypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.impl.MeasurementUnitsPackageImpl;

import org.eclipse.papyrus.MARTE_Library.RS_Library.RS_LibraryPackage;

import org.eclipse.papyrus.MARTE_Library.RS_Library.impl.RS_LibraryPackageImpl;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.TimeLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.impl.TimeLibraryPackageImpl;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.TimeTypesLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.impl.TimeTypesLibraryPackageImpl;

import org.eclipse.uml2.types.TypesPackage;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AllocPackageImpl extends EPackageImpl implements AllocPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass allocatedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass allocateActivityGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass nfpRefineEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass assignEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass allocateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EEnum allocationEndKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EEnum allocationNatureEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EEnum allocationKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EEnum assignmentKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EEnum assignmentNatureEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.AllocPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AllocPackageImpl() {
		super(eNS_URI, AllocFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>
	 * This method is used to initialize {@link AllocPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AllocPackage init() {
		if (isInited) {
			return (AllocPackage) EPackage.Registry.INSTANCE.getEPackage(AllocPackage.eNS_URI);
		}

		// Obtain or create and register package
		Object registeredAllocPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		AllocPackageImpl theAllocPackage = registeredAllocPackage instanceof AllocPackageImpl ? (AllocPackageImpl) registeredAllocPackage : new AllocPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTEPackage.eNS_URI);
		MARTEPackageImpl theMARTEPackage = (MARTEPackageImpl) (registeredPackage instanceof MARTEPackageImpl ? registeredPackage : MARTEPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_FoundationsPackage.eNS_URI);
		MARTE_FoundationsPackageImpl theMARTE_FoundationsPackage = (MARTE_FoundationsPackageImpl) (registeredPackage instanceof MARTE_FoundationsPackageImpl ? registeredPackage : MARTE_FoundationsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(NFPsPackage.eNS_URI);
		NFPsPackageImpl theNFPsPackage = (NFPsPackageImpl) (registeredPackage instanceof NFPsPackageImpl ? registeredPackage : NFPsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CoreElementsPackage.eNS_URI);
		CoreElementsPackageImpl theCoreElementsPackage = (CoreElementsPackageImpl) (registeredPackage instanceof CoreElementsPackageImpl ? registeredPackage : CoreElementsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimePackage.eNS_URI);
		TimePackageImpl theTimePackage = (TimePackageImpl) (registeredPackage instanceof TimePackageImpl ? registeredPackage : TimePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GRMPackage.eNS_URI);
		GRMPackageImpl theGRMPackage = (GRMPackageImpl) (registeredPackage instanceof GRMPackageImpl ? registeredPackage : GRMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_AnnexesPackage.eNS_URI);
		MARTE_AnnexesPackageImpl theMARTE_AnnexesPackage = (MARTE_AnnexesPackageImpl) (registeredPackage instanceof MARTE_AnnexesPackageImpl ? registeredPackage : MARTE_AnnexesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(RSMPackage.eNS_URI);
		RSMPackageImpl theRSMPackage = (RSMPackageImpl) (registeredPackage instanceof RSMPackageImpl ? registeredPackage : RSMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(VSLPackage.eNS_URI);
		VSLPackageImpl theVSLPackage = (VSLPackageImpl) (registeredPackage instanceof VSLPackageImpl ? registeredPackage : VSLPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(VariablesPackage.eNS_URI);
		VariablesPackageImpl theVariablesPackage = (VariablesPackageImpl) (registeredPackage instanceof VariablesPackageImpl ? registeredPackage : VariablesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI);
		OperatorsPackageImpl theOperatorsPackage = (OperatorsPackageImpl) (registeredPackage instanceof OperatorsPackageImpl ? registeredPackage : OperatorsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(DataTypesPackage.eNS_URI);
		DataTypesPackageImpl theDataTypesPackage = (DataTypesPackageImpl) (registeredPackage instanceof DataTypesPackageImpl ? registeredPackage : DataTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_DesignModelPackage.eNS_URI);
		MARTE_DesignModelPackageImpl theMARTE_DesignModelPackage = (MARTE_DesignModelPackageImpl) (registeredPackage instanceof MARTE_DesignModelPackageImpl ? registeredPackage : MARTE_DesignModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HLAMPackage.eNS_URI);
		HLAMPackageImpl theHLAMPackage = (HLAMPackageImpl) (registeredPackage instanceof HLAMPackageImpl ? registeredPackage : HLAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HRMPackage.eNS_URI);
		HRMPackageImpl theHRMPackage = (HRMPackageImpl) (registeredPackage instanceof HRMPackageImpl ? registeredPackage : HRMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwLogicalPackage.eNS_URI);
		HwLogicalPackageImpl theHwLogicalPackage = (HwLogicalPackageImpl) (registeredPackage instanceof HwLogicalPackageImpl ? registeredPackage : HwLogicalPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwComputingPackage.eNS_URI);
		HwComputingPackageImpl theHwComputingPackage = (HwComputingPackageImpl) (registeredPackage instanceof HwComputingPackageImpl ? registeredPackage : HwComputingPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwCommunicationPackage.eNS_URI);
		HwCommunicationPackageImpl theHwCommunicationPackage = (HwCommunicationPackageImpl) (registeredPackage instanceof HwCommunicationPackageImpl ? registeredPackage : HwCommunicationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwStoragePackage.eNS_URI);
		HwStoragePackageImpl theHwStoragePackage = (HwStoragePackageImpl) (registeredPackage instanceof HwStoragePackageImpl ? registeredPackage : HwStoragePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwStorageManagerPackage.eNS_URI);
		HwStorageManagerPackageImpl theHwStorageManagerPackage = (HwStorageManagerPackageImpl) (registeredPackage instanceof HwStorageManagerPackageImpl ? registeredPackage : HwStorageManagerPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwMemoryPackage.eNS_URI);
		HwMemoryPackageImpl theHwMemoryPackage = (HwMemoryPackageImpl) (registeredPackage instanceof HwMemoryPackageImpl ? registeredPackage : HwMemoryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwTimingPackage.eNS_URI);
		HwTimingPackageImpl theHwTimingPackage = (HwTimingPackageImpl) (registeredPackage instanceof HwTimingPackageImpl ? registeredPackage : HwTimingPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwDevicePackage.eNS_URI);
		HwDevicePackageImpl theHwDevicePackage = (HwDevicePackageImpl) (registeredPackage instanceof HwDevicePackageImpl ? registeredPackage : HwDevicePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwGeneralPackage.eNS_URI);
		HwGeneralPackageImpl theHwGeneralPackage = (HwGeneralPackageImpl) (registeredPackage instanceof HwGeneralPackageImpl ? registeredPackage : HwGeneralPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwPhysicalPackage.eNS_URI);
		HwPhysicalPackageImpl theHwPhysicalPackage = (HwPhysicalPackageImpl) (registeredPackage instanceof HwPhysicalPackageImpl ? registeredPackage : HwPhysicalPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwLayoutPackage.eNS_URI);
		HwLayoutPackageImpl theHwLayoutPackage = (HwLayoutPackageImpl) (registeredPackage instanceof HwLayoutPackageImpl ? registeredPackage : HwLayoutPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwPowerPackage.eNS_URI);
		HwPowerPackageImpl theHwPowerPackage = (HwPowerPackageImpl) (registeredPackage instanceof HwPowerPackageImpl ? registeredPackage : HwPowerPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SRMPackage.eNS_URI);
		SRMPackageImpl theSRMPackage = (SRMPackageImpl) (registeredPackage instanceof SRMPackageImpl ? registeredPackage : SRMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_ResourceCorePackage.eNS_URI);
		SW_ResourceCorePackageImpl theSW_ResourceCorePackage = (SW_ResourceCorePackageImpl) (registeredPackage instanceof SW_ResourceCorePackageImpl ? registeredPackage : SW_ResourceCorePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_ConcurrencyPackage.eNS_URI);
		SW_ConcurrencyPackageImpl theSW_ConcurrencyPackage = (SW_ConcurrencyPackageImpl) (registeredPackage instanceof SW_ConcurrencyPackageImpl ? registeredPackage : SW_ConcurrencyPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_BrokeringPackage.eNS_URI);
		SW_BrokeringPackageImpl theSW_BrokeringPackage = (SW_BrokeringPackageImpl) (registeredPackage instanceof SW_BrokeringPackageImpl ? registeredPackage : SW_BrokeringPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_InteractionPackage.eNS_URI);
		SW_InteractionPackageImpl theSW_InteractionPackage = (SW_InteractionPackageImpl) (registeredPackage instanceof SW_InteractionPackageImpl ? registeredPackage : SW_InteractionPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GCMPackage.eNS_URI);
		GCMPackageImpl theGCMPackage = (GCMPackageImpl) (registeredPackage instanceof GCMPackageImpl ? registeredPackage : GCMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_AnalysisModelPackage.eNS_URI);
		MARTE_AnalysisModelPackageImpl theMARTE_AnalysisModelPackage = (MARTE_AnalysisModelPackageImpl) (registeredPackage instanceof MARTE_AnalysisModelPackageImpl ? registeredPackage : MARTE_AnalysisModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GQAMPackage.eNS_URI);
		GQAMPackageImpl theGQAMPackage = (GQAMPackageImpl) (registeredPackage instanceof GQAMPackageImpl ? registeredPackage : GQAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SAMPackage.eNS_URI);
		SAMPackageImpl theSAMPackage = (SAMPackageImpl) (registeredPackage instanceof SAMPackageImpl ? registeredPackage : SAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(PAMPackage.eNS_URI);
		PAMPackageImpl thePAMPackage = (PAMPackageImpl) (registeredPackage instanceof PAMPackageImpl ? registeredPackage : PAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MeasurementUnitsPackage.eNS_URI);
		MeasurementUnitsPackageImpl theMeasurementUnitsPackage = (MeasurementUnitsPackageImpl) (registeredPackage instanceof MeasurementUnitsPackageImpl ? registeredPackage : MeasurementUnitsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GRM_BasicTypesPackage.eNS_URI);
		GRM_BasicTypesPackageImpl theGRM_BasicTypesPackage = (GRM_BasicTypesPackageImpl) (registeredPackage instanceof GRM_BasicTypesPackageImpl ? registeredPackage : GRM_BasicTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_DataTypesPackage.eNS_URI);
		MARTE_DataTypesPackageImpl theMARTE_DataTypesPackage = (MARTE_DataTypesPackageImpl) (registeredPackage instanceof MARTE_DataTypesPackageImpl ? registeredPackage : MARTE_DataTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BasicNFP_TypesPackage.eNS_URI);
		BasicNFP_TypesPackageImpl theBasicNFP_TypesPackage = (BasicNFP_TypesPackageImpl) (registeredPackage instanceof BasicNFP_TypesPackageImpl ? registeredPackage : BasicNFP_TypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimeTypesLibraryPackage.eNS_URI);
		TimeTypesLibraryPackageImpl theTimeTypesLibraryPackage = (TimeTypesLibraryPackageImpl) (registeredPackage instanceof TimeTypesLibraryPackageImpl ? registeredPackage : TimeTypesLibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimeLibraryPackage.eNS_URI);
		TimeLibraryPackageImpl theTimeLibraryPackage = (TimeLibraryPackageImpl) (registeredPackage instanceof TimeLibraryPackageImpl ? registeredPackage : TimeLibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(RS_LibraryPackage.eNS_URI);
		RS_LibraryPackageImpl theRS_LibraryPackage = (RS_LibraryPackageImpl) (registeredPackage instanceof RS_LibraryPackageImpl ? registeredPackage : RS_LibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_PrimitivesTypesPackage.eNS_URI);
		MARTE_PrimitivesTypesPackageImpl theMARTE_PrimitivesTypesPackage = (MARTE_PrimitivesTypesPackageImpl) (registeredPackage instanceof MARTE_PrimitivesTypesPackageImpl ? registeredPackage : MARTE_PrimitivesTypesPackage.eINSTANCE);

		// Create package meta-data objects
		theAllocPackage.createPackageContents();
		theMARTEPackage.createPackageContents();
		theMARTE_FoundationsPackage.createPackageContents();
		theNFPsPackage.createPackageContents();
		theCoreElementsPackage.createPackageContents();
		theTimePackage.createPackageContents();
		theGRMPackage.createPackageContents();
		theMARTE_AnnexesPackage.createPackageContents();
		theRSMPackage.createPackageContents();
		theVSLPackage.createPackageContents();
		theVariablesPackage.createPackageContents();
		theOperatorsPackage.createPackageContents();
		theDataTypesPackage.createPackageContents();
		theMARTE_DesignModelPackage.createPackageContents();
		theHLAMPackage.createPackageContents();
		theHRMPackage.createPackageContents();
		theHwLogicalPackage.createPackageContents();
		theHwComputingPackage.createPackageContents();
		theHwCommunicationPackage.createPackageContents();
		theHwStoragePackage.createPackageContents();
		theHwStorageManagerPackage.createPackageContents();
		theHwMemoryPackage.createPackageContents();
		theHwTimingPackage.createPackageContents();
		theHwDevicePackage.createPackageContents();
		theHwGeneralPackage.createPackageContents();
		theHwPhysicalPackage.createPackageContents();
		theHwLayoutPackage.createPackageContents();
		theHwPowerPackage.createPackageContents();
		theSRMPackage.createPackageContents();
		theSW_ResourceCorePackage.createPackageContents();
		theSW_ConcurrencyPackage.createPackageContents();
		theSW_BrokeringPackage.createPackageContents();
		theSW_InteractionPackage.createPackageContents();
		theGCMPackage.createPackageContents();
		theMARTE_AnalysisModelPackage.createPackageContents();
		theGQAMPackage.createPackageContents();
		theSAMPackage.createPackageContents();
		thePAMPackage.createPackageContents();
		theMeasurementUnitsPackage.createPackageContents();
		theGRM_BasicTypesPackage.createPackageContents();
		theMARTE_DataTypesPackage.createPackageContents();
		theBasicNFP_TypesPackage.createPackageContents();
		theTimeTypesLibraryPackage.createPackageContents();
		theTimeLibraryPackage.createPackageContents();
		theRS_LibraryPackage.createPackageContents();
		theMARTE_PrimitivesTypesPackage.createPackageContents();

		// Initialize created meta-data
		theAllocPackage.initializePackageContents();
		theMARTEPackage.initializePackageContents();
		theMARTE_FoundationsPackage.initializePackageContents();
		theNFPsPackage.initializePackageContents();
		theCoreElementsPackage.initializePackageContents();
		theTimePackage.initializePackageContents();
		theGRMPackage.initializePackageContents();
		theMARTE_AnnexesPackage.initializePackageContents();
		theRSMPackage.initializePackageContents();
		theVSLPackage.initializePackageContents();
		theVariablesPackage.initializePackageContents();
		theOperatorsPackage.initializePackageContents();
		theDataTypesPackage.initializePackageContents();
		theMARTE_DesignModelPackage.initializePackageContents();
		theHLAMPackage.initializePackageContents();
		theHRMPackage.initializePackageContents();
		theHwLogicalPackage.initializePackageContents();
		theHwComputingPackage.initializePackageContents();
		theHwCommunicationPackage.initializePackageContents();
		theHwStoragePackage.initializePackageContents();
		theHwStorageManagerPackage.initializePackageContents();
		theHwMemoryPackage.initializePackageContents();
		theHwTimingPackage.initializePackageContents();
		theHwDevicePackage.initializePackageContents();
		theHwGeneralPackage.initializePackageContents();
		theHwPhysicalPackage.initializePackageContents();
		theHwLayoutPackage.initializePackageContents();
		theHwPowerPackage.initializePackageContents();
		theSRMPackage.initializePackageContents();
		theSW_ResourceCorePackage.initializePackageContents();
		theSW_ConcurrencyPackage.initializePackageContents();
		theSW_BrokeringPackage.initializePackageContents();
		theSW_InteractionPackage.initializePackageContents();
		theGCMPackage.initializePackageContents();
		theMARTE_AnalysisModelPackage.initializePackageContents();
		theGQAMPackage.initializePackageContents();
		theSAMPackage.initializePackageContents();
		thePAMPackage.initializePackageContents();
		theMeasurementUnitsPackage.initializePackageContents();
		theGRM_BasicTypesPackage.initializePackageContents();
		theMARTE_DataTypesPackage.initializePackageContents();
		theBasicNFP_TypesPackage.initializePackageContents();
		theTimeTypesLibraryPackage.initializePackageContents();
		theTimeLibraryPackage.initializePackageContents();
		theRS_LibraryPackage.initializePackageContents();
		theMARTE_PrimitivesTypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAllocPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AllocPackage.eNS_URI, theAllocPackage);
		return theAllocPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getAllocated() {
		return allocatedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getAllocated_Base_NamedElement() {
		return (EReference) allocatedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getAllocated_AllocatedTo() {
		return (EReference) allocatedEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getAllocated_AllocatedFrom() {
		return (EReference) allocatedEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getAllocated_Kind() {
		return (EAttribute) allocatedEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getAllocateActivityGroup() {
		return allocateActivityGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getAllocateActivityGroup_IsUnique() {
		return (EAttribute) allocateActivityGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getAllocateActivityGroup_Base_ActivityPartition() {
		return (EReference) allocateActivityGroupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getNfpRefine() {
		return nfpRefineEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getNfpRefine_Base_Dependency() {
		return (EReference) nfpRefineEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getNfpRefine_Constraint() {
		return (EReference) nfpRefineEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getAssign() {
		return assignEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getAssign_Kind() {
		return (EAttribute) assignEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getAssign_Nature() {
		return (EAttribute) assignEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getAssign_ImpliedConstraint() {
		return (EReference) assignEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getAssign_From() {
		return (EReference) assignEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getAssign_To() {
		return (EReference) assignEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getAssign_Base_Comment() {
		return (EReference) assignEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getAllocate() {
		return allocateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getAllocate_Kind() {
		return (EAttribute) allocateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getAllocate_Nature() {
		return (EAttribute) allocateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getAllocate_Base_Abstraction() {
		return (EReference) allocateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getAllocate_ImpliedConstraint() {
		return (EReference) allocateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EEnum getAllocationEndKind() {
		return allocationEndKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EEnum getAllocationNature() {
		return allocationNatureEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EEnum getAllocationKind() {
		return allocationKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EEnum getAssignmentKind() {
		return assignmentKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EEnum getAssignmentNature() {
		return assignmentNatureEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public AllocFactory getAllocFactory() {
		return (AllocFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package. This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) {
			return;
		}
		isCreated = true;

		// Create classes and their features
		allocatedEClass = createEClass(ALLOCATED);
		createEReference(allocatedEClass, ALLOCATED__BASE_NAMED_ELEMENT);
		createEReference(allocatedEClass, ALLOCATED__ALLOCATED_TO);
		createEReference(allocatedEClass, ALLOCATED__ALLOCATED_FROM);
		createEAttribute(allocatedEClass, ALLOCATED__KIND);

		allocateActivityGroupEClass = createEClass(ALLOCATE_ACTIVITY_GROUP);
		createEAttribute(allocateActivityGroupEClass, ALLOCATE_ACTIVITY_GROUP__IS_UNIQUE);
		createEReference(allocateActivityGroupEClass, ALLOCATE_ACTIVITY_GROUP__BASE_ACTIVITY_PARTITION);

		nfpRefineEClass = createEClass(NFP_REFINE);
		createEReference(nfpRefineEClass, NFP_REFINE__BASE_DEPENDENCY);
		createEReference(nfpRefineEClass, NFP_REFINE__CONSTRAINT);

		assignEClass = createEClass(ASSIGN);
		createEAttribute(assignEClass, ASSIGN__KIND);
		createEAttribute(assignEClass, ASSIGN__NATURE);
		createEReference(assignEClass, ASSIGN__IMPLIED_CONSTRAINT);
		createEReference(assignEClass, ASSIGN__FROM);
		createEReference(assignEClass, ASSIGN__TO);
		createEReference(assignEClass, ASSIGN__BASE_COMMENT);

		allocateEClass = createEClass(ALLOCATE);
		createEAttribute(allocateEClass, ALLOCATE__KIND);
		createEAttribute(allocateEClass, ALLOCATE__NATURE);
		createEReference(allocateEClass, ALLOCATE__BASE_ABSTRACTION);
		createEReference(allocateEClass, ALLOCATE__IMPLIED_CONSTRAINT);

		// Create enums
		allocationEndKindEEnum = createEEnum(ALLOCATION_END_KIND);
		allocationNatureEEnum = createEEnum(ALLOCATION_NATURE);
		allocationKindEEnum = createEEnum(ALLOCATION_KIND);
		assignmentKindEEnum = createEEnum(ASSIGNMENT_KIND);
		assignmentNatureEEnum = createEEnum(ASSIGNMENT_NATURE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) {
			return;
		}
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		UMLPackage theUMLPackage = (UMLPackage) EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		MARTE_PrimitivesTypesPackage theMARTE_PrimitivesTypesPackage = (MARTE_PrimitivesTypesPackage) EPackage.Registry.INSTANCE.getEPackage(MARTE_PrimitivesTypesPackage.eNS_URI);
		NFPsPackage theNFPsPackage = (NFPsPackage) EPackage.Registry.INSTANCE.getEPackage(NFPsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(allocatedEClass, Allocated.class, "Allocated", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getAllocated_Base_NamedElement(), theUMLPackage.getNamedElement(), null, "base_NamedElement", null, 0, 1, Allocated.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getAllocated_AllocatedTo(), this.getAllocated(), null, "allocatedTo", null, 0, -1, Allocated.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getAllocated_AllocatedFrom(), this.getAllocated(), null, "allocatedFrom", null, 0, -1, Allocated.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getAllocated_Kind(), this.getAllocationEndKind(), "kind", null, 0, 1, Allocated.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(allocateActivityGroupEClass, AllocateActivityGroup.class, "AllocateActivityGroup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getAllocateActivityGroup_IsUnique(), theMARTE_PrimitivesTypesPackage.getBoolean(), "isUnique", "false", 0, 1, AllocateActivityGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$ //$NON-NLS-2$
				!IS_ORDERED);
		initEReference(getAllocateActivityGroup_Base_ActivityPartition(), theUMLPackage.getActivityPartition(), null, "base_ActivityPartition", null, 0, 1, AllocateActivityGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(nfpRefineEClass, NfpRefine.class, "NfpRefine", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getNfpRefine_Base_Dependency(), theUMLPackage.getDependency(), null, "base_Dependency", null, 0, 1, NfpRefine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getNfpRefine_Constraint(), theNFPsPackage.getNfpConstraint(), null, "constraint", null, 0, -1, NfpRefine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);

		initEClass(assignEClass, Assign.class, "Assign", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getAssign_Kind(), this.getAssignmentKind(), "kind", null, 1, 1, Assign.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getAssign_Nature(), this.getAssignmentNature(), "nature", null, 1, 1, Assign.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getAssign_ImpliedConstraint(), theNFPsPackage.getNfpConstraint(), null, "impliedConstraint", null, 0, -1, Assign.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getAssign_From(), theUMLPackage.getElement(), null, "from", null, 1, -1, Assign.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getAssign_To(), theUMLPackage.getElement(), null, "to", null, 1, -1, Assign.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getAssign_Base_Comment(), theUMLPackage.getComment(), null, "base_Comment", null, 0, 1, Assign.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(allocateEClass, Allocate.class, "Allocate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getAllocate_Kind(), this.getAllocationKind(), "kind", null, 0, 1, Allocate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getAllocate_Nature(), this.getAllocationNature(), "nature", null, 0, 1, Allocate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getAllocate_Base_Abstraction(), theUMLPackage.getAbstraction(), null, "base_Abstraction", null, 0, 1, Allocate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getAllocate_ImpliedConstraint(), theNFPsPackage.getNfpConstraint(), null, "impliedConstraint", null, 0, -1, Allocate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(allocationEndKindEEnum, AllocationEndKind.class, "AllocationEndKind"); //$NON-NLS-1$
		addEEnumLiteral(allocationEndKindEEnum, AllocationEndKind.UNDEF);
		addEEnumLiteral(allocationEndKindEEnum, AllocationEndKind.APPLICATION);
		addEEnumLiteral(allocationEndKindEEnum, AllocationEndKind.EXECUTION_PLATFORM);
		addEEnumLiteral(allocationEndKindEEnum, AllocationEndKind.BOTH);

		initEEnum(allocationNatureEEnum, AllocationNature.class, "AllocationNature"); //$NON-NLS-1$
		addEEnumLiteral(allocationNatureEEnum, AllocationNature.SPATIAL_DISTRIBUTION);
		addEEnumLiteral(allocationNatureEEnum, AllocationNature.TIME_SCHEDULING);

		initEEnum(allocationKindEEnum, AllocationKind.class, "AllocationKind"); //$NON-NLS-1$
		addEEnumLiteral(allocationKindEEnum, AllocationKind.STRUCTURAL);
		addEEnumLiteral(allocationKindEEnum, AllocationKind.BEHAVIORAL);
		addEEnumLiteral(allocationKindEEnum, AllocationKind.HYBRID);

		initEEnum(assignmentKindEEnum, AssignmentKind.class, "AssignmentKind"); //$NON-NLS-1$
		addEEnumLiteral(assignmentKindEEnum, AssignmentKind.STRUCTURAL);
		addEEnumLiteral(assignmentKindEEnum, AssignmentKind.BEHAVIORAL);
		addEEnumLiteral(assignmentKindEEnum, AssignmentKind.HYBRID);

		initEEnum(assignmentNatureEEnum, AssignmentNature.class, "AssignmentNature"); //$NON-NLS-1$
		addEEnumLiteral(assignmentNatureEEnum, AssignmentNature.SPATIAL_DISTRIBUTION);
		addEEnumLiteral(assignmentNatureEEnum, AssignmentNature.TIME_SCHEDULING);
	}

} // AllocPackageImpl

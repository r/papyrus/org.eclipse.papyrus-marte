/**
 * Copyright (c) 2010, 2016 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Chokri MRAIDHA (CEA LIST) chokri.mraidha@cea.fr - Initial API and implementation
 *
 */
package org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.MARTE.MARTEPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GQAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GQAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.MARTE_AnalysisModelPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.PAM.PAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.PAM.impl.PAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.SAM.SAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.SAM.impl.SAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.impl.MARTE_AnalysisModelPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.MARTE_AnnexesPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.RSM.RSMPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.RSM.impl.RSMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.DataTypes.DataTypesPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.DataTypes.impl.DataTypesPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Operators.OperatorsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Operators.impl.OperatorsPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.VSLPackage;
import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Variables.VariablesPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Variables.impl.VariablesPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.impl.VSLPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_Annexes.impl.MARTE_AnnexesPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.GCMPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.impl.GCMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HLAM.HLAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HLAM.impl.HLAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HRMPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwGeneral.HwGeneralPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwGeneral.impl.HwGeneralPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwCommunication.HwCommunicationPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwCommunication.impl.HwCommunicationPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwComputing.HwComputingPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwComputing.impl.HwComputingPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwDevice.HwDevicePackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwDevice.impl.HwDevicePackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwLogicalPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwMemory.HwMemoryPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwMemory.impl.HwMemoryPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwStorageManager.HwStorageManagerPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwStorageManager.impl.HwStorageManagerPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwStoragePackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.impl.HwStoragePackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwTiming.HwTimingPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwTiming.impl.HwTimingPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.impl.HwLogicalPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwLayout.HwLayoutPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwLayout.impl.HwLayoutPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwPhysicalPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwPower.HwPowerPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwPower.impl.HwPowerPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.impl.HwPhysicalPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.impl.HRMPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.MARTE_DesignModelPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SRMPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.SW_BrokeringPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.impl.SW_BrokeringPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SW_ConcurrencyPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.impl.SW_ConcurrencyPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.SW_InteractionPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.impl.SW_InteractionPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SW_ResourceCorePackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.impl.SW_ResourceCorePackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.impl.SRMPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.impl.MARTE_DesignModelPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.AllocPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.impl.AllocPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.CoreElements.CoreElementsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.CoreElements.impl.CoreElementsPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.Acquire;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.ClockResource;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.CommunicationEndPoint;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.CommunicationMedia;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.ComputingResource;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.ConcurrencyResource;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.DeviceResource;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.GRMFactory;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.GRMPackage;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.GrService;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.MutualExclusionResource;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.ProcessingResource;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.Release;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.Resource;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.ResourceUsage;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.SchedulableResource;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.Scheduler;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.SecondaryScheduler;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.StorageResource;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.SynchronizationResource;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.TimerResource;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.TimingResource;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.MARTE_FoundationsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.NFPs.NFPsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.NFPs.impl.NFPsPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.TimePackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.impl.TimePackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.impl.MARTE_FoundationsPackageImpl;

import org.eclipse.papyrus.MARTE.impl.MARTEPackageImpl;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.GRM_BasicTypesPackage;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.impl.GRM_BasicTypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.impl.MARTE_DataTypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.impl.MARTE_PrimitivesTypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.impl.MeasurementUnitsPackageImpl;

import org.eclipse.papyrus.MARTE_Library.RS_Library.RS_LibraryPackage;

import org.eclipse.papyrus.MARTE_Library.RS_Library.impl.RS_LibraryPackageImpl;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.TimeLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.impl.TimeLibraryPackageImpl;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.TimeTypesLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.impl.TimeTypesLibraryPackageImpl;

import org.eclipse.uml2.types.TypesPackage;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GRMPackageImpl extends EPackageImpl implements GRMPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass resourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass storageResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass communicationEndPointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass synchronizationResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass concurrencyResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass schedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass processingResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass computingResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass mutualExclusionResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass schedulableResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass secondarySchedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass communicationMediaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass deviceResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass timingResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass clockResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass timerResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass grServiceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass releaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass acquireEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass resourceUsageEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.GRMPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private GRMPackageImpl() {
		super(eNS_URI, GRMFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>
	 * This method is used to initialize {@link GRMPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static GRMPackage init() {
		if (isInited) {
			return (GRMPackage) EPackage.Registry.INSTANCE.getEPackage(GRMPackage.eNS_URI);
		}

		// Obtain or create and register package
		Object registeredGRMPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		GRMPackageImpl theGRMPackage = registeredGRMPackage instanceof GRMPackageImpl ? (GRMPackageImpl) registeredGRMPackage : new GRMPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTEPackage.eNS_URI);
		MARTEPackageImpl theMARTEPackage = (MARTEPackageImpl) (registeredPackage instanceof MARTEPackageImpl ? registeredPackage : MARTEPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_FoundationsPackage.eNS_URI);
		MARTE_FoundationsPackageImpl theMARTE_FoundationsPackage = (MARTE_FoundationsPackageImpl) (registeredPackage instanceof MARTE_FoundationsPackageImpl ? registeredPackage : MARTE_FoundationsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(NFPsPackage.eNS_URI);
		NFPsPackageImpl theNFPsPackage = (NFPsPackageImpl) (registeredPackage instanceof NFPsPackageImpl ? registeredPackage : NFPsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CoreElementsPackage.eNS_URI);
		CoreElementsPackageImpl theCoreElementsPackage = (CoreElementsPackageImpl) (registeredPackage instanceof CoreElementsPackageImpl ? registeredPackage : CoreElementsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(AllocPackage.eNS_URI);
		AllocPackageImpl theAllocPackage = (AllocPackageImpl) (registeredPackage instanceof AllocPackageImpl ? registeredPackage : AllocPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimePackage.eNS_URI);
		TimePackageImpl theTimePackage = (TimePackageImpl) (registeredPackage instanceof TimePackageImpl ? registeredPackage : TimePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_AnnexesPackage.eNS_URI);
		MARTE_AnnexesPackageImpl theMARTE_AnnexesPackage = (MARTE_AnnexesPackageImpl) (registeredPackage instanceof MARTE_AnnexesPackageImpl ? registeredPackage : MARTE_AnnexesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(RSMPackage.eNS_URI);
		RSMPackageImpl theRSMPackage = (RSMPackageImpl) (registeredPackage instanceof RSMPackageImpl ? registeredPackage : RSMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(VSLPackage.eNS_URI);
		VSLPackageImpl theVSLPackage = (VSLPackageImpl) (registeredPackage instanceof VSLPackageImpl ? registeredPackage : VSLPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(VariablesPackage.eNS_URI);
		VariablesPackageImpl theVariablesPackage = (VariablesPackageImpl) (registeredPackage instanceof VariablesPackageImpl ? registeredPackage : VariablesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI);
		OperatorsPackageImpl theOperatorsPackage = (OperatorsPackageImpl) (registeredPackage instanceof OperatorsPackageImpl ? registeredPackage : OperatorsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(DataTypesPackage.eNS_URI);
		DataTypesPackageImpl theDataTypesPackage = (DataTypesPackageImpl) (registeredPackage instanceof DataTypesPackageImpl ? registeredPackage : DataTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_DesignModelPackage.eNS_URI);
		MARTE_DesignModelPackageImpl theMARTE_DesignModelPackage = (MARTE_DesignModelPackageImpl) (registeredPackage instanceof MARTE_DesignModelPackageImpl ? registeredPackage : MARTE_DesignModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HLAMPackage.eNS_URI);
		HLAMPackageImpl theHLAMPackage = (HLAMPackageImpl) (registeredPackage instanceof HLAMPackageImpl ? registeredPackage : HLAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HRMPackage.eNS_URI);
		HRMPackageImpl theHRMPackage = (HRMPackageImpl) (registeredPackage instanceof HRMPackageImpl ? registeredPackage : HRMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwLogicalPackage.eNS_URI);
		HwLogicalPackageImpl theHwLogicalPackage = (HwLogicalPackageImpl) (registeredPackage instanceof HwLogicalPackageImpl ? registeredPackage : HwLogicalPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwComputingPackage.eNS_URI);
		HwComputingPackageImpl theHwComputingPackage = (HwComputingPackageImpl) (registeredPackage instanceof HwComputingPackageImpl ? registeredPackage : HwComputingPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwCommunicationPackage.eNS_URI);
		HwCommunicationPackageImpl theHwCommunicationPackage = (HwCommunicationPackageImpl) (registeredPackage instanceof HwCommunicationPackageImpl ? registeredPackage : HwCommunicationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwStoragePackage.eNS_URI);
		HwStoragePackageImpl theHwStoragePackage = (HwStoragePackageImpl) (registeredPackage instanceof HwStoragePackageImpl ? registeredPackage : HwStoragePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwStorageManagerPackage.eNS_URI);
		HwStorageManagerPackageImpl theHwStorageManagerPackage = (HwStorageManagerPackageImpl) (registeredPackage instanceof HwStorageManagerPackageImpl ? registeredPackage : HwStorageManagerPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwMemoryPackage.eNS_URI);
		HwMemoryPackageImpl theHwMemoryPackage = (HwMemoryPackageImpl) (registeredPackage instanceof HwMemoryPackageImpl ? registeredPackage : HwMemoryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwTimingPackage.eNS_URI);
		HwTimingPackageImpl theHwTimingPackage = (HwTimingPackageImpl) (registeredPackage instanceof HwTimingPackageImpl ? registeredPackage : HwTimingPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwDevicePackage.eNS_URI);
		HwDevicePackageImpl theHwDevicePackage = (HwDevicePackageImpl) (registeredPackage instanceof HwDevicePackageImpl ? registeredPackage : HwDevicePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwGeneralPackage.eNS_URI);
		HwGeneralPackageImpl theHwGeneralPackage = (HwGeneralPackageImpl) (registeredPackage instanceof HwGeneralPackageImpl ? registeredPackage : HwGeneralPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwPhysicalPackage.eNS_URI);
		HwPhysicalPackageImpl theHwPhysicalPackage = (HwPhysicalPackageImpl) (registeredPackage instanceof HwPhysicalPackageImpl ? registeredPackage : HwPhysicalPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwLayoutPackage.eNS_URI);
		HwLayoutPackageImpl theHwLayoutPackage = (HwLayoutPackageImpl) (registeredPackage instanceof HwLayoutPackageImpl ? registeredPackage : HwLayoutPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwPowerPackage.eNS_URI);
		HwPowerPackageImpl theHwPowerPackage = (HwPowerPackageImpl) (registeredPackage instanceof HwPowerPackageImpl ? registeredPackage : HwPowerPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SRMPackage.eNS_URI);
		SRMPackageImpl theSRMPackage = (SRMPackageImpl) (registeredPackage instanceof SRMPackageImpl ? registeredPackage : SRMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_ResourceCorePackage.eNS_URI);
		SW_ResourceCorePackageImpl theSW_ResourceCorePackage = (SW_ResourceCorePackageImpl) (registeredPackage instanceof SW_ResourceCorePackageImpl ? registeredPackage : SW_ResourceCorePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_ConcurrencyPackage.eNS_URI);
		SW_ConcurrencyPackageImpl theSW_ConcurrencyPackage = (SW_ConcurrencyPackageImpl) (registeredPackage instanceof SW_ConcurrencyPackageImpl ? registeredPackage : SW_ConcurrencyPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_BrokeringPackage.eNS_URI);
		SW_BrokeringPackageImpl theSW_BrokeringPackage = (SW_BrokeringPackageImpl) (registeredPackage instanceof SW_BrokeringPackageImpl ? registeredPackage : SW_BrokeringPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_InteractionPackage.eNS_URI);
		SW_InteractionPackageImpl theSW_InteractionPackage = (SW_InteractionPackageImpl) (registeredPackage instanceof SW_InteractionPackageImpl ? registeredPackage : SW_InteractionPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GCMPackage.eNS_URI);
		GCMPackageImpl theGCMPackage = (GCMPackageImpl) (registeredPackage instanceof GCMPackageImpl ? registeredPackage : GCMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_AnalysisModelPackage.eNS_URI);
		MARTE_AnalysisModelPackageImpl theMARTE_AnalysisModelPackage = (MARTE_AnalysisModelPackageImpl) (registeredPackage instanceof MARTE_AnalysisModelPackageImpl ? registeredPackage : MARTE_AnalysisModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GQAMPackage.eNS_URI);
		GQAMPackageImpl theGQAMPackage = (GQAMPackageImpl) (registeredPackage instanceof GQAMPackageImpl ? registeredPackage : GQAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SAMPackage.eNS_URI);
		SAMPackageImpl theSAMPackage = (SAMPackageImpl) (registeredPackage instanceof SAMPackageImpl ? registeredPackage : SAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(PAMPackage.eNS_URI);
		PAMPackageImpl thePAMPackage = (PAMPackageImpl) (registeredPackage instanceof PAMPackageImpl ? registeredPackage : PAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MeasurementUnitsPackage.eNS_URI);
		MeasurementUnitsPackageImpl theMeasurementUnitsPackage = (MeasurementUnitsPackageImpl) (registeredPackage instanceof MeasurementUnitsPackageImpl ? registeredPackage : MeasurementUnitsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GRM_BasicTypesPackage.eNS_URI);
		GRM_BasicTypesPackageImpl theGRM_BasicTypesPackage = (GRM_BasicTypesPackageImpl) (registeredPackage instanceof GRM_BasicTypesPackageImpl ? registeredPackage : GRM_BasicTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_DataTypesPackage.eNS_URI);
		MARTE_DataTypesPackageImpl theMARTE_DataTypesPackage = (MARTE_DataTypesPackageImpl) (registeredPackage instanceof MARTE_DataTypesPackageImpl ? registeredPackage : MARTE_DataTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BasicNFP_TypesPackage.eNS_URI);
		BasicNFP_TypesPackageImpl theBasicNFP_TypesPackage = (BasicNFP_TypesPackageImpl) (registeredPackage instanceof BasicNFP_TypesPackageImpl ? registeredPackage : BasicNFP_TypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimeTypesLibraryPackage.eNS_URI);
		TimeTypesLibraryPackageImpl theTimeTypesLibraryPackage = (TimeTypesLibraryPackageImpl) (registeredPackage instanceof TimeTypesLibraryPackageImpl ? registeredPackage : TimeTypesLibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimeLibraryPackage.eNS_URI);
		TimeLibraryPackageImpl theTimeLibraryPackage = (TimeLibraryPackageImpl) (registeredPackage instanceof TimeLibraryPackageImpl ? registeredPackage : TimeLibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(RS_LibraryPackage.eNS_URI);
		RS_LibraryPackageImpl theRS_LibraryPackage = (RS_LibraryPackageImpl) (registeredPackage instanceof RS_LibraryPackageImpl ? registeredPackage : RS_LibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_PrimitivesTypesPackage.eNS_URI);
		MARTE_PrimitivesTypesPackageImpl theMARTE_PrimitivesTypesPackage = (MARTE_PrimitivesTypesPackageImpl) (registeredPackage instanceof MARTE_PrimitivesTypesPackageImpl ? registeredPackage : MARTE_PrimitivesTypesPackage.eINSTANCE);

		// Create package meta-data objects
		theGRMPackage.createPackageContents();
		theMARTEPackage.createPackageContents();
		theMARTE_FoundationsPackage.createPackageContents();
		theNFPsPackage.createPackageContents();
		theCoreElementsPackage.createPackageContents();
		theAllocPackage.createPackageContents();
		theTimePackage.createPackageContents();
		theMARTE_AnnexesPackage.createPackageContents();
		theRSMPackage.createPackageContents();
		theVSLPackage.createPackageContents();
		theVariablesPackage.createPackageContents();
		theOperatorsPackage.createPackageContents();
		theDataTypesPackage.createPackageContents();
		theMARTE_DesignModelPackage.createPackageContents();
		theHLAMPackage.createPackageContents();
		theHRMPackage.createPackageContents();
		theHwLogicalPackage.createPackageContents();
		theHwComputingPackage.createPackageContents();
		theHwCommunicationPackage.createPackageContents();
		theHwStoragePackage.createPackageContents();
		theHwStorageManagerPackage.createPackageContents();
		theHwMemoryPackage.createPackageContents();
		theHwTimingPackage.createPackageContents();
		theHwDevicePackage.createPackageContents();
		theHwGeneralPackage.createPackageContents();
		theHwPhysicalPackage.createPackageContents();
		theHwLayoutPackage.createPackageContents();
		theHwPowerPackage.createPackageContents();
		theSRMPackage.createPackageContents();
		theSW_ResourceCorePackage.createPackageContents();
		theSW_ConcurrencyPackage.createPackageContents();
		theSW_BrokeringPackage.createPackageContents();
		theSW_InteractionPackage.createPackageContents();
		theGCMPackage.createPackageContents();
		theMARTE_AnalysisModelPackage.createPackageContents();
		theGQAMPackage.createPackageContents();
		theSAMPackage.createPackageContents();
		thePAMPackage.createPackageContents();
		theMeasurementUnitsPackage.createPackageContents();
		theGRM_BasicTypesPackage.createPackageContents();
		theMARTE_DataTypesPackage.createPackageContents();
		theBasicNFP_TypesPackage.createPackageContents();
		theTimeTypesLibraryPackage.createPackageContents();
		theTimeLibraryPackage.createPackageContents();
		theRS_LibraryPackage.createPackageContents();
		theMARTE_PrimitivesTypesPackage.createPackageContents();

		// Initialize created meta-data
		theGRMPackage.initializePackageContents();
		theMARTEPackage.initializePackageContents();
		theMARTE_FoundationsPackage.initializePackageContents();
		theNFPsPackage.initializePackageContents();
		theCoreElementsPackage.initializePackageContents();
		theAllocPackage.initializePackageContents();
		theTimePackage.initializePackageContents();
		theMARTE_AnnexesPackage.initializePackageContents();
		theRSMPackage.initializePackageContents();
		theVSLPackage.initializePackageContents();
		theVariablesPackage.initializePackageContents();
		theOperatorsPackage.initializePackageContents();
		theDataTypesPackage.initializePackageContents();
		theMARTE_DesignModelPackage.initializePackageContents();
		theHLAMPackage.initializePackageContents();
		theHRMPackage.initializePackageContents();
		theHwLogicalPackage.initializePackageContents();
		theHwComputingPackage.initializePackageContents();
		theHwCommunicationPackage.initializePackageContents();
		theHwStoragePackage.initializePackageContents();
		theHwStorageManagerPackage.initializePackageContents();
		theHwMemoryPackage.initializePackageContents();
		theHwTimingPackage.initializePackageContents();
		theHwDevicePackage.initializePackageContents();
		theHwGeneralPackage.initializePackageContents();
		theHwPhysicalPackage.initializePackageContents();
		theHwLayoutPackage.initializePackageContents();
		theHwPowerPackage.initializePackageContents();
		theSRMPackage.initializePackageContents();
		theSW_ResourceCorePackage.initializePackageContents();
		theSW_ConcurrencyPackage.initializePackageContents();
		theSW_BrokeringPackage.initializePackageContents();
		theSW_InteractionPackage.initializePackageContents();
		theGCMPackage.initializePackageContents();
		theMARTE_AnalysisModelPackage.initializePackageContents();
		theGQAMPackage.initializePackageContents();
		theSAMPackage.initializePackageContents();
		thePAMPackage.initializePackageContents();
		theMeasurementUnitsPackage.initializePackageContents();
		theGRM_BasicTypesPackage.initializePackageContents();
		theMARTE_DataTypesPackage.initializePackageContents();
		theBasicNFP_TypesPackage.initializePackageContents();
		theTimeTypesLibraryPackage.initializePackageContents();
		theTimeLibraryPackage.initializePackageContents();
		theRS_LibraryPackage.initializePackageContents();
		theMARTE_PrimitivesTypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theGRMPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(GRMPackage.eNS_URI, theGRMPackage);
		return theGRMPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getResource() {
		return resourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getResource_ResMult() {
		return (EAttribute) resourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getResource_IsProtected() {
		return (EAttribute) resourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getResource_IsActive() {
		return (EAttribute) resourceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getResource_Base_Property() {
		return (EReference) resourceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getResource_Base_InstanceSpecification() {
		return (EReference) resourceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getResource_Base_Classifier() {
		return (EReference) resourceEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getResource_Base_Lifeline() {
		return (EReference) resourceEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getResource_Base_ConnectableElement() {
		return (EReference) resourceEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getStorageResource() {
		return storageResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getStorageResource_ElementSize() {
		return (EAttribute) storageResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getCommunicationEndPoint() {
		return communicationEndPointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getCommunicationEndPoint_PacketSize() {
		return (EAttribute) communicationEndPointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getSynchronizationResource() {
		return synchronizationResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getConcurrencyResource() {
		return concurrencyResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getScheduler() {
		return schedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getScheduler_IsPreemptible() {
		return (EAttribute) schedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getScheduler_SchedPolicy() {
		return (EAttribute) schedulerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getScheduler_OtherSchedPolicy() {
		return (EAttribute) schedulerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getScheduler_Schedule() {
		return (EAttribute) schedulerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getScheduler_ProcessingUnits() {
		return (EReference) schedulerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getScheduler_Host() {
		return (EReference) schedulerEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getScheduler_ProtectedSharedResources() {
		return (EReference) schedulerEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getScheduler_SchedulableResources() {
		return (EReference) schedulerEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getProcessingResource() {
		return processingResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getProcessingResource_SpeedFactor() {
		return (EAttribute) processingResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getProcessingResource_MainScheduler() {
		return (EReference) processingResourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getComputingResource() {
		return computingResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getMutualExclusionResource() {
		return mutualExclusionResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getMutualExclusionResource_ProtectKind() {
		return (EAttribute) mutualExclusionResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getMutualExclusionResource_Ceiling() {
		return (EAttribute) mutualExclusionResourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getMutualExclusionResource_OtherProtectProtocol() {
		return (EAttribute) mutualExclusionResourceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getMutualExclusionResource_Scheduler() {
		return (EReference) mutualExclusionResourceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getSchedulableResource() {
		return schedulableResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getSchedulableResource_SchedParams() {
		return (EAttribute) schedulableResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSchedulableResource_DependentScheduler() {
		return (EReference) schedulableResourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSchedulableResource_Host() {
		return (EReference) schedulableResourceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getSecondaryScheduler() {
		return secondarySchedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getSecondaryScheduler_VirtualProcessingUnits() {
		return (EReference) secondarySchedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getCommunicationMedia() {
		return communicationMediaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getCommunicationMedia_ElementSize() {
		return (EAttribute) communicationMediaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getCommunicationMedia_Base_Connector() {
		return (EReference) communicationMediaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getCommunicationMedia_TransmMode() {
		return (EAttribute) communicationMediaEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getCommunicationMedia_BlockT() {
		return (EAttribute) communicationMediaEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getCommunicationMedia_PacketT() {
		return (EAttribute) communicationMediaEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getCommunicationMedia_Capacity() {
		return (EAttribute) communicationMediaEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getDeviceResource() {
		return deviceResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getTimingResource() {
		return timingResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getClockResource() {
		return clockResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getTimerResource() {
		return timerResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getTimerResource_Duration() {
		return (EAttribute) timerResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getTimerResource_IsPeriodic() {
		return (EAttribute) timerResourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getGrService() {
		return grServiceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getGrService_Owner() {
		return (EReference) grServiceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getGrService_Base_ExecutionSpecification() {
		return (EReference) grServiceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getGrService_Base_BehavioralFeature() {
		return (EReference) grServiceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getGrService_Base_Behavior() {
		return (EReference) grServiceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getGrService_Base_Collaboration() {
		return (EReference) grServiceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getGrService_Base_CollaborationUse() {
		return (EReference) grServiceEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getRelease() {
		return releaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getAcquire() {
		return acquireEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getAcquire_IsBlocking() {
		return (EAttribute) acquireEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getResourceUsage() {
		return resourceUsageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getResourceUsage_ExecTime() {
		return (EAttribute) resourceUsageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getResourceUsage_AllocatedMemory() {
		return (EAttribute) resourceUsageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getResourceUsage_UsedMemory() {
		return (EAttribute) resourceUsageEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getResourceUsage_PowerPeak() {
		return (EAttribute) resourceUsageEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getResourceUsage_Energy() {
		return (EAttribute) resourceUsageEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getResourceUsage_Base_NamedElement() {
		return (EReference) resourceUsageEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getResourceUsage_SubUsage() {
		return (EReference) resourceUsageEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getResourceUsage_UsedResources() {
		return (EReference) resourceUsageEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getResourceUsage_MsgSize() {
		return (EAttribute) resourceUsageEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public GRMFactory getGRMFactory() {
		return (GRMFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package. This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) {
			return;
		}
		isCreated = true;

		// Create classes and their features
		resourceEClass = createEClass(RESOURCE);
		createEAttribute(resourceEClass, RESOURCE__RES_MULT);
		createEAttribute(resourceEClass, RESOURCE__IS_PROTECTED);
		createEAttribute(resourceEClass, RESOURCE__IS_ACTIVE);
		createEReference(resourceEClass, RESOURCE__BASE_PROPERTY);
		createEReference(resourceEClass, RESOURCE__BASE_INSTANCE_SPECIFICATION);
		createEReference(resourceEClass, RESOURCE__BASE_CLASSIFIER);
		createEReference(resourceEClass, RESOURCE__BASE_LIFELINE);
		createEReference(resourceEClass, RESOURCE__BASE_CONNECTABLE_ELEMENT);

		storageResourceEClass = createEClass(STORAGE_RESOURCE);
		createEAttribute(storageResourceEClass, STORAGE_RESOURCE__ELEMENT_SIZE);

		communicationEndPointEClass = createEClass(COMMUNICATION_END_POINT);
		createEAttribute(communicationEndPointEClass, COMMUNICATION_END_POINT__PACKET_SIZE);

		synchronizationResourceEClass = createEClass(SYNCHRONIZATION_RESOURCE);

		concurrencyResourceEClass = createEClass(CONCURRENCY_RESOURCE);

		schedulerEClass = createEClass(SCHEDULER);
		createEAttribute(schedulerEClass, SCHEDULER__IS_PREEMPTIBLE);
		createEAttribute(schedulerEClass, SCHEDULER__SCHED_POLICY);
		createEAttribute(schedulerEClass, SCHEDULER__OTHER_SCHED_POLICY);
		createEAttribute(schedulerEClass, SCHEDULER__SCHEDULE);
		createEReference(schedulerEClass, SCHEDULER__PROCESSING_UNITS);
		createEReference(schedulerEClass, SCHEDULER__HOST);
		createEReference(schedulerEClass, SCHEDULER__PROTECTED_SHARED_RESOURCES);
		createEReference(schedulerEClass, SCHEDULER__SCHEDULABLE_RESOURCES);

		processingResourceEClass = createEClass(PROCESSING_RESOURCE);
		createEAttribute(processingResourceEClass, PROCESSING_RESOURCE__SPEED_FACTOR);
		createEReference(processingResourceEClass, PROCESSING_RESOURCE__MAIN_SCHEDULER);

		computingResourceEClass = createEClass(COMPUTING_RESOURCE);

		mutualExclusionResourceEClass = createEClass(MUTUAL_EXCLUSION_RESOURCE);
		createEAttribute(mutualExclusionResourceEClass, MUTUAL_EXCLUSION_RESOURCE__PROTECT_KIND);
		createEAttribute(mutualExclusionResourceEClass, MUTUAL_EXCLUSION_RESOURCE__CEILING);
		createEAttribute(mutualExclusionResourceEClass, MUTUAL_EXCLUSION_RESOURCE__OTHER_PROTECT_PROTOCOL);
		createEReference(mutualExclusionResourceEClass, MUTUAL_EXCLUSION_RESOURCE__SCHEDULER);

		schedulableResourceEClass = createEClass(SCHEDULABLE_RESOURCE);
		createEAttribute(schedulableResourceEClass, SCHEDULABLE_RESOURCE__SCHED_PARAMS);
		createEReference(schedulableResourceEClass, SCHEDULABLE_RESOURCE__DEPENDENT_SCHEDULER);
		createEReference(schedulableResourceEClass, SCHEDULABLE_RESOURCE__HOST);

		secondarySchedulerEClass = createEClass(SECONDARY_SCHEDULER);
		createEReference(secondarySchedulerEClass, SECONDARY_SCHEDULER__VIRTUAL_PROCESSING_UNITS);

		communicationMediaEClass = createEClass(COMMUNICATION_MEDIA);
		createEAttribute(communicationMediaEClass, COMMUNICATION_MEDIA__ELEMENT_SIZE);
		createEReference(communicationMediaEClass, COMMUNICATION_MEDIA__BASE_CONNECTOR);
		createEAttribute(communicationMediaEClass, COMMUNICATION_MEDIA__TRANSM_MODE);
		createEAttribute(communicationMediaEClass, COMMUNICATION_MEDIA__BLOCK_T);
		createEAttribute(communicationMediaEClass, COMMUNICATION_MEDIA__PACKET_T);
		createEAttribute(communicationMediaEClass, COMMUNICATION_MEDIA__CAPACITY);

		deviceResourceEClass = createEClass(DEVICE_RESOURCE);

		timingResourceEClass = createEClass(TIMING_RESOURCE);

		clockResourceEClass = createEClass(CLOCK_RESOURCE);

		timerResourceEClass = createEClass(TIMER_RESOURCE);
		createEAttribute(timerResourceEClass, TIMER_RESOURCE__DURATION);
		createEAttribute(timerResourceEClass, TIMER_RESOURCE__IS_PERIODIC);

		grServiceEClass = createEClass(GR_SERVICE);
		createEReference(grServiceEClass, GR_SERVICE__OWNER);
		createEReference(grServiceEClass, GR_SERVICE__BASE_EXECUTION_SPECIFICATION);
		createEReference(grServiceEClass, GR_SERVICE__BASE_BEHAVIORAL_FEATURE);
		createEReference(grServiceEClass, GR_SERVICE__BASE_BEHAVIOR);
		createEReference(grServiceEClass, GR_SERVICE__BASE_COLLABORATION);
		createEReference(grServiceEClass, GR_SERVICE__BASE_COLLABORATION_USE);

		releaseEClass = createEClass(RELEASE);

		acquireEClass = createEClass(ACQUIRE);
		createEAttribute(acquireEClass, ACQUIRE__IS_BLOCKING);

		resourceUsageEClass = createEClass(RESOURCE_USAGE);
		createEAttribute(resourceUsageEClass, RESOURCE_USAGE__EXEC_TIME);
		createEAttribute(resourceUsageEClass, RESOURCE_USAGE__ALLOCATED_MEMORY);
		createEAttribute(resourceUsageEClass, RESOURCE_USAGE__USED_MEMORY);
		createEAttribute(resourceUsageEClass, RESOURCE_USAGE__POWER_PEAK);
		createEAttribute(resourceUsageEClass, RESOURCE_USAGE__ENERGY);
		createEReference(resourceUsageEClass, RESOURCE_USAGE__BASE_NAMED_ELEMENT);
		createEReference(resourceUsageEClass, RESOURCE_USAGE__SUB_USAGE);
		createEReference(resourceUsageEClass, RESOURCE_USAGE__USED_RESOURCES);
		createEAttribute(resourceUsageEClass, RESOURCE_USAGE__MSG_SIZE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) {
			return;
		}
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		BasicNFP_TypesPackage theBasicNFP_TypesPackage = (BasicNFP_TypesPackage) EPackage.Registry.INSTANCE.getEPackage(BasicNFP_TypesPackage.eNS_URI);
		MARTE_PrimitivesTypesPackage theMARTE_PrimitivesTypesPackage = (MARTE_PrimitivesTypesPackage) EPackage.Registry.INSTANCE.getEPackage(MARTE_PrimitivesTypesPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage) EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		GRM_BasicTypesPackage theGRM_BasicTypesPackage = (GRM_BasicTypesPackage) EPackage.Registry.INSTANCE.getEPackage(GRM_BasicTypesPackage.eNS_URI);
		MARTE_DataTypesPackage theMARTE_DataTypesPackage = (MARTE_DataTypesPackage) EPackage.Registry.INSTANCE.getEPackage(MARTE_DataTypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		storageResourceEClass.getESuperTypes().add(this.getResource());
		communicationEndPointEClass.getESuperTypes().add(this.getResource());
		synchronizationResourceEClass.getESuperTypes().add(this.getResource());
		concurrencyResourceEClass.getESuperTypes().add(this.getResource());
		schedulerEClass.getESuperTypes().add(this.getResource());
		processingResourceEClass.getESuperTypes().add(this.getResource());
		computingResourceEClass.getESuperTypes().add(this.getProcessingResource());
		mutualExclusionResourceEClass.getESuperTypes().add(this.getResource());
		schedulableResourceEClass.getESuperTypes().add(this.getResource());
		secondarySchedulerEClass.getESuperTypes().add(this.getScheduler());
		communicationMediaEClass.getESuperTypes().add(this.getProcessingResource());
		deviceResourceEClass.getESuperTypes().add(this.getProcessingResource());
		timingResourceEClass.getESuperTypes().add(this.getResource());
		clockResourceEClass.getESuperTypes().add(this.getTimingResource());
		timerResourceEClass.getESuperTypes().add(this.getTimingResource());
		releaseEClass.getESuperTypes().add(this.getGrService());
		acquireEClass.getESuperTypes().add(this.getGrService());

		// Initialize classes and features; add operations and parameters
		initEClass(resourceEClass, Resource.class, "Resource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getResource_ResMult(), theBasicNFP_TypesPackage.getNFP_Integer(), "resMult", "1", 0, 1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEAttribute(getResource_IsProtected(), theMARTE_PrimitivesTypesPackage.getBoolean(), "isProtected", null, 0, 1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getResource_IsActive(), theMARTE_PrimitivesTypesPackage.getBoolean(), "isActive", null, 0, 1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getResource_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 0, 1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getResource_Base_InstanceSpecification(), theUMLPackage.getInstanceSpecification(), null, "base_InstanceSpecification", null, 0, 1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getResource_Base_Classifier(), theUMLPackage.getClassifier(), null, "base_Classifier", null, 0, 1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getResource_Base_Lifeline(), theUMLPackage.getLifeline(), null, "base_Lifeline", null, 0, 1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getResource_Base_ConnectableElement(), theUMLPackage.getConnectableElement(), null, "base_ConnectableElement", null, 0, 1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(storageResourceEClass, StorageResource.class, "StorageResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getStorageResource_ElementSize(), theBasicNFP_TypesPackage.getNFP_Integer(), "elementSize", null, 0, 1, StorageResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(communicationEndPointEClass, CommunicationEndPoint.class, "CommunicationEndPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getCommunicationEndPoint_PacketSize(), theBasicNFP_TypesPackage.getNFP_Integer(), "packetSize", null, 0, 1, CommunicationEndPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);

		initEClass(synchronizationResourceEClass, SynchronizationResource.class, "SynchronizationResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(concurrencyResourceEClass, ConcurrencyResource.class, "ConcurrencyResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(schedulerEClass, Scheduler.class, "Scheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getScheduler_IsPreemptible(), theMARTE_PrimitivesTypesPackage.getBoolean(), "isPreemptible", "true", 0, 1, Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEAttribute(getScheduler_SchedPolicy(), theGRM_BasicTypesPackage.getSchedPolicyKind(), "schedPolicy", "FixedPriority", 0, 1, Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEAttribute(getScheduler_OtherSchedPolicy(), theMARTE_PrimitivesTypesPackage.getString(), "otherSchedPolicy", null, 0, 1, Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getScheduler_Schedule(), theGRM_BasicTypesPackage.getScheduleSpecification(), "schedule", null, 0, 1, Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getScheduler_ProcessingUnits(), this.getProcessingResource(), null, "processingUnits", null, 0, -1, Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getScheduler_Host(), this.getComputingResource(), null, "host", null, 0, 1, Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getScheduler_ProtectedSharedResources(), this.getMutualExclusionResource(), this.getMutualExclusionResource_Scheduler(), "protectedSharedResources", null, 0, -1, Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getScheduler_SchedulableResources(), this.getSchedulableResource(), this.getSchedulableResource_Host(), "schedulableResources", null, 0, -1, Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(processingResourceEClass, ProcessingResource.class, "ProcessingResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getProcessingResource_SpeedFactor(), theBasicNFP_TypesPackage.getNFP_Real(), "speedFactor", "1.0", 0, 1, ProcessingResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEReference(getProcessingResource_MainScheduler(), this.getScheduler(), null, "mainScheduler", null, 0, 1, ProcessingResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);

		initEClass(computingResourceEClass, ComputingResource.class, "ComputingResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(mutualExclusionResourceEClass, MutualExclusionResource.class, "MutualExclusionResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getMutualExclusionResource_ProtectKind(), theGRM_BasicTypesPackage.getProtectProtocolKind(), "protectKind", "PriorityInheritance", 0, 1, MutualExclusionResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, //$NON-NLS-1$ //$NON-NLS-2$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getMutualExclusionResource_Ceiling(), theBasicNFP_TypesPackage.getNFP_Integer(), "ceiling", null, 0, 1, MutualExclusionResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEAttribute(getMutualExclusionResource_OtherProtectProtocol(), theMARTE_PrimitivesTypesPackage.getString(), "otherProtectProtocol", null, 0, 1, MutualExclusionResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, //$NON-NLS-1$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getMutualExclusionResource_Scheduler(), this.getScheduler(), this.getScheduler_ProtectedSharedResources(), "scheduler", null, 0, 1, MutualExclusionResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(schedulableResourceEClass, SchedulableResource.class, "SchedulableResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getSchedulableResource_SchedParams(), theGRM_BasicTypesPackage.getSchedParameters(), "schedParams", null, 0, -1, SchedulableResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getSchedulableResource_DependentScheduler(), this.getSecondaryScheduler(), this.getSecondaryScheduler_VirtualProcessingUnits(), "dependentScheduler", null, 0, 1, SchedulableResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, //$NON-NLS-1$
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getSchedulableResource_Host(), this.getScheduler(), this.getScheduler_SchedulableResources(), "host", null, 0, 1, SchedulableResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(secondarySchedulerEClass, SecondaryScheduler.class, "SecondaryScheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSecondaryScheduler_VirtualProcessingUnits(), this.getSchedulableResource(), this.getSchedulableResource_DependentScheduler(), "virtualProcessingUnits", null, 0, -1, SecondaryScheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, //$NON-NLS-1$
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(communicationMediaEClass, CommunicationMedia.class, "CommunicationMedia", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getCommunicationMedia_ElementSize(), theBasicNFP_TypesPackage.getNFP_Integer(), "elementSize", null, 0, 1, CommunicationMedia.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getCommunicationMedia_Base_Connector(), theUMLPackage.getConnector(), null, "base_Connector", null, 0, 1, CommunicationMedia.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCommunicationMedia_TransmMode(), theMARTE_DataTypesPackage.getTransmModeKind(), "transmMode", null, 0, 1, CommunicationMedia.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEAttribute(getCommunicationMedia_BlockT(), theBasicNFP_TypesPackage.getNFP_Duration(), "blockT", null, 0, -1, CommunicationMedia.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getCommunicationMedia_PacketT(), theBasicNFP_TypesPackage.getNFP_Duration(), "packetT", null, 0, -1, CommunicationMedia.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getCommunicationMedia_Capacity(), theBasicNFP_TypesPackage.getNFP_DataTxRate(), "capacity", null, 0, -1, CommunicationMedia.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(deviceResourceEClass, DeviceResource.class, "DeviceResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(timingResourceEClass, TimingResource.class, "TimingResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(clockResourceEClass, ClockResource.class, "ClockResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(timerResourceEClass, TimerResource.class, "TimerResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getTimerResource_Duration(), theBasicNFP_TypesPackage.getNFP_Duration(), "duration", null, 0, 1, TimerResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getTimerResource_IsPeriodic(), theMARTE_PrimitivesTypesPackage.getBoolean(), "isPeriodic", null, 0, 1, TimerResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(grServiceEClass, GrService.class, "GrService", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getGrService_Owner(), this.getResource(), null, "owner", null, 0, 1, GrService.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getGrService_Base_ExecutionSpecification(), theUMLPackage.getExecutionSpecification(), null, "base_ExecutionSpecification", null, 0, 1, GrService.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, //$NON-NLS-1$
				!IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getGrService_Base_BehavioralFeature(), theUMLPackage.getBehavioralFeature(), null, "base_BehavioralFeature", null, 1, 1, GrService.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getGrService_Base_Behavior(), theUMLPackage.getBehavior(), null, "base_Behavior", null, 0, 1, GrService.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getGrService_Base_Collaboration(), theUMLPackage.getCollaboration(), null, "base_Collaboration", null, 0, 1, GrService.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getGrService_Base_CollaborationUse(), theUMLPackage.getCollaborationUse(), null, "base_CollaborationUse", null, 0, 1, GrService.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(releaseEClass, Release.class, "Release", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(acquireEClass, Acquire.class, "Acquire", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getAcquire_IsBlocking(), theMARTE_PrimitivesTypesPackage.getBoolean(), "isBlocking", null, 0, 1, Acquire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(resourceUsageEClass, ResourceUsage.class, "ResourceUsage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getResourceUsage_ExecTime(), theBasicNFP_TypesPackage.getNFP_Duration(), "execTime", null, 0, -1, ResourceUsage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getResourceUsage_AllocatedMemory(), theBasicNFP_TypesPackage.getNFP_DataSize(), "allocatedMemory", null, 0, -1, ResourceUsage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getResourceUsage_UsedMemory(), theBasicNFP_TypesPackage.getNFP_DataSize(), "usedMemory", null, 0, -1, ResourceUsage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getResourceUsage_PowerPeak(), theBasicNFP_TypesPackage.getNFP_Power(), "powerPeak", null, 0, -1, ResourceUsage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getResourceUsage_Energy(), theBasicNFP_TypesPackage.getNFP_Energy(), "energy", null, 0, -1, ResourceUsage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(getResourceUsage_Base_NamedElement(), theUMLPackage.getNamedElement(), null, "base_NamedElement", null, 0, 1, ResourceUsage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getResourceUsage_SubUsage(), this.getResourceUsage(), null, "subUsage", null, 0, -1, ResourceUsage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getResourceUsage_UsedResources(), this.getResource(), null, "usedResources", null, 0, -1, ResourceUsage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				IS_ORDERED);
		initEAttribute(getResourceUsage_MsgSize(), theBasicNFP_TypesPackage.getNFP_DataSize(), "msgSize", null, 0, -1, ResourceUsage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
	}

} // GRMPackageImpl

/**
 * Copyright (c) 2010, 2016 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Chokri MRAIDHA (CEA LIST) chokri.mraidha@cea.fr - Initial API and implementation
 *
 */
package org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.MARTE.MARTEPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GQAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GQAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.MARTE_AnalysisModelPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.PAM.PAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.PAM.impl.PAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.SAM.SAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.SAM.impl.SAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.impl.MARTE_AnalysisModelPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.MARTE_AnnexesPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.RSM.RSMPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.RSM.impl.RSMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.DataTypes.DataTypesPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.DataTypes.impl.DataTypesPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Operators.OperatorsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Operators.impl.OperatorsPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.VSLPackage;
import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Variables.VariablesPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Variables.impl.VariablesPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.impl.VSLPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_Annexes.impl.MARTE_AnnexesPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.GCMPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.impl.GCMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HLAM.HLAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HLAM.impl.HLAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HRMPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwGeneral.HwGeneralPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwGeneral.impl.HwGeneralPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwCommunication.HwCommunicationPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwCommunication.impl.HwCommunicationPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwComputing.HwComputingPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwComputing.impl.HwComputingPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwDevice.HwDevicePackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwDevice.impl.HwDevicePackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwLogicalPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwMemory.HwMemoryPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwMemory.impl.HwMemoryPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwStorageManager.HwStorageManagerPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwStorageManager.impl.HwStorageManagerPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwStoragePackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.impl.HwStoragePackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwTiming.HwTimingPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwTiming.impl.HwTimingPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.impl.HwLogicalPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwLayout.HwLayoutPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwLayout.impl.HwLayoutPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwPhysicalPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwPower.HwPowerPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwPower.impl.HwPowerPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.impl.HwPhysicalPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.impl.HRMPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.MARTE_DesignModelPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SRMPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.SW_BrokeringPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.impl.SW_BrokeringPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SW_ConcurrencyPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.impl.SW_ConcurrencyPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.SW_InteractionPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.impl.SW_InteractionPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SW_ResourceCorePackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.impl.SW_ResourceCorePackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.impl.SRMPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.impl.MARTE_DesignModelPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.AllocPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.impl.AllocPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.CoreElements.CoreElementsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.CoreElements.impl.CoreElementsPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.GRMPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.impl.GRMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.MARTE_FoundationsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.NFPs.NFPsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.NFPs.impl.NFPsPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.Clock;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.ClockConstraint;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.ClockType;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.TimeFactory;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.TimePackage;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.TimedConstraint;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.TimedDomain;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.TimedDurationObservation;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.TimedElement;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.TimedEvent;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.TimedInstantObservation;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.TimedProcessing;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.TimedValueSpecification;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.impl.MARTE_FoundationsPackageImpl;

import org.eclipse.papyrus.MARTE.impl.MARTEPackageImpl;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.impl.BasicNFP_TypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.GRM_BasicTypesPackage;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.impl.GRM_BasicTypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.impl.MARTE_DataTypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.impl.MARTE_PrimitivesTypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.impl.MeasurementUnitsPackageImpl;

import org.eclipse.papyrus.MARTE_Library.RS_Library.RS_LibraryPackage;

import org.eclipse.papyrus.MARTE_Library.RS_Library.impl.RS_LibraryPackageImpl;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.TimeLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.impl.TimeLibraryPackageImpl;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.TimeTypesLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.impl.TimeTypesLibraryPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TimePackageImpl extends EPackageImpl implements TimePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass timedDomainEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass clockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass clockTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass timedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass timedValueSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass timedConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass clockConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass timedInstantObservationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass timedDurationObservationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass timedEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EClass timedProcessingEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.TimePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TimePackageImpl() {
		super(eNS_URI, TimeFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>
	 * This method is used to initialize {@link TimePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TimePackage init() {
		if (isInited) {
			return (TimePackage) EPackage.Registry.INSTANCE.getEPackage(TimePackage.eNS_URI);
		}

		// Obtain or create and register package
		Object registeredTimePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		TimePackageImpl theTimePackage = registeredTimePackage instanceof TimePackageImpl ? (TimePackageImpl) registeredTimePackage : new TimePackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTEPackage.eNS_URI);
		MARTEPackageImpl theMARTEPackage = (MARTEPackageImpl) (registeredPackage instanceof MARTEPackageImpl ? registeredPackage : MARTEPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_FoundationsPackage.eNS_URI);
		MARTE_FoundationsPackageImpl theMARTE_FoundationsPackage = (MARTE_FoundationsPackageImpl) (registeredPackage instanceof MARTE_FoundationsPackageImpl ? registeredPackage : MARTE_FoundationsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(NFPsPackage.eNS_URI);
		NFPsPackageImpl theNFPsPackage = (NFPsPackageImpl) (registeredPackage instanceof NFPsPackageImpl ? registeredPackage : NFPsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CoreElementsPackage.eNS_URI);
		CoreElementsPackageImpl theCoreElementsPackage = (CoreElementsPackageImpl) (registeredPackage instanceof CoreElementsPackageImpl ? registeredPackage : CoreElementsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(AllocPackage.eNS_URI);
		AllocPackageImpl theAllocPackage = (AllocPackageImpl) (registeredPackage instanceof AllocPackageImpl ? registeredPackage : AllocPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GRMPackage.eNS_URI);
		GRMPackageImpl theGRMPackage = (GRMPackageImpl) (registeredPackage instanceof GRMPackageImpl ? registeredPackage : GRMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_AnnexesPackage.eNS_URI);
		MARTE_AnnexesPackageImpl theMARTE_AnnexesPackage = (MARTE_AnnexesPackageImpl) (registeredPackage instanceof MARTE_AnnexesPackageImpl ? registeredPackage : MARTE_AnnexesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(RSMPackage.eNS_URI);
		RSMPackageImpl theRSMPackage = (RSMPackageImpl) (registeredPackage instanceof RSMPackageImpl ? registeredPackage : RSMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(VSLPackage.eNS_URI);
		VSLPackageImpl theVSLPackage = (VSLPackageImpl) (registeredPackage instanceof VSLPackageImpl ? registeredPackage : VSLPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(VariablesPackage.eNS_URI);
		VariablesPackageImpl theVariablesPackage = (VariablesPackageImpl) (registeredPackage instanceof VariablesPackageImpl ? registeredPackage : VariablesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI);
		OperatorsPackageImpl theOperatorsPackage = (OperatorsPackageImpl) (registeredPackage instanceof OperatorsPackageImpl ? registeredPackage : OperatorsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(DataTypesPackage.eNS_URI);
		DataTypesPackageImpl theDataTypesPackage = (DataTypesPackageImpl) (registeredPackage instanceof DataTypesPackageImpl ? registeredPackage : DataTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_DesignModelPackage.eNS_URI);
		MARTE_DesignModelPackageImpl theMARTE_DesignModelPackage = (MARTE_DesignModelPackageImpl) (registeredPackage instanceof MARTE_DesignModelPackageImpl ? registeredPackage : MARTE_DesignModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HLAMPackage.eNS_URI);
		HLAMPackageImpl theHLAMPackage = (HLAMPackageImpl) (registeredPackage instanceof HLAMPackageImpl ? registeredPackage : HLAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HRMPackage.eNS_URI);
		HRMPackageImpl theHRMPackage = (HRMPackageImpl) (registeredPackage instanceof HRMPackageImpl ? registeredPackage : HRMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwLogicalPackage.eNS_URI);
		HwLogicalPackageImpl theHwLogicalPackage = (HwLogicalPackageImpl) (registeredPackage instanceof HwLogicalPackageImpl ? registeredPackage : HwLogicalPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwComputingPackage.eNS_URI);
		HwComputingPackageImpl theHwComputingPackage = (HwComputingPackageImpl) (registeredPackage instanceof HwComputingPackageImpl ? registeredPackage : HwComputingPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwCommunicationPackage.eNS_URI);
		HwCommunicationPackageImpl theHwCommunicationPackage = (HwCommunicationPackageImpl) (registeredPackage instanceof HwCommunicationPackageImpl ? registeredPackage : HwCommunicationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwStoragePackage.eNS_URI);
		HwStoragePackageImpl theHwStoragePackage = (HwStoragePackageImpl) (registeredPackage instanceof HwStoragePackageImpl ? registeredPackage : HwStoragePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwStorageManagerPackage.eNS_URI);
		HwStorageManagerPackageImpl theHwStorageManagerPackage = (HwStorageManagerPackageImpl) (registeredPackage instanceof HwStorageManagerPackageImpl ? registeredPackage : HwStorageManagerPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwMemoryPackage.eNS_URI);
		HwMemoryPackageImpl theHwMemoryPackage = (HwMemoryPackageImpl) (registeredPackage instanceof HwMemoryPackageImpl ? registeredPackage : HwMemoryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwTimingPackage.eNS_URI);
		HwTimingPackageImpl theHwTimingPackage = (HwTimingPackageImpl) (registeredPackage instanceof HwTimingPackageImpl ? registeredPackage : HwTimingPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwDevicePackage.eNS_URI);
		HwDevicePackageImpl theHwDevicePackage = (HwDevicePackageImpl) (registeredPackage instanceof HwDevicePackageImpl ? registeredPackage : HwDevicePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwGeneralPackage.eNS_URI);
		HwGeneralPackageImpl theHwGeneralPackage = (HwGeneralPackageImpl) (registeredPackage instanceof HwGeneralPackageImpl ? registeredPackage : HwGeneralPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwPhysicalPackage.eNS_URI);
		HwPhysicalPackageImpl theHwPhysicalPackage = (HwPhysicalPackageImpl) (registeredPackage instanceof HwPhysicalPackageImpl ? registeredPackage : HwPhysicalPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwLayoutPackage.eNS_URI);
		HwLayoutPackageImpl theHwLayoutPackage = (HwLayoutPackageImpl) (registeredPackage instanceof HwLayoutPackageImpl ? registeredPackage : HwLayoutPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwPowerPackage.eNS_URI);
		HwPowerPackageImpl theHwPowerPackage = (HwPowerPackageImpl) (registeredPackage instanceof HwPowerPackageImpl ? registeredPackage : HwPowerPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SRMPackage.eNS_URI);
		SRMPackageImpl theSRMPackage = (SRMPackageImpl) (registeredPackage instanceof SRMPackageImpl ? registeredPackage : SRMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_ResourceCorePackage.eNS_URI);
		SW_ResourceCorePackageImpl theSW_ResourceCorePackage = (SW_ResourceCorePackageImpl) (registeredPackage instanceof SW_ResourceCorePackageImpl ? registeredPackage : SW_ResourceCorePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_ConcurrencyPackage.eNS_URI);
		SW_ConcurrencyPackageImpl theSW_ConcurrencyPackage = (SW_ConcurrencyPackageImpl) (registeredPackage instanceof SW_ConcurrencyPackageImpl ? registeredPackage : SW_ConcurrencyPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_BrokeringPackage.eNS_URI);
		SW_BrokeringPackageImpl theSW_BrokeringPackage = (SW_BrokeringPackageImpl) (registeredPackage instanceof SW_BrokeringPackageImpl ? registeredPackage : SW_BrokeringPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_InteractionPackage.eNS_URI);
		SW_InteractionPackageImpl theSW_InteractionPackage = (SW_InteractionPackageImpl) (registeredPackage instanceof SW_InteractionPackageImpl ? registeredPackage : SW_InteractionPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GCMPackage.eNS_URI);
		GCMPackageImpl theGCMPackage = (GCMPackageImpl) (registeredPackage instanceof GCMPackageImpl ? registeredPackage : GCMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_AnalysisModelPackage.eNS_URI);
		MARTE_AnalysisModelPackageImpl theMARTE_AnalysisModelPackage = (MARTE_AnalysisModelPackageImpl) (registeredPackage instanceof MARTE_AnalysisModelPackageImpl ? registeredPackage : MARTE_AnalysisModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GQAMPackage.eNS_URI);
		GQAMPackageImpl theGQAMPackage = (GQAMPackageImpl) (registeredPackage instanceof GQAMPackageImpl ? registeredPackage : GQAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SAMPackage.eNS_URI);
		SAMPackageImpl theSAMPackage = (SAMPackageImpl) (registeredPackage instanceof SAMPackageImpl ? registeredPackage : SAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(PAMPackage.eNS_URI);
		PAMPackageImpl thePAMPackage = (PAMPackageImpl) (registeredPackage instanceof PAMPackageImpl ? registeredPackage : PAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MeasurementUnitsPackage.eNS_URI);
		MeasurementUnitsPackageImpl theMeasurementUnitsPackage = (MeasurementUnitsPackageImpl) (registeredPackage instanceof MeasurementUnitsPackageImpl ? registeredPackage : MeasurementUnitsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GRM_BasicTypesPackage.eNS_URI);
		GRM_BasicTypesPackageImpl theGRM_BasicTypesPackage = (GRM_BasicTypesPackageImpl) (registeredPackage instanceof GRM_BasicTypesPackageImpl ? registeredPackage : GRM_BasicTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_DataTypesPackage.eNS_URI);
		MARTE_DataTypesPackageImpl theMARTE_DataTypesPackage = (MARTE_DataTypesPackageImpl) (registeredPackage instanceof MARTE_DataTypesPackageImpl ? registeredPackage : MARTE_DataTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BasicNFP_TypesPackage.eNS_URI);
		BasicNFP_TypesPackageImpl theBasicNFP_TypesPackage = (BasicNFP_TypesPackageImpl) (registeredPackage instanceof BasicNFP_TypesPackageImpl ? registeredPackage : BasicNFP_TypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimeTypesLibraryPackage.eNS_URI);
		TimeTypesLibraryPackageImpl theTimeTypesLibraryPackage = (TimeTypesLibraryPackageImpl) (registeredPackage instanceof TimeTypesLibraryPackageImpl ? registeredPackage : TimeTypesLibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimeLibraryPackage.eNS_URI);
		TimeLibraryPackageImpl theTimeLibraryPackage = (TimeLibraryPackageImpl) (registeredPackage instanceof TimeLibraryPackageImpl ? registeredPackage : TimeLibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(RS_LibraryPackage.eNS_URI);
		RS_LibraryPackageImpl theRS_LibraryPackage = (RS_LibraryPackageImpl) (registeredPackage instanceof RS_LibraryPackageImpl ? registeredPackage : RS_LibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_PrimitivesTypesPackage.eNS_URI);
		MARTE_PrimitivesTypesPackageImpl theMARTE_PrimitivesTypesPackage = (MARTE_PrimitivesTypesPackageImpl) (registeredPackage instanceof MARTE_PrimitivesTypesPackageImpl ? registeredPackage : MARTE_PrimitivesTypesPackage.eINSTANCE);

		// Create package meta-data objects
		theTimePackage.createPackageContents();
		theMARTEPackage.createPackageContents();
		theMARTE_FoundationsPackage.createPackageContents();
		theNFPsPackage.createPackageContents();
		theCoreElementsPackage.createPackageContents();
		theAllocPackage.createPackageContents();
		theGRMPackage.createPackageContents();
		theMARTE_AnnexesPackage.createPackageContents();
		theRSMPackage.createPackageContents();
		theVSLPackage.createPackageContents();
		theVariablesPackage.createPackageContents();
		theOperatorsPackage.createPackageContents();
		theDataTypesPackage.createPackageContents();
		theMARTE_DesignModelPackage.createPackageContents();
		theHLAMPackage.createPackageContents();
		theHRMPackage.createPackageContents();
		theHwLogicalPackage.createPackageContents();
		theHwComputingPackage.createPackageContents();
		theHwCommunicationPackage.createPackageContents();
		theHwStoragePackage.createPackageContents();
		theHwStorageManagerPackage.createPackageContents();
		theHwMemoryPackage.createPackageContents();
		theHwTimingPackage.createPackageContents();
		theHwDevicePackage.createPackageContents();
		theHwGeneralPackage.createPackageContents();
		theHwPhysicalPackage.createPackageContents();
		theHwLayoutPackage.createPackageContents();
		theHwPowerPackage.createPackageContents();
		theSRMPackage.createPackageContents();
		theSW_ResourceCorePackage.createPackageContents();
		theSW_ConcurrencyPackage.createPackageContents();
		theSW_BrokeringPackage.createPackageContents();
		theSW_InteractionPackage.createPackageContents();
		theGCMPackage.createPackageContents();
		theMARTE_AnalysisModelPackage.createPackageContents();
		theGQAMPackage.createPackageContents();
		theSAMPackage.createPackageContents();
		thePAMPackage.createPackageContents();
		theMeasurementUnitsPackage.createPackageContents();
		theGRM_BasicTypesPackage.createPackageContents();
		theMARTE_DataTypesPackage.createPackageContents();
		theBasicNFP_TypesPackage.createPackageContents();
		theTimeTypesLibraryPackage.createPackageContents();
		theTimeLibraryPackage.createPackageContents();
		theRS_LibraryPackage.createPackageContents();
		theMARTE_PrimitivesTypesPackage.createPackageContents();

		// Initialize created meta-data
		theTimePackage.initializePackageContents();
		theMARTEPackage.initializePackageContents();
		theMARTE_FoundationsPackage.initializePackageContents();
		theNFPsPackage.initializePackageContents();
		theCoreElementsPackage.initializePackageContents();
		theAllocPackage.initializePackageContents();
		theGRMPackage.initializePackageContents();
		theMARTE_AnnexesPackage.initializePackageContents();
		theRSMPackage.initializePackageContents();
		theVSLPackage.initializePackageContents();
		theVariablesPackage.initializePackageContents();
		theOperatorsPackage.initializePackageContents();
		theDataTypesPackage.initializePackageContents();
		theMARTE_DesignModelPackage.initializePackageContents();
		theHLAMPackage.initializePackageContents();
		theHRMPackage.initializePackageContents();
		theHwLogicalPackage.initializePackageContents();
		theHwComputingPackage.initializePackageContents();
		theHwCommunicationPackage.initializePackageContents();
		theHwStoragePackage.initializePackageContents();
		theHwStorageManagerPackage.initializePackageContents();
		theHwMemoryPackage.initializePackageContents();
		theHwTimingPackage.initializePackageContents();
		theHwDevicePackage.initializePackageContents();
		theHwGeneralPackage.initializePackageContents();
		theHwPhysicalPackage.initializePackageContents();
		theHwLayoutPackage.initializePackageContents();
		theHwPowerPackage.initializePackageContents();
		theSRMPackage.initializePackageContents();
		theSW_ResourceCorePackage.initializePackageContents();
		theSW_ConcurrencyPackage.initializePackageContents();
		theSW_BrokeringPackage.initializePackageContents();
		theSW_InteractionPackage.initializePackageContents();
		theGCMPackage.initializePackageContents();
		theMARTE_AnalysisModelPackage.initializePackageContents();
		theGQAMPackage.initializePackageContents();
		theSAMPackage.initializePackageContents();
		thePAMPackage.initializePackageContents();
		theMeasurementUnitsPackage.initializePackageContents();
		theGRM_BasicTypesPackage.initializePackageContents();
		theMARTE_DataTypesPackage.initializePackageContents();
		theBasicNFP_TypesPackage.initializePackageContents();
		theTimeTypesLibraryPackage.initializePackageContents();
		theTimeLibraryPackage.initializePackageContents();
		theRS_LibraryPackage.initializePackageContents();
		theMARTE_PrimitivesTypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTimePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TimePackage.eNS_URI, theTimePackage);
		return theTimePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getTimedDomain() {
		return timedDomainEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getTimedDomain_Base_Namespace() {
		return (EReference) timedDomainEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getClock() {
		return clockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getClock_Standard() {
		return (EAttribute) clockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getClock_Base_InstanceSpecification() {
		return (EReference) clockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getClock_Type() {
		return (EReference) clockEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getClock_Unit() {
		return (EReference) clockEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getClock_Base_Property() {
		return (EReference) clockEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getClock_Base_Event() {
		return (EReference) clockEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getClockType() {
		return clockTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getClockType_Nature() {
		return (EAttribute) clockTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getClockType_UnitType() {
		return (EReference) clockTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getClockType_IsLogical() {
		return (EAttribute) clockTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getClockType_ResolAttr() {
		return (EReference) clockTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getClockType_MaxValAttr() {
		return (EReference) clockTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getClockType_OffsetAttr() {
		return (EReference) clockTypeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getClockType_GetTime() {
		return (EReference) clockTypeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getClockType_SetTime() {
		return (EReference) clockTypeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getClockType_IndexToValue() {
		return (EReference) clockTypeEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getClockType_Base_Class() {
		return (EReference) clockTypeEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getTimedElement() {
		return timedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getTimedElement_On() {
		return (EReference) timedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getTimedValueSpecification() {
		return timedValueSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getTimedValueSpecification_Interpretation() {
		return (EAttribute) timedValueSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getTimedValueSpecification_Base_ValueSpecification() {
		return (EReference) timedValueSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getTimedConstraint() {
		return timedConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getTimedConstraint_Interpretation() {
		return (EAttribute) timedConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getClockConstraint() {
		return clockConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getClockConstraint_IsCoincidenceBased() {
		return (EAttribute) clockConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getClockConstraint_IsPrecedenceBased() {
		return (EAttribute) clockConstraintEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getClockConstraint_IsChronometricBased() {
		return (EAttribute) clockConstraintEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getTimedInstantObservation() {
		return timedInstantObservationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getTimedInstantObservation_ObsKind() {
		return (EAttribute) timedInstantObservationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getTimedInstantObservation_Base_TimeObservation() {
		return (EReference) timedInstantObservationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getTimedDurationObservation() {
		return timedDurationObservationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getTimedDurationObservation_ObsKind() {
		return (EAttribute) timedDurationObservationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getTimedDurationObservation_Base_DurationObservation() {
		return (EReference) timedDurationObservationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getTimedEvent() {
		return timedEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EAttribute getTimedEvent_Repetition() {
		return (EAttribute) timedEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getTimedEvent_Base_TimeEvent() {
		return (EReference) timedEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getTimedEvent_Every() {
		return (EReference) timedEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EClass getTimedProcessing() {
		return timedProcessingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getTimedProcessing_Base_Action() {
		return (EReference) timedProcessingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getTimedProcessing_Base_Behavior() {
		return (EReference) timedProcessingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getTimedProcessing_Base_Message() {
		return (EReference) timedProcessingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getTimedProcessing_Duration() {
		return (EReference) timedProcessingEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getTimedProcessing_Start() {
		return (EReference) timedProcessingEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EReference getTimedProcessing_Finish() {
		return (EReference) timedProcessingEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public TimeFactory getTimeFactory() {
		return (TimeFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package. This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) {
			return;
		}
		isCreated = true;

		// Create classes and their features
		timedDomainEClass = createEClass(TIMED_DOMAIN);
		createEReference(timedDomainEClass, TIMED_DOMAIN__BASE_NAMESPACE);

		clockEClass = createEClass(CLOCK);
		createEAttribute(clockEClass, CLOCK__STANDARD);
		createEReference(clockEClass, CLOCK__BASE_INSTANCE_SPECIFICATION);
		createEReference(clockEClass, CLOCK__TYPE);
		createEReference(clockEClass, CLOCK__UNIT);
		createEReference(clockEClass, CLOCK__BASE_PROPERTY);
		createEReference(clockEClass, CLOCK__BASE_EVENT);

		clockTypeEClass = createEClass(CLOCK_TYPE);
		createEAttribute(clockTypeEClass, CLOCK_TYPE__NATURE);
		createEReference(clockTypeEClass, CLOCK_TYPE__UNIT_TYPE);
		createEAttribute(clockTypeEClass, CLOCK_TYPE__IS_LOGICAL);
		createEReference(clockTypeEClass, CLOCK_TYPE__RESOL_ATTR);
		createEReference(clockTypeEClass, CLOCK_TYPE__MAX_VAL_ATTR);
		createEReference(clockTypeEClass, CLOCK_TYPE__OFFSET_ATTR);
		createEReference(clockTypeEClass, CLOCK_TYPE__GET_TIME);
		createEReference(clockTypeEClass, CLOCK_TYPE__SET_TIME);
		createEReference(clockTypeEClass, CLOCK_TYPE__INDEX_TO_VALUE);
		createEReference(clockTypeEClass, CLOCK_TYPE__BASE_CLASS);

		timedElementEClass = createEClass(TIMED_ELEMENT);
		createEReference(timedElementEClass, TIMED_ELEMENT__ON);

		timedValueSpecificationEClass = createEClass(TIMED_VALUE_SPECIFICATION);
		createEAttribute(timedValueSpecificationEClass, TIMED_VALUE_SPECIFICATION__INTERPRETATION);
		createEReference(timedValueSpecificationEClass, TIMED_VALUE_SPECIFICATION__BASE_VALUE_SPECIFICATION);

		timedConstraintEClass = createEClass(TIMED_CONSTRAINT);
		createEAttribute(timedConstraintEClass, TIMED_CONSTRAINT__INTERPRETATION);

		clockConstraintEClass = createEClass(CLOCK_CONSTRAINT);
		createEAttribute(clockConstraintEClass, CLOCK_CONSTRAINT__IS_COINCIDENCE_BASED);
		createEAttribute(clockConstraintEClass, CLOCK_CONSTRAINT__IS_PRECEDENCE_BASED);
		createEAttribute(clockConstraintEClass, CLOCK_CONSTRAINT__IS_CHRONOMETRIC_BASED);

		timedInstantObservationEClass = createEClass(TIMED_INSTANT_OBSERVATION);
		createEAttribute(timedInstantObservationEClass, TIMED_INSTANT_OBSERVATION__OBS_KIND);
		createEReference(timedInstantObservationEClass, TIMED_INSTANT_OBSERVATION__BASE_TIME_OBSERVATION);

		timedDurationObservationEClass = createEClass(TIMED_DURATION_OBSERVATION);
		createEAttribute(timedDurationObservationEClass, TIMED_DURATION_OBSERVATION__OBS_KIND);
		createEReference(timedDurationObservationEClass, TIMED_DURATION_OBSERVATION__BASE_DURATION_OBSERVATION);

		timedEventEClass = createEClass(TIMED_EVENT);
		createEAttribute(timedEventEClass, TIMED_EVENT__REPETITION);
		createEReference(timedEventEClass, TIMED_EVENT__BASE_TIME_EVENT);
		createEReference(timedEventEClass, TIMED_EVENT__EVERY);

		timedProcessingEClass = createEClass(TIMED_PROCESSING);
		createEReference(timedProcessingEClass, TIMED_PROCESSING__BASE_ACTION);
		createEReference(timedProcessingEClass, TIMED_PROCESSING__BASE_BEHAVIOR);
		createEReference(timedProcessingEClass, TIMED_PROCESSING__BASE_MESSAGE);
		createEReference(timedProcessingEClass, TIMED_PROCESSING__DURATION);
		createEReference(timedProcessingEClass, TIMED_PROCESSING__START);
		createEReference(timedProcessingEClass, TIMED_PROCESSING__FINISH);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) {
			return;
		}
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		UMLPackage theUMLPackage = (UMLPackage) EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		TimeTypesLibraryPackage theTimeTypesLibraryPackage = (TimeTypesLibraryPackage) EPackage.Registry.INSTANCE.getEPackage(TimeTypesLibraryPackage.eNS_URI);
		NFPsPackage theNFPsPackage = (NFPsPackage) EPackage.Registry.INSTANCE.getEPackage(NFPsPackage.eNS_URI);
		MARTE_PrimitivesTypesPackage theMARTE_PrimitivesTypesPackage = (MARTE_PrimitivesTypesPackage) EPackage.Registry.INSTANCE.getEPackage(MARTE_PrimitivesTypesPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage) EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		timedValueSpecificationEClass.getESuperTypes().add(this.getTimedElement());
		timedConstraintEClass.getESuperTypes().add(theNFPsPackage.getNfpConstraint());
		timedConstraintEClass.getESuperTypes().add(this.getTimedElement());
		clockConstraintEClass.getESuperTypes().add(theNFPsPackage.getNfpConstraint());
		clockConstraintEClass.getESuperTypes().add(this.getTimedElement());
		timedInstantObservationEClass.getESuperTypes().add(this.getTimedElement());
		timedDurationObservationEClass.getESuperTypes().add(this.getTimedElement());
		timedEventEClass.getESuperTypes().add(this.getTimedElement());
		timedProcessingEClass.getESuperTypes().add(this.getTimedElement());

		// Initialize classes and features; add operations and parameters
		initEClass(timedDomainEClass, TimedDomain.class, "TimedDomain", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getTimedDomain_Base_Namespace(), theUMLPackage.getNamespace(), null, "base_Namespace", null, 0, 1, TimedDomain.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);

		initEClass(clockEClass, Clock.class, "Clock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getClock_Standard(), theTimeTypesLibraryPackage.getTimeStandardKind(), "standard", null, 0, 1, Clock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getClock_Base_InstanceSpecification(), theUMLPackage.getInstanceSpecification(), null, "base_InstanceSpecification", null, 0, 1, Clock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, //$NON-NLS-1$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getClock_Type(), this.getClockType(), null, "type", null, 1, 1, Clock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getClock_Unit(), theNFPsPackage.getUnit(), null, "unit", null, 0, 1, Clock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getClock_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 0, 1, Clock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getClock_Base_Event(), theUMLPackage.getEvent(), null, "base_Event", null, 0, 1, Clock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(clockTypeEClass, ClockType.class, "ClockType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getClockType_Nature(), theTimeTypesLibraryPackage.getTimeNatureKind(), "nature", null, 1, 1, ClockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getClockType_UnitType(), theUMLPackage.getEnumeration(), null, "unitType", null, 0, 1, ClockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getClockType_IsLogical(), theMARTE_PrimitivesTypesPackage.getBoolean(), "isLogical", "false", 1, 1, ClockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEReference(getClockType_ResolAttr(), theUMLPackage.getProperty(), null, "resolAttr", null, 0, 1, ClockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getClockType_MaxValAttr(), theUMLPackage.getProperty(), null, "maxValAttr", null, 0, 1, ClockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getClockType_OffsetAttr(), theUMLPackage.getProperty(), null, "offsetAttr", null, 0, 1, ClockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getClockType_GetTime(), theUMLPackage.getOperation(), null, "getTime", null, 0, 1, ClockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getClockType_SetTime(), theUMLPackage.getOperation(), null, "setTime", null, 0, 1, ClockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getClockType_IndexToValue(), theUMLPackage.getOperation(), null, "indexToValue", null, 0, 1, ClockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getClockType_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 0, 1, ClockType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(timedElementEClass, TimedElement.class, "TimedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getTimedElement_On(), this.getClock(), null, "on", null, 1, -1, TimedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(timedValueSpecificationEClass, TimedValueSpecification.class, "TimedValueSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getTimedValueSpecification_Interpretation(), theTimeTypesLibraryPackage.getTimeInterpretationKind(), "interpretation", null, 0, 1, TimedValueSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, //$NON-NLS-1$
				!IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTimedValueSpecification_Base_ValueSpecification(), theUMLPackage.getValueSpecification(), null, "base_ValueSpecification", null, 0, 1, TimedValueSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(timedConstraintEClass, TimedConstraint.class, "TimedConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getTimedConstraint_Interpretation(), theTimeTypesLibraryPackage.getTimeInterpretationKind(), "interpretation", null, 0, 1, TimedConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);

		initEClass(clockConstraintEClass, ClockConstraint.class, "ClockConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getClockConstraint_IsCoincidenceBased(), theMARTE_PrimitivesTypesPackage.getBoolean(), "isCoincidenceBased", null, 1, 1, ClockConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEAttribute(getClockConstraint_IsPrecedenceBased(), theTypesPackage.getBoolean(), "isPrecedenceBased", null, 1, 1, ClockConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getClockConstraint_IsChronometricBased(), theMARTE_PrimitivesTypesPackage.getBoolean(), "isChronometricBased", null, 1, 1, ClockConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);

		initEClass(timedInstantObservationEClass, TimedInstantObservation.class, "TimedInstantObservation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getTimedInstantObservation_ObsKind(), theTimeTypesLibraryPackage.getEventKind(), "obsKind", null, 0, 1, TimedInstantObservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getTimedInstantObservation_Base_TimeObservation(), theUMLPackage.getTimeObservation(), null, "base_TimeObservation", null, 0, 1, TimedInstantObservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(timedDurationObservationEClass, TimedDurationObservation.class, "TimedDurationObservation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getTimedDurationObservation_ObsKind(), theTimeTypesLibraryPackage.getEventKind(), "obsKind", null, 0, 2, TimedDurationObservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getTimedDurationObservation_Base_DurationObservation(), theUMLPackage.getDurationObservation(), null, "base_DurationObservation", null, 0, 1, TimedDurationObservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, //$NON-NLS-1$
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(timedEventEClass, TimedEvent.class, "TimedEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getTimedEvent_Repetition(), theMARTE_PrimitivesTypesPackage.getInteger(), "repetition", null, 0, 1, TimedEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getTimedEvent_Base_TimeEvent(), theUMLPackage.getTimeEvent(), null, "base_TimeEvent", null, 0, 1, TimedEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getTimedEvent_Every(), theUMLPackage.getValueSpecification(), null, "every", null, 0, 1, TimedEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);

		initEClass(timedProcessingEClass, TimedProcessing.class, "TimedProcessing", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getTimedProcessing_Base_Action(), theUMLPackage.getAction(), null, "base_Action", null, 0, 1, TimedProcessing.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
		initEReference(getTimedProcessing_Base_Behavior(), theUMLPackage.getBehavior(), null, "base_Behavior", null, 0, 1, TimedProcessing.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getTimedProcessing_Base_Message(), theUMLPackage.getMessage(), null, "base_Message", null, 0, 1, TimedProcessing.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getTimedProcessing_Duration(), theUMLPackage.getValueSpecification(), null, "duration", null, 0, 1, TimedProcessing.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, //$NON-NLS-1$
				!IS_DERIVED, !IS_ORDERED);
		initEReference(getTimedProcessing_Start(), theUMLPackage.getEvent(), null, "start", null, 0, 1, TimedProcessing.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getTimedProcessing_Finish(), theUMLPackage.getEvent(), null, "finish", null, 0, 1, TimedProcessing.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, //$NON-NLS-1$
				!IS_ORDERED);
	}

} // TimePackageImpl

/**
 * Copyright (c) 2010, 2016 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Chokri MRAIDHA (CEA LIST) chokri.mraidha@cea.fr - Initial API and implementation
 *
 */
package org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.impl;

import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.MARTE.MARTEPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GQAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.impl.GQAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.MARTE_AnalysisModelPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.PAM.PAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.PAM.impl.PAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.SAM.SAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.SAM.impl.SAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.impl.MARTE_AnalysisModelPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.MARTE_AnnexesPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.RSM.RSMPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.RSM.impl.RSMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.DataTypes.DataTypesPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.DataTypes.impl.DataTypesPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Operators.OperatorsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Operators.impl.OperatorsPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.VSLPackage;
import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Variables.VariablesPackage;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.Variables.impl.VariablesPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.impl.VSLPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_Annexes.impl.MARTE_AnnexesPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.GCMPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.impl.GCMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HLAM.HLAMPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HLAM.impl.HLAMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HRMPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwGeneral.HwGeneralPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwGeneral.impl.HwGeneralPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwCommunication.HwCommunicationPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwCommunication.impl.HwCommunicationPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwComputing.HwComputingPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwComputing.impl.HwComputingPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwDevice.HwDevicePackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwDevice.impl.HwDevicePackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwLogicalPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwMemory.HwMemoryPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwMemory.impl.HwMemoryPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwStorageManager.HwStorageManagerPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwStorageManager.impl.HwStorageManagerPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.HwStoragePackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwStorage.impl.HwStoragePackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwTiming.HwTimingPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwTiming.impl.HwTimingPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.impl.HwLogicalPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwLayout.HwLayoutPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwLayout.impl.HwLayoutPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwPhysicalPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwPower.HwPowerPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.HwPower.impl.HwPowerPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwPhysical.impl.HwPhysicalPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.impl.HRMPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.MARTE_DesignModelPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SRMPackage;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.SW_BrokeringPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Brokering.impl.SW_BrokeringPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SW_ConcurrencyPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.impl.SW_ConcurrencyPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.SW_InteractionPackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Interaction.impl.SW_InteractionPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.SW_ResourceCorePackage;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_ResourceCore.impl.SW_ResourceCorePackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.impl.SRMPackageImpl;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.impl.MARTE_DesignModelPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.AllocPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Alloc.impl.AllocPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.CoreElements.CoreElementsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.CoreElements.impl.CoreElementsPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.GRMPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.impl.GRMPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.MARTE_FoundationsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.NFPs.NFPsPackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.NFPs.impl.NFPsPackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.TimePackage;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.Time.impl.TimePackageImpl;

import org.eclipse.papyrus.MARTE.MARTE_Foundations.impl.MARTE_FoundationsPackageImpl;

import org.eclipse.papyrus.MARTE.impl.MARTEPackageImpl;

import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesFactory;
import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage;
import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.DirectionKind;
import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.SourceKind;
import org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.StatisticalQualifierKind;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.GRM_BasicTypesPackage;

import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.impl.GRM_BasicTypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.MARTE_DataTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_DataTypes.impl.MARTE_DataTypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.MARTE_PrimitivesTypesPackage;

import org.eclipse.papyrus.MARTE_Library.MARTE_PrimitivesTypes.impl.MARTE_PrimitivesTypesPackageImpl;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.MeasurementUnitsPackage;

import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.impl.MeasurementUnitsPackageImpl;

import org.eclipse.papyrus.MARTE_Library.RS_Library.RS_LibraryPackage;

import org.eclipse.papyrus.MARTE_Library.RS_Library.impl.RS_LibraryPackageImpl;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.TimeLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeLibrary.impl.TimeLibraryPackageImpl;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.TimeTypesLibraryPackage;

import org.eclipse.papyrus.MARTE_Library.TimeTypesLibrary.impl.TimeTypesLibraryPackageImpl;

import org.eclipse.uml2.types.TypesPackage;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BasicNFP_TypesPackageImpl extends EPackageImpl implements BasicNFP_TypesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EEnum sourceKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EEnum directionKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EEnum statisticalQualifierKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_CommonTypeEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_BooleanEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_NaturalEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_StringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_RealEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_IntegerEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_DateTimeEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_DurationEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_DataTxRateEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_FrequencyEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_PowerEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_DataSizeEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_EnergyEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_LengthEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_AreaEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType arrivalPatternEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType periodicPatternEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType aperiodicPatternEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType closedPatternEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType sporadicPatternEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType burstPatternEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType irregularPatternEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_PercentageEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType openPatternEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_PriceEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private EDataType nfP_WeightEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.MARTE_Library.BasicNFP_Types.BasicNFP_TypesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private BasicNFP_TypesPackageImpl() {
		super(eNS_URI, BasicNFP_TypesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>
	 * This method is used to initialize {@link BasicNFP_TypesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static BasicNFP_TypesPackage init() {
		if (isInited) {
			return (BasicNFP_TypesPackage) EPackage.Registry.INSTANCE.getEPackage(BasicNFP_TypesPackage.eNS_URI);
		}

		// Obtain or create and register package
		Object registeredBasicNFP_TypesPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		BasicNFP_TypesPackageImpl theBasicNFP_TypesPackage = registeredBasicNFP_TypesPackage instanceof BasicNFP_TypesPackageImpl ? (BasicNFP_TypesPackageImpl) registeredBasicNFP_TypesPackage : new BasicNFP_TypesPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTEPackage.eNS_URI);
		MARTEPackageImpl theMARTEPackage = (MARTEPackageImpl) (registeredPackage instanceof MARTEPackageImpl ? registeredPackage : MARTEPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_FoundationsPackage.eNS_URI);
		MARTE_FoundationsPackageImpl theMARTE_FoundationsPackage = (MARTE_FoundationsPackageImpl) (registeredPackage instanceof MARTE_FoundationsPackageImpl ? registeredPackage : MARTE_FoundationsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(NFPsPackage.eNS_URI);
		NFPsPackageImpl theNFPsPackage = (NFPsPackageImpl) (registeredPackage instanceof NFPsPackageImpl ? registeredPackage : NFPsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CoreElementsPackage.eNS_URI);
		CoreElementsPackageImpl theCoreElementsPackage = (CoreElementsPackageImpl) (registeredPackage instanceof CoreElementsPackageImpl ? registeredPackage : CoreElementsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(AllocPackage.eNS_URI);
		AllocPackageImpl theAllocPackage = (AllocPackageImpl) (registeredPackage instanceof AllocPackageImpl ? registeredPackage : AllocPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimePackage.eNS_URI);
		TimePackageImpl theTimePackage = (TimePackageImpl) (registeredPackage instanceof TimePackageImpl ? registeredPackage : TimePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GRMPackage.eNS_URI);
		GRMPackageImpl theGRMPackage = (GRMPackageImpl) (registeredPackage instanceof GRMPackageImpl ? registeredPackage : GRMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_AnnexesPackage.eNS_URI);
		MARTE_AnnexesPackageImpl theMARTE_AnnexesPackage = (MARTE_AnnexesPackageImpl) (registeredPackage instanceof MARTE_AnnexesPackageImpl ? registeredPackage : MARTE_AnnexesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(RSMPackage.eNS_URI);
		RSMPackageImpl theRSMPackage = (RSMPackageImpl) (registeredPackage instanceof RSMPackageImpl ? registeredPackage : RSMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(VSLPackage.eNS_URI);
		VSLPackageImpl theVSLPackage = (VSLPackageImpl) (registeredPackage instanceof VSLPackageImpl ? registeredPackage : VSLPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(VariablesPackage.eNS_URI);
		VariablesPackageImpl theVariablesPackage = (VariablesPackageImpl) (registeredPackage instanceof VariablesPackageImpl ? registeredPackage : VariablesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI);
		OperatorsPackageImpl theOperatorsPackage = (OperatorsPackageImpl) (registeredPackage instanceof OperatorsPackageImpl ? registeredPackage : OperatorsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(DataTypesPackage.eNS_URI);
		DataTypesPackageImpl theDataTypesPackage = (DataTypesPackageImpl) (registeredPackage instanceof DataTypesPackageImpl ? registeredPackage : DataTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_DesignModelPackage.eNS_URI);
		MARTE_DesignModelPackageImpl theMARTE_DesignModelPackage = (MARTE_DesignModelPackageImpl) (registeredPackage instanceof MARTE_DesignModelPackageImpl ? registeredPackage : MARTE_DesignModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HLAMPackage.eNS_URI);
		HLAMPackageImpl theHLAMPackage = (HLAMPackageImpl) (registeredPackage instanceof HLAMPackageImpl ? registeredPackage : HLAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HRMPackage.eNS_URI);
		HRMPackageImpl theHRMPackage = (HRMPackageImpl) (registeredPackage instanceof HRMPackageImpl ? registeredPackage : HRMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwLogicalPackage.eNS_URI);
		HwLogicalPackageImpl theHwLogicalPackage = (HwLogicalPackageImpl) (registeredPackage instanceof HwLogicalPackageImpl ? registeredPackage : HwLogicalPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwComputingPackage.eNS_URI);
		HwComputingPackageImpl theHwComputingPackage = (HwComputingPackageImpl) (registeredPackage instanceof HwComputingPackageImpl ? registeredPackage : HwComputingPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwCommunicationPackage.eNS_URI);
		HwCommunicationPackageImpl theHwCommunicationPackage = (HwCommunicationPackageImpl) (registeredPackage instanceof HwCommunicationPackageImpl ? registeredPackage : HwCommunicationPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwStoragePackage.eNS_URI);
		HwStoragePackageImpl theHwStoragePackage = (HwStoragePackageImpl) (registeredPackage instanceof HwStoragePackageImpl ? registeredPackage : HwStoragePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwStorageManagerPackage.eNS_URI);
		HwStorageManagerPackageImpl theHwStorageManagerPackage = (HwStorageManagerPackageImpl) (registeredPackage instanceof HwStorageManagerPackageImpl ? registeredPackage : HwStorageManagerPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwMemoryPackage.eNS_URI);
		HwMemoryPackageImpl theHwMemoryPackage = (HwMemoryPackageImpl) (registeredPackage instanceof HwMemoryPackageImpl ? registeredPackage : HwMemoryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwTimingPackage.eNS_URI);
		HwTimingPackageImpl theHwTimingPackage = (HwTimingPackageImpl) (registeredPackage instanceof HwTimingPackageImpl ? registeredPackage : HwTimingPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwDevicePackage.eNS_URI);
		HwDevicePackageImpl theHwDevicePackage = (HwDevicePackageImpl) (registeredPackage instanceof HwDevicePackageImpl ? registeredPackage : HwDevicePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwGeneralPackage.eNS_URI);
		HwGeneralPackageImpl theHwGeneralPackage = (HwGeneralPackageImpl) (registeredPackage instanceof HwGeneralPackageImpl ? registeredPackage : HwGeneralPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwPhysicalPackage.eNS_URI);
		HwPhysicalPackageImpl theHwPhysicalPackage = (HwPhysicalPackageImpl) (registeredPackage instanceof HwPhysicalPackageImpl ? registeredPackage : HwPhysicalPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwLayoutPackage.eNS_URI);
		HwLayoutPackageImpl theHwLayoutPackage = (HwLayoutPackageImpl) (registeredPackage instanceof HwLayoutPackageImpl ? registeredPackage : HwLayoutPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(HwPowerPackage.eNS_URI);
		HwPowerPackageImpl theHwPowerPackage = (HwPowerPackageImpl) (registeredPackage instanceof HwPowerPackageImpl ? registeredPackage : HwPowerPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SRMPackage.eNS_URI);
		SRMPackageImpl theSRMPackage = (SRMPackageImpl) (registeredPackage instanceof SRMPackageImpl ? registeredPackage : SRMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_ResourceCorePackage.eNS_URI);
		SW_ResourceCorePackageImpl theSW_ResourceCorePackage = (SW_ResourceCorePackageImpl) (registeredPackage instanceof SW_ResourceCorePackageImpl ? registeredPackage : SW_ResourceCorePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_ConcurrencyPackage.eNS_URI);
		SW_ConcurrencyPackageImpl theSW_ConcurrencyPackage = (SW_ConcurrencyPackageImpl) (registeredPackage instanceof SW_ConcurrencyPackageImpl ? registeredPackage : SW_ConcurrencyPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_BrokeringPackage.eNS_URI);
		SW_BrokeringPackageImpl theSW_BrokeringPackage = (SW_BrokeringPackageImpl) (registeredPackage instanceof SW_BrokeringPackageImpl ? registeredPackage : SW_BrokeringPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SW_InteractionPackage.eNS_URI);
		SW_InteractionPackageImpl theSW_InteractionPackage = (SW_InteractionPackageImpl) (registeredPackage instanceof SW_InteractionPackageImpl ? registeredPackage : SW_InteractionPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GCMPackage.eNS_URI);
		GCMPackageImpl theGCMPackage = (GCMPackageImpl) (registeredPackage instanceof GCMPackageImpl ? registeredPackage : GCMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_AnalysisModelPackage.eNS_URI);
		MARTE_AnalysisModelPackageImpl theMARTE_AnalysisModelPackage = (MARTE_AnalysisModelPackageImpl) (registeredPackage instanceof MARTE_AnalysisModelPackageImpl ? registeredPackage : MARTE_AnalysisModelPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GQAMPackage.eNS_URI);
		GQAMPackageImpl theGQAMPackage = (GQAMPackageImpl) (registeredPackage instanceof GQAMPackageImpl ? registeredPackage : GQAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SAMPackage.eNS_URI);
		SAMPackageImpl theSAMPackage = (SAMPackageImpl) (registeredPackage instanceof SAMPackageImpl ? registeredPackage : SAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(PAMPackage.eNS_URI);
		PAMPackageImpl thePAMPackage = (PAMPackageImpl) (registeredPackage instanceof PAMPackageImpl ? registeredPackage : PAMPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MeasurementUnitsPackage.eNS_URI);
		MeasurementUnitsPackageImpl theMeasurementUnitsPackage = (MeasurementUnitsPackageImpl) (registeredPackage instanceof MeasurementUnitsPackageImpl ? registeredPackage : MeasurementUnitsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GRM_BasicTypesPackage.eNS_URI);
		GRM_BasicTypesPackageImpl theGRM_BasicTypesPackage = (GRM_BasicTypesPackageImpl) (registeredPackage instanceof GRM_BasicTypesPackageImpl ? registeredPackage : GRM_BasicTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_DataTypesPackage.eNS_URI);
		MARTE_DataTypesPackageImpl theMARTE_DataTypesPackage = (MARTE_DataTypesPackageImpl) (registeredPackage instanceof MARTE_DataTypesPackageImpl ? registeredPackage : MARTE_DataTypesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimeTypesLibraryPackage.eNS_URI);
		TimeTypesLibraryPackageImpl theTimeTypesLibraryPackage = (TimeTypesLibraryPackageImpl) (registeredPackage instanceof TimeTypesLibraryPackageImpl ? registeredPackage : TimeTypesLibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(TimeLibraryPackage.eNS_URI);
		TimeLibraryPackageImpl theTimeLibraryPackage = (TimeLibraryPackageImpl) (registeredPackage instanceof TimeLibraryPackageImpl ? registeredPackage : TimeLibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(RS_LibraryPackage.eNS_URI);
		RS_LibraryPackageImpl theRS_LibraryPackage = (RS_LibraryPackageImpl) (registeredPackage instanceof RS_LibraryPackageImpl ? registeredPackage : RS_LibraryPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MARTE_PrimitivesTypesPackage.eNS_URI);
		MARTE_PrimitivesTypesPackageImpl theMARTE_PrimitivesTypesPackage = (MARTE_PrimitivesTypesPackageImpl) (registeredPackage instanceof MARTE_PrimitivesTypesPackageImpl ? registeredPackage : MARTE_PrimitivesTypesPackage.eINSTANCE);

		// Create package meta-data objects
		theBasicNFP_TypesPackage.createPackageContents();
		theMARTEPackage.createPackageContents();
		theMARTE_FoundationsPackage.createPackageContents();
		theNFPsPackage.createPackageContents();
		theCoreElementsPackage.createPackageContents();
		theAllocPackage.createPackageContents();
		theTimePackage.createPackageContents();
		theGRMPackage.createPackageContents();
		theMARTE_AnnexesPackage.createPackageContents();
		theRSMPackage.createPackageContents();
		theVSLPackage.createPackageContents();
		theVariablesPackage.createPackageContents();
		theOperatorsPackage.createPackageContents();
		theDataTypesPackage.createPackageContents();
		theMARTE_DesignModelPackage.createPackageContents();
		theHLAMPackage.createPackageContents();
		theHRMPackage.createPackageContents();
		theHwLogicalPackage.createPackageContents();
		theHwComputingPackage.createPackageContents();
		theHwCommunicationPackage.createPackageContents();
		theHwStoragePackage.createPackageContents();
		theHwStorageManagerPackage.createPackageContents();
		theHwMemoryPackage.createPackageContents();
		theHwTimingPackage.createPackageContents();
		theHwDevicePackage.createPackageContents();
		theHwGeneralPackage.createPackageContents();
		theHwPhysicalPackage.createPackageContents();
		theHwLayoutPackage.createPackageContents();
		theHwPowerPackage.createPackageContents();
		theSRMPackage.createPackageContents();
		theSW_ResourceCorePackage.createPackageContents();
		theSW_ConcurrencyPackage.createPackageContents();
		theSW_BrokeringPackage.createPackageContents();
		theSW_InteractionPackage.createPackageContents();
		theGCMPackage.createPackageContents();
		theMARTE_AnalysisModelPackage.createPackageContents();
		theGQAMPackage.createPackageContents();
		theSAMPackage.createPackageContents();
		thePAMPackage.createPackageContents();
		theMeasurementUnitsPackage.createPackageContents();
		theGRM_BasicTypesPackage.createPackageContents();
		theMARTE_DataTypesPackage.createPackageContents();
		theTimeTypesLibraryPackage.createPackageContents();
		theTimeLibraryPackage.createPackageContents();
		theRS_LibraryPackage.createPackageContents();
		theMARTE_PrimitivesTypesPackage.createPackageContents();

		// Initialize created meta-data
		theBasicNFP_TypesPackage.initializePackageContents();
		theMARTEPackage.initializePackageContents();
		theMARTE_FoundationsPackage.initializePackageContents();
		theNFPsPackage.initializePackageContents();
		theCoreElementsPackage.initializePackageContents();
		theAllocPackage.initializePackageContents();
		theTimePackage.initializePackageContents();
		theGRMPackage.initializePackageContents();
		theMARTE_AnnexesPackage.initializePackageContents();
		theRSMPackage.initializePackageContents();
		theVSLPackage.initializePackageContents();
		theVariablesPackage.initializePackageContents();
		theOperatorsPackage.initializePackageContents();
		theDataTypesPackage.initializePackageContents();
		theMARTE_DesignModelPackage.initializePackageContents();
		theHLAMPackage.initializePackageContents();
		theHRMPackage.initializePackageContents();
		theHwLogicalPackage.initializePackageContents();
		theHwComputingPackage.initializePackageContents();
		theHwCommunicationPackage.initializePackageContents();
		theHwStoragePackage.initializePackageContents();
		theHwStorageManagerPackage.initializePackageContents();
		theHwMemoryPackage.initializePackageContents();
		theHwTimingPackage.initializePackageContents();
		theHwDevicePackage.initializePackageContents();
		theHwGeneralPackage.initializePackageContents();
		theHwPhysicalPackage.initializePackageContents();
		theHwLayoutPackage.initializePackageContents();
		theHwPowerPackage.initializePackageContents();
		theSRMPackage.initializePackageContents();
		theSW_ResourceCorePackage.initializePackageContents();
		theSW_ConcurrencyPackage.initializePackageContents();
		theSW_BrokeringPackage.initializePackageContents();
		theSW_InteractionPackage.initializePackageContents();
		theGCMPackage.initializePackageContents();
		theMARTE_AnalysisModelPackage.initializePackageContents();
		theGQAMPackage.initializePackageContents();
		theSAMPackage.initializePackageContents();
		thePAMPackage.initializePackageContents();
		theMeasurementUnitsPackage.initializePackageContents();
		theGRM_BasicTypesPackage.initializePackageContents();
		theMARTE_DataTypesPackage.initializePackageContents();
		theTimeTypesLibraryPackage.initializePackageContents();
		theTimeLibraryPackage.initializePackageContents();
		theRS_LibraryPackage.initializePackageContents();
		theMARTE_PrimitivesTypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBasicNFP_TypesPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BasicNFP_TypesPackage.eNS_URI, theBasicNFP_TypesPackage);
		return theBasicNFP_TypesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EEnum getSourceKind() {
		return sourceKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EEnum getDirectionKind() {
		return directionKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EEnum getStatisticalQualifierKind() {
		return statisticalQualifierKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_CommonType() {
		return nfP_CommonTypeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_Boolean() {
		return nfP_BooleanEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_Natural() {
		return nfP_NaturalEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_String() {
		return nfP_StringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_Real() {
		return nfP_RealEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_Integer() {
		return nfP_IntegerEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_DateTime() {
		return nfP_DateTimeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_Duration() {
		return nfP_DurationEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_DataTxRate() {
		return nfP_DataTxRateEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_Frequency() {
		return nfP_FrequencyEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_Power() {
		return nfP_PowerEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_DataSize() {
		return nfP_DataSizeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_Energy() {
		return nfP_EnergyEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_Length() {
		return nfP_LengthEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_Area() {
		return nfP_AreaEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getArrivalPattern() {
		return arrivalPatternEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getPeriodicPattern() {
		return periodicPatternEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getAperiodicPattern() {
		return aperiodicPatternEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getClosedPattern() {
		return closedPatternEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getSporadicPattern() {
		return sporadicPatternEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getBurstPattern() {
		return burstPatternEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getIrregularPattern() {
		return irregularPatternEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_Percentage() {
		return nfP_PercentageEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getOpenPattern() {
		return openPatternEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_Price() {
		return nfP_PriceEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public EDataType getNFP_Weight() {
		return nfP_WeightEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	@Override
	public BasicNFP_TypesFactory getBasicNFP_TypesFactory() {
		return (BasicNFP_TypesFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package. This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) {
			return;
		}
		isCreated = true;

		// Create enums
		sourceKindEEnum = createEEnum(SOURCE_KIND);
		directionKindEEnum = createEEnum(DIRECTION_KIND);
		statisticalQualifierKindEEnum = createEEnum(STATISTICAL_QUALIFIER_KIND);

		// Create data types
		nfP_CommonTypeEDataType = createEDataType(NFP_COMMON_TYPE);
		nfP_BooleanEDataType = createEDataType(NFP_BOOLEAN);
		nfP_NaturalEDataType = createEDataType(NFP_NATURAL);
		nfP_StringEDataType = createEDataType(NFP_STRING);
		nfP_RealEDataType = createEDataType(NFP_REAL);
		nfP_IntegerEDataType = createEDataType(NFP_INTEGER);
		nfP_DateTimeEDataType = createEDataType(NFP_DATE_TIME);
		nfP_DurationEDataType = createEDataType(NFP_DURATION);
		nfP_DataTxRateEDataType = createEDataType(NFP_DATA_TX_RATE);
		nfP_FrequencyEDataType = createEDataType(NFP_FREQUENCY);
		nfP_PowerEDataType = createEDataType(NFP_POWER);
		nfP_DataSizeEDataType = createEDataType(NFP_DATA_SIZE);
		nfP_EnergyEDataType = createEDataType(NFP_ENERGY);
		nfP_LengthEDataType = createEDataType(NFP_LENGTH);
		nfP_AreaEDataType = createEDataType(NFP_AREA);
		arrivalPatternEDataType = createEDataType(ARRIVAL_PATTERN);
		periodicPatternEDataType = createEDataType(PERIODIC_PATTERN);
		aperiodicPatternEDataType = createEDataType(APERIODIC_PATTERN);
		closedPatternEDataType = createEDataType(CLOSED_PATTERN);
		sporadicPatternEDataType = createEDataType(SPORADIC_PATTERN);
		burstPatternEDataType = createEDataType(BURST_PATTERN);
		irregularPatternEDataType = createEDataType(IRREGULAR_PATTERN);
		nfP_PercentageEDataType = createEDataType(NFP_PERCENTAGE);
		openPatternEDataType = createEDataType(OPEN_PATTERN);
		nfP_PriceEDataType = createEDataType(NFP_PRICE);
		nfP_WeightEDataType = createEDataType(NFP_WEIGHT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) {
			return;
		}
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Initialize enums and add enum literals
		initEEnum(sourceKindEEnum, SourceKind.class, "SourceKind"); //$NON-NLS-1$
		addEEnumLiteral(sourceKindEEnum, SourceKind.EST);
		addEEnumLiteral(sourceKindEEnum, SourceKind.MEAS);
		addEEnumLiteral(sourceKindEEnum, SourceKind.CALC);
		addEEnumLiteral(sourceKindEEnum, SourceKind.REQ);

		initEEnum(directionKindEEnum, DirectionKind.class, "DirectionKind"); //$NON-NLS-1$
		addEEnumLiteral(directionKindEEnum, DirectionKind.INCR);
		addEEnumLiteral(directionKindEEnum, DirectionKind.DECR);

		initEEnum(statisticalQualifierKindEEnum, StatisticalQualifierKind.class, "StatisticalQualifierKind"); //$NON-NLS-1$
		addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.MAX);
		addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.MIN);
		addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.MEAN);
		addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.RANGE);
		addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.PERCENT);
		addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.DISTRIB);
		addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.DETERM);
		addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.OTHER);
		addEEnumLiteral(statisticalQualifierKindEEnum, StatisticalQualifierKind.VARIANCE);

		// Initialize data types
		initEDataType(nfP_CommonTypeEDataType, String.class, "NFP_CommonType", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_BooleanEDataType, String.class, "NFP_Boolean", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_NaturalEDataType, String.class, "NFP_Natural", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_StringEDataType, String.class, "NFP_String", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_RealEDataType, String.class, "NFP_Real", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_IntegerEDataType, String.class, "NFP_Integer", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_DateTimeEDataType, String.class, "NFP_DateTime", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_DurationEDataType, String.class, "NFP_Duration", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_DataTxRateEDataType, String.class, "NFP_DataTxRate", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_FrequencyEDataType, String.class, "NFP_Frequency", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_PowerEDataType, String.class, "NFP_Power", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_DataSizeEDataType, String.class, "NFP_DataSize", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_EnergyEDataType, String.class, "NFP_Energy", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_LengthEDataType, String.class, "NFP_Length", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_AreaEDataType, String.class, "NFP_Area", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(arrivalPatternEDataType, String.class, "ArrivalPattern", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(periodicPatternEDataType, String.class, "PeriodicPattern", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(aperiodicPatternEDataType, String.class, "AperiodicPattern", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(closedPatternEDataType, String.class, "ClosedPattern", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(sporadicPatternEDataType, String.class, "SporadicPattern", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(burstPatternEDataType, String.class, "BurstPattern", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(irregularPatternEDataType, String.class, "IrregularPattern", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_PercentageEDataType, String.class, "NFP_Percentage", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(openPatternEDataType, String.class, "OpenPattern", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_PriceEDataType, String.class, "NFP_Price", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEDataType(nfP_WeightEDataType, String.class, "NFP_Weight", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		// Create resource
		createResource(eNS_URI);
	}

} // BasicNFP_TypesPackageImpl
